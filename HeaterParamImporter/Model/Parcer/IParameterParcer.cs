﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.HeaterParamImporter
{
    internal interface IHeaterParcer
    {
        IEnumerable<HeaterData> GetData();
    }
}
