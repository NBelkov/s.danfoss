﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace S.Danfoss.HeaterParamImporter
{
    internal abstract class DanfossParcer
    {
        //protected readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected IDataReader dr;
        protected LevelProvider lp;
        protected string systemSeparator;
        protected string decimalSeparator;
        protected string decimalPattern;
        protected List<HeaterData> output = new List<HeaterData>();

        public DanfossParcer(IDataReader dataReader, LevelProvider levelProvider)
        {
            dr = dataReader;
            lp = levelProvider;
            systemSeparator = /*Regex.Escape(*/CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator/*)*/;
            decimalSeparator = "[.,]";
            decimalPattern = $@"\d+({decimalSeparator}\d+)?";
        }
        public DanfossParcer(IDataReader dataReader, LevelProvider levelProvider, IEnumerable<HeaterData> heaters) : this(dataReader, levelProvider)
        {
            output = new List<HeaterData>(heaters);
        }
        protected virtual int GetInt(string input)
        {
            int output = 0;
            int.TryParse(input, out output);
            return output;
        }
        protected virtual double[] GetDoubles(string input)
        {
            ICollection<double> result = new List<double>();
            MatchCollection matches = Regex.Matches(input, decimalPattern);

            foreach (var part in matches)
            {
                double output;
                string st = Regex.Replace(part.ToString(), decimalSeparator, systemSeparator);
                if (double.TryParse(st, out output))
                {
                    result.Add(output);
                }
                else
                {
                    //log.Debug($"Cannot convert string {part} to double");
                }

            }
            return result.ToArray();
        }
        protected virtual HeaterData Contains(Level l, string riser)
        {
            foreach (var item in output)
            {
                if (item.LevelWrapperForTheSakeOfRevitApiDeveloppers == new LevelWrapper(l)
                    && item.Riser == riser)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
