﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.HeaterParamImporter
{
    internal class LevelWrapper /*: IComparable<LevelWrapper>, IComparable*/
    {
        public Level Level { get; private set; }
        public double? Elevation { get; private set; }

        public LevelWrapper(Level level)
        {
            Level = level;
            Elevation = level?.Elevation;
        }

        //public int CompareTo(LevelWrapper lw)
        //{
        //    if (lw is null && lw.Level is null) return 1;
        //    return Level.Elevation.CompareTo(lw.Level.Elevation);
        //}

        //public int CompareTo(object obj)
        //{
        //    if (obj is LevelWrapper lw)
        //    {
        //        if (lw is null && lw.Level is null) return 1;
        //        return Level.Elevation.CompareTo(lw.Level.Elevation);
        //    }
        //    return 1;
        //}

        public override bool Equals(object obj) => obj is LevelWrapper lw && lw.Level != null && lw.Level.Id.IntegerValue == Level.Id.IntegerValue;
        public override int GetHashCode() => 163565 + 6545 * (Level is null ? 12121 : Level.UniqueId.GetHashCode());
        public override string ToString() => Level?.Name;
    }
}
