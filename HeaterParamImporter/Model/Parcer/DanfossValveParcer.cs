﻿using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace S.Danfoss.HeaterParamImporter
{
    internal class DanfossValveParcer : DanfossParcer, IHeaterParcer
    {
        public DanfossValveParcer(IDataReader dataReader, LevelProvider levelProvider) : base(dataReader, levelProvider)
        {
        }

        public DanfossValveParcer(IDataReader dataReader, LevelProvider levelProvider, IEnumerable<HeaterData> heaters) : base(dataReader, levelProvider, heaters)
        {
        }

        public IEnumerable<HeaterData> GetData()
        {
            var data = dr.RawData;
            //log.Info($"Найдено {data.Count} строк");
            foreach (var dataItem in data)
            {
                bool isUsing = true;
                string msg = string.Empty;

                (Level floor, string riser) = GetPlace(dataItem[3], ref msg);
                var vlvProp = GetValveSet(dataItem[5], ref msg);

                if (floor is null || riser is null || riser == string.Empty)
                {
                    isUsing = false;
                }

                if (Contains(floor, riser) is HeaterData hd)
                {
                    hd.ValveProp = vlvProp;
                    hd.Message = msg;
                    hd.IsUsing = hd.IsUsing == true ? isUsing : false;
                }
                else
                {
                    var heater = new HeaterData
                    {
                        LevelWrapperForTheSakeOfRevitApiDeveloppers = new LevelWrapper(floor),
                        Riser = riser,
                        ValveProp = vlvProp,
                        Message = msg,
                        IsUsing = isUsing,
                    };
                    output.Add(heater);
                }
            }
            return output;
        }

        protected (Level lvl, string riser) GetPlace(string input, ref string msg)
        {
            Level floor = null;
            string riser = "нет";

            if (input.Length >= 4)
            {
                int riserCharsCount = input.Length - 3;
                var lvl = input.Substring(0, riserCharsCount);

                int levelNumber = 0;
                if (int.TryParse(lvl, out levelNumber))
                {
                    floor = lp.GetLevel(levelNumber);
                }
                else
                {
                    floor = lp.GetLevel(lvl);
                }
                if (floor is null)
                {
                    //log.Error($"Не удалось определить уровень по имени Пом.: '{input}', этаж '{lvl}'");
                    msg += $"{(msg == string.Empty ? "" : "\n")}Не удалось определить уровень по имени. Пом.: '{input}', этаж '{lvl}'";
                }

                riser = input.Substring(riserCharsCount);
            }
            else
            {
                msg += $"{(msg == string.Empty ? "" : "\n")}Error: Параметр Пом. '{input}'. Значение имеет не верный формат.";
                //log.Warn($"Пом.: '{input}'. Значение параметра Этаж и Номер стояка должно содержать как минимум 4 симыола, 3 последних - номер стояка");
            }

            return (floor, riser);
        }

        private double GetValveSet(string input, ref string msg)
        {
            var dbls = GetDoubles(input);
            switch (dbls.Length)
            {
                case 0:
                    {
                        msg += $"{(msg == string.Empty ? "" : "\n")}Error: Параметр Настройки '{input}'. Не удалось получить значение.";
                        //log.Warn($"Настройки '{input}'. Не удалось получить значение.");
                        return 0.0;
                    }
                case 1: return dbls[0];
                default:
                    {
                        msg += $"{(msg == string.Empty ? "" : "\n")}Error: Параметр Настройки '{input}'. Несколько значений. Принято {dbls[0]}";
                        //log.Warn($"Настройки '{input}'. Не удалось получить значение. Принято {dbls[0]}");
                        return dbls[0];
                    }

            }
        }
    }
}
