﻿using Autodesk.Revit.DB;
using System.Collections.Generic;



namespace S.Danfoss.HeaterParamImporter
{
    internal class DanfossHeaterParcer : DanfossParcer, IHeaterParcer
    {
        public DanfossHeaterParcer(IDataReader dataReader, LevelProvider levelProvider) : base(dataReader, levelProvider)
        {
        }
        public DanfossHeaterParcer(IDataReader dataReader, LevelProvider levelProvider, IEnumerable<HeaterData> heaters) : base(dataReader, levelProvider, heaters)
        {
        }
        public IEnumerable<HeaterData> GetData()
        {
            var data = dr.RawData;
            //log.Info($"Найдено {data.Count} строк");
            foreach (var dataItem in data)
            {
                bool isUsing = true;
                //Этажи и уровни
                string msg = string.Empty;
                string id = string.Empty;
                id = dataItem[2];
                Level floor = null;
                string riser = "нет";
                if ((id!=string.Empty)&&(id.Length >= 4))
                {
                    int riserCharsCount = 2;
                    var lvl = id.Substring(0, riserCharsCount);

                    int levelNumber = 0;

                    if (int.TryParse(lvl, out levelNumber))
                    {
                        floor = lp.GetLevel(levelNumber);
                    }
                    else
                    {
                        floor = lp.GetLevel(lvl);
                    }
                    if (floor is null)
                    {
                        isUsing = false;
                        //log.Error($"Не удалось определить уровень по имени '{lvl}'");
                        msg += $"{(msg == string.Empty ? "" : "\n")}Не удалось определить уровень по имени. Пом.: '{ dataItem[2]}', этаж '{lvl}'";
                    }
                    //стояк
                    if ((dataItem[1] == null) || (dataItem[1] == ""))
                    {
                        riser = id.Substring(riserCharsCount);
                    }
                    else
                    {
                        string str = System.Convert.ToInt32(dataItem[1].Substring(dataItem[1].Length - 2, 2)).ToString();
                        string str1 = dataItem[1].Substring(0, dataItem[1].Length - 2);
                        riser = str1 + "." + str;
                    }
                }
                else
                {
                    isUsing = false;
                    msg += $"{(msg == string.Empty ? "" : "\n")}Error: Параметр Пом. '{dataItem[2]}'. Значение имеет не верный формат.";
                    //log.Warn($"Пом.: '{dataItem[2]}'. Значение параметра Этаж и Номер стояка должно содержать как минимум 4 символа, 3 последних - номер стояка");
                    // continue;
                }

                //Высота и глубина отопительного прибора
                var depthAndHeight = GetDoubles(dataItem[3]);
                var depth = 0.0;
                var height = 0.0;
                if (depthAndHeight.Length < 2)
                {
                    isUsing = false;
                    msg += $"{(msg == string.Empty ? "" : "\n")}Error: Тип от. пр. '{dataItem[3]}'. Не удалсь распознать одно из значений";
                    //log.Warn($"Тип от. пр.: '{dataItem[3]}'. Не удалсь распознать одно из значений");
                    // continue;
                }
                else if (depthAndHeight.Length > 2)
                {
                    msg += $"{(msg == string.Empty ? "" : "\n")}Warn: Тип от. пр.: '{dataItem[3]}'. Слишком много параметров, будут использованы '{depthAndHeight[0]}' '{depthAndHeight[1]}'";
                    //log.Warn($"Тип от. пр.: '{dataItem[3]}'. Слишком много параметров, будут использованы '{depthAndHeight[0]}' '{depthAndHeight[1]}'");
                    depth = depthAndHeight[0];
                    height = depthAndHeight[1] * 10;

                }
                else
                {
                    depth = depthAndHeight[0];
                    height = depthAndHeight[1] * 10;
                }

                //Ширина отопительного прибора
                var widthArr = GetDoubles(dataItem[5]);
                var width = 0.0;
                if (widthArr.Length < 1)
                {
                    isUsing = false;
                    msg += $"{(msg == string.Empty ? "" : "\n")}L: {dataItem[5]}. Не удалось определить значение";
                    //log.Warn($"L: {dataItem[5]}. Не удалось определить значение");
                    // continue;
                }
                else
                {
                    width = widthArr[0] * 1000;
                }



                //Расход Q
                var qpacArr = GetDoubles(dataItem[6]);
                var qpac = 0.0;
                if (qpacArr.Length < 1)
                {
                    isUsing = false;
                    msg += $"{(msg == string.Empty ? "" : "\n")}Qрас: {dataItem[6]}. Не удалось определить значение";
                    //log.Warn($"Qрас: {dataItem[6]}. Не удалось определить значение");
                    // continue;
                }
                else
                {
                    qpac = qpacArr[0];
                }

                //настройки клапана
                var valvePropArr = GetDoubles(dataItem[7]);
                double valveProp = 0.0;
                if (valvePropArr.Length < 1)
                {
                    isUsing = false;
                    msg += $"{(msg == string.Empty ? "" : "\n")}L: {dataItem[7]}. Не удалось определить значение";
                    //log.Warn($"Настройки: {dataItem[7]}. Не удалось определить значение");
                    // continue;
                }
                else
                {
                    valveProp = valvePropArr[0];
                }


                //запись в класс
                if (Contains(floor, riser) is HeaterData hd)
                {
                    hd.Depth = depth;
                    hd.Height = height;
                    hd.Width = width;
                    hd.Qpac = qpac;
                    hd.ValveProp = valveProp;
                    hd.IsUsing = hd.IsUsing = hd.IsUsing == true ? isUsing : false;
                    hd.Message = msg;
                }
                else
                {
                    var heater = new HeaterData
                    {
                        LevelWrapperForTheSakeOfRevitApiDeveloppers = new LevelWrapper(floor),
                        Riser = riser,
                        Depth = depth,
                        Height = height,
                        Width = width,
                        Qpac = qpac,
                        ValveProp = valveProp,
                        Message = msg,
                        IsUsing = isUsing
                    };
                    // //log.Debug($"{heater}");
                    output.Add(heater);
                }
            }
            return output;
        }
    }
}
