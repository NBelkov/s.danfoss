﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.ViewHelper;

namespace S.Danfoss.HeaterParamImporter
{
    internal class HeaterData : Notifier
    {
       // private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private LevelWrapper floor;
        private string message;
        private bool isUseing;
        public HeaterData()
        {
            Message = string.Empty;
            IsUsing = true;
        }
        public HeaterData(HeaterData other)
        {
            LevelWrapperForTheSakeOfRevitApiDeveloppers = new LevelWrapper(other.LevelWrapperForTheSakeOfRevitApiDeveloppers.Level);
            Riser = other.Riser;
            Depth = other.Depth;
            Height = other.Height;
            Width = other.Width;
            Qpac = other.Qpac;
            ValveProp = other.ValveProp;
            Message = other.Message;
            IsUsing = other.IsUsing;
        }
        public LevelWrapper LevelWrapperForTheSakeOfRevitApiDeveloppers
        {
            get => floor;
            set
            {
                ////log.Debug($"Floor name '{floor}'->'{value}'");
                floor = value;
                NotifyPropertyChanged();
            }
        }
        public string Message
        {
            get
            {
                if (message.Equals(string.Empty))
                    return "OK";
                return message;
            }
            set
            {
                message = value;
                NotifyPropertyChanged();
            }
        }
        public bool IsUsing
        {
            get => isUseing;
            set
            {
                isUseing = value;
                NotifyPropertyChanged();
            }
        }
        public string Riser { get; set; }
        public double Depth { get; set; }
        public double Height { get; set; }
        public double Width { get; set; }
        public double Qpac { get; set; }
        public double ValveProp { get; set; }
        public override string ToString() =>
            $"Уровень {LevelWrapperForTheSakeOfRevitApiDeveloppers}, стояк {Riser}, {Depth}_{Height}_{Width}, Q {Qpac}, Клапан {ValveProp}";
    }
}
