﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.IO;
using S.Danfoss.CommonTools.ViewHelper;
using System.Data;

namespace S.Danfoss.HeaterParamImporter
{
    internal class CsvReader : IDataReader
    {
        private string path;//Путь к файлу
        private List<string[]> rawData = new List<string[]>();//выходной массив строк из CVS
        private List<int> skippingLines = new List<int>();//массив номеров строк которые надо пропустить
        private MainViewModel model;
        public List<string[]> RawData
        {
            get
            {
                if (!rawData.Any()) Update();
                return rawData;
            }
        }
        public CsvReader(MainViewModel MainViewModel,string path, int skipLineCount = 0)//строки таблицы которые нужно пропустить при обработке
        {
            this.path = path;
            this.model = MainViewModel;
            for (int i = 0; i < skipLineCount; i++)
            {
                skippingLines.Add(i);
            }
        }
        public CsvReader(string path, IEnumerable<int> skippingLines)
        {
            this.path = path.Trim();
            this.skippingLines = new List<int>(skippingLines);
        }
        public void Update()
        {
            List<string> xlsx = new List<string> { ".xlsx" };
            List<string> csv = new List<string> { ".csv", ".txt" };
            List<string> formats = new List<string> { ".xlsx", ".csv", ".txt" };
            string file_extension = path.Substring(path.LastIndexOf("."));

            if (!formats.Any(a => a.Equals(file_extension)))
            {
                model.ErrFileExtension();
            }
            else
            {
                try
                {
                    if (xlsx.Any(a => a.Equals(file_extension)))
                    {
                        Update_xlsx();
                    }
                    if (csv.Any(a => a.Equals(file_extension)))
                    {
                        Update_csv();
                    }
                }
                catch
                {
                    model.ErrFile();
                }
            }
        }
        public void Update_csv()
        {
            List<string[]> output = new List<string[]>();
            using (StreamReader parser = File.OpenText(path))
            {
                string Stream_read = parser.ReadToEnd().Trim();
                
                string[] result_read = Stream_read.Split('\n');
                
                int row = 0;

                foreach(string str in result_read)
                {
                    string[] parts = str.Split(';');
                    if (parts is null) break;

                    if (!skippingLines.Any(l => l.Equals(row)))
                    {
                        output.Add(parts);
                    }
                    row++;
                }
            }
            rawData = output;
        }
        public void Update_xlsx()
        {
            List<string[]> output = new List<string[]>();

            using (SpreadsheetDocument document = SpreadsheetDocument.Open(path, true))//(путь,возможность редактирования)
            {
                WorkbookPart workbookPart = document.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.FirstOrDefault();
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                int row = 0;

                foreach (Row r in sheetData.Elements<Row>())
                {
                    if (!skippingLines.Any(l => l.Equals(row)))
                    {
                        string[] parts = new string[18];
                        int stringId = 0;
                        try
                        {
                            foreach (Cell c in r.Elements<Cell>())
                            {
                                if (c.CellValue != null)
                                {
                                    switch (c.CellReference.ToString().Substring(0, 1))
                                    {
                                        case "A": parts[0] = c.CellValue.Text; break;
                                        case "B": parts[1] = c.CellValue.Text; break;
                                        case "C":
                                            try
                                            {
                                                stringId = Convert.ToInt32(c.InnerText);
                                                parts[2] = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(stringId).InnerText;
                                            }
                                            catch
                                            {
                                                parts[2] = c.CellValue.Text;
                                            }
                                            break;
                                        case "D":
                                            stringId = Convert.ToInt32(c.InnerText);
                                            parts[3] = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(stringId).InnerText; break;
                                        case "E": parts[4] = c.CellValue.Text; break;
                                        case "F": parts[5] = c.CellValue.Text; break;
                                        case "G": parts[6] = c.CellValue.Text; break;
                                        case "H": parts[7] = c.CellValue.Text; break;
                                        case "I": parts[8] = c.CellValue.Text; break;
                                        case "J": parts[9] = c.CellValue.Text; break;
                                        case "K": parts[10] = c.CellValue.Text; break;
                                        case "L": parts[11] = c.CellValue.Text; break;
                                        case "M": parts[12] = c.CellValue.Text; break;
                                        case "N": parts[13] = c.CellValue.Text; break;
                                        case "O": parts[14] = c.CellValue.Text; break;
                                        case "P": parts[15] = c.CellValue.Text; break;
                                        case "Q": parts[16] = c.CellValue.Text; break;
                                        case "R": parts[17] = c.CellValue.Text; break;
                                    }
                                }
                            }
                            if ((parts[2]!=null)&& (parts[3] != null) && (parts[5] != null) && (parts[6] != null) && (parts[7] != null))
                            {
                                output.Add(parts);
                            }
                        }
                        catch (Exception e)
                        {
                            model.ErrParse(e);
                        }
                    }
                    row++;
                }
            }
            rawData = output;
        }
    }
}
