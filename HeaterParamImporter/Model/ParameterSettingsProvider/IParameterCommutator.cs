﻿using System.Collections.Generic;

namespace S.Danfoss.HeaterParamImporter
{
    public interface IParameterCommutator
    {
        IEnumerable<string> Configuration { get; set; }
    }
}
