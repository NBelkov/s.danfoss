﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace S.Danfoss.HeaterParamImporter
{
    //отвечает за чтение настроек из json, на вход путь, на выходе List<string>
    public class DirectFamilyJsonConfigurator : IParameterCommutator
    {
        private string path;

        public DirectFamilyJsonConfigurator(string path)
        {
            this.path = path;
        }

        public IEnumerable<string> Configuration
        {
            get => Read();
            set => Write(value);
        }

        private void Write(IEnumerable<string> input)
        {
            using (StreamWriter file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, input);
            }
        }

        private IEnumerable<string> Read()
        {
            if (!File.Exists(path))
            {
                using (var file = File.Create(path))
                {
                    file.Close();
                }
            }
            using (StreamReader file = File.OpenText(path))
            {
                string data = file.ReadToEnd();
                var result = JsonConvert.DeserializeObject<List<string>>(data);
                return result is null ? new List<string>() : result;
            }
        }
    }
}
