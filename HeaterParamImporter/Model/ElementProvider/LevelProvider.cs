﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace S.Danfoss.HeaterParamImporter
{
    internal class LevelProvider
    {
        //private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Document doc;
        private Dictionary<string, Level> levelsString = new Dictionary<string, Level>(); //уровни по имени
        private Dictionary<int, Level> levelsNumeric = new Dictionary<int, Level>();//уровни по ID

        private string intPattern = @"-?\d+";
        public LevelProvider(Document document)
        {
            doc = document;
            GroupLevel();
        }
        public Level GetLevel(int levelNumber)
        {
            if (levelsNumeric.ContainsKey(levelNumber))
            {
                return levelsNumeric[levelNumber];
            }
            else
            {
                return null;
            }
        }
        public Level GetLevel(string levelName)
        {
            if (levelsString.ContainsKey(levelName))
            {
                return levelsString[levelName];
            }
            else
            {
                foreach (var ln in levelsString.Keys)
                {
                    if (ln.Contains(levelName))
                    {
                        levelsString[levelName] = levelsString[ln];
                        return levelsString[ln];
                    }
                }
                return null;
            }
        }
        private void GroupLevel()
        {
            var levelFec = new FilteredElementCollector(doc)
                .WhereElementIsNotElementType()
                .OfClass(typeof(Level))
                .Cast<Level>();

            foreach (var l in levelFec)
            {
                int levelNumber = 0;
                if (IsContainsNumber(l.Name, out levelNumber))
                {
                    if (levelsNumeric.ContainsKey(levelNumber))
                    {
                        ////log.Error($"'{levelsNumeric[levelNumber].Name}' and '{l.Name}' has been determined as the same. '{levelsNumeric[levelNumber].Name}' will be used");
                    }
                    else
                    {
                        levelsNumeric[levelNumber] = l;
                    }
                }
                else
                {
                    levelsString[l.Name] = l;
                }
            }
        }
        private bool IsContainsNumber(string input, out int number)
        {
            var matches = Regex.Matches(input, intPattern);
            switch (matches.Count)
            {
                case 0:
                    {
                        number = 0;
                        ////log.Debug($"input '{input}' does not contains any number");
                        return false;
                    }
                default:
                    {
                        var numLine = matches[0].ToString();
                        if (matches.Count > 1)
                        {
                            ////log.Warn($"input '{input}': has {matches.Count} matches. The first one will be used, {numLine}");
                        }
                        if (int.TryParse(numLine, out number))
                        {
                            return true;
                        }
                        else
                        {
                            number = 0;
                            ////log.Warn($"input '{input}': '{numLine}' cannot be converted to the number");
                            return false;
                        }
                    }
            }
        }
    }
}
