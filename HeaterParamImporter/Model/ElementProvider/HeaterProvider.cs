﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.HeaterParamImporter
{
    internal class HeaterProvider : IElementProvider
    {
        //protected readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IParameterCommutator cc;
        private Document doc;
        public HeaterProvider(IParameterCommutator cc, Document doc)
        {
            this.cc = cc;
            this.doc = doc;
        }
        public ICollection<ElementId> GetElementIds() => FilterElements().ToElementIds();
        public ICollection<Element> GetElements() => FilterElements().ToElements();

        private FilteredElementCollector FilterElements()
        {
            var famNames = cc.Configuration;

            var fec = new FilteredElementCollector(doc)
                .WhereElementIsElementType()
                .OfClass(typeof(FamilySymbol))
                .Cast<FamilySymbol>()
                .Where(fs => famNames.Any(cfgName => cfgName.Equals(fs.FamilyName)))
                .Select(fs => new FamilyInstanceFilter(doc, fs.Id) as ElementFilter)
                .ToList();

            ////log.Debug($"filters {fec.Count}");

            if (fec.Count != 0)
            {
                var filter = new LogicalOrFilter(fec);
                return new FilteredElementCollector(doc)
                .WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_MechanicalEquipment)
                .WherePasses(filter);
            }
            return new FilteredElementCollector(doc).SetCollectorToEmpty();
        }
    }
    public static class FilterElementCollectorUtils
    {
        public static FilteredElementCollector OfElementType(this FilteredElementCollector fec, ElementId typeId)
        {
            var rule = ParameterFilterRuleFactory.CreateEqualsRule(new ElementId(BuiltInParameter.ELEM_TYPE_PARAM), typeId);
            var filter = new ElementParameterFilter(rule);
            return fec.WherePasses(filter);
        }

        /// <summary>
        /// Makes ihe input collector empty
        /// </summary>
        /// <param name="fec">input collector</param>
        public static FilteredElementCollector SetCollectorToEmpty(this FilteredElementCollector fec)
        {
            var filter = new LogicalAndFilter(new ElementIsElementTypeFilter(), new ElementIsElementTypeFilter(true));
            return fec.WherePasses(filter);
        }

        /// <summary>
        /// Leaves the input collector unchanged
        /// </summary>
        /// <param name="fec">input collector</param>
        public static FilteredElementCollector AllElements(this FilteredElementCollector fec)
        {
            var filter = new LogicalOrFilter(new ElementIsElementTypeFilter(), new ElementIsElementTypeFilter(true));
            return fec.WherePasses(filter);
        }

        /// <summary>
        /// Filter all categories except model categories
        /// </summary>
        /// <param name="fec">input collector</param>
    }
}
