﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.HeaterParamImporter
{
    internal interface IElementProvider
    {
        ICollection<Element> GetElements();
        ICollection<ElementId> GetElementIds();
    }
}
