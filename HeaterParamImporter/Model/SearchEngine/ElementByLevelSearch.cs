﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.HeaterParamImporter
{
    internal class ElementByLevelSearch
    {
        //private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Document doc;

        public ElementByLevelSearch(Document document)
        {
            doc = document;
        }

        public ICollection<ElementId> GetElementIds(ICollection<ElementId> ids, Level level)
        {
            if (level is null || !ids.Any()) return new List<ElementId>();

            var elems = new FilteredElementCollector(doc, ids)
                 .WherePasses(new ElementLevelFilter(level.Id))
                 .ToElementIds();

            //log.Debug($"{elems.Count()} found on level '{level?.Name}', LevelId '{level.Id}'");
            return elems;
        }

        public ICollection<Element> GetElements(ICollection<ElementId> ids, Level level)
        {
            if (level is null || !ids.Any()) return new List<Element>();

            var elems = new FilteredElementCollector(doc, ids)
                 .WherePasses(new ElementLevelFilter(level.Id))
                 .ToElements();

            //log.Debug($"{elems.Count()} found on level '{level?.Name}' LevelId '{level.Id}'");
            return elems;
        }
    }
}
