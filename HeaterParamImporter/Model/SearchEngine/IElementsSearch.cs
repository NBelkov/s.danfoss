﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.HeaterParamImporter
{
    internal interface IElementsSearch
    {
        ICollection<Element> GetElements(ICollection<ElementId> ids, string input);
        ICollection<ElementId> GetElementIds(ICollection<ElementId> ids, string input);
    }
}
