﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.HeaterParamImporter
{
    internal class ElementByReiserSearch : IElementsSearch
    {
        private Document doc;
        private ElementId riserParamId;
        //protected readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ElementByReiserSearch(Document doc, ElementId riserParamId)
        {
            this.doc = doc;
            this.riserParamId = riserParamId;
        }
        public ICollection<Element> GetElements(ICollection<ElementId> ids, string riser)
        {
            if (!ids.Any()) return new List<Element>();
            var elems = FilterElements(ids, riser).ToElements();
            //log.Debug($"{elems.Count} found on Riser {riser}");
            return elems;
        }
        public ICollection<ElementId> GetElementIds(ICollection<ElementId> ids, string riser)
        {
            if (!ids.Any()) return new List<ElementId>();

            var elems = FilterElements(ids, riser).ToElementIds();
            //log.Debug($"{elems.Count} found on Riser {riser}");
            return elems;
        }
        private FilteredElementCollector FilterElements(ICollection<ElementId> ids, string riser)
        {
            var filterRule = ParameterFilterRuleFactory.CreateEqualsRule(riserParamId, riser, false);

            return new FilteredElementCollector(doc, ids)
                .WherePasses(new ElementParameterFilter(filterRule));
        }
    }
}
