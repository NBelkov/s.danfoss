﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace S.Danfoss.HeaterParamImporter
{
    internal class AllParametersHeaterWorker : IHeaterWorker
    {
        #region Fields

        //private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Document doc;
        private string sep = @"[^0-9]+";
        private ICollection<ElementId> allHeaters;
        private ElementByLevelSearch hls;
        private ElementByReiserSearch hrs;

        #endregion
        #region Constructor

        public AllParametersHeaterWorker(Document doc, ICollection<ElementId> allHeaters, ElementByLevelSearch hls, ElementByReiserSearch hrs)
        {
            this.doc = doc;
            this.allHeaters = allHeaters;
            this.hls = hls;
            this.hrs = hrs;
        }

        #endregion
        #region Methods

        public bool DoWork(HeaterData hd)
        {
            var elem = FindHeater(hd.LevelWrapperForTheSakeOfRevitApiDeveloppers.Level, hd.Riser);
            if (elem is null)
            {
                ////log.Error($"Не удалось найти элемент '{hd}'");
                return false;
            }

            var type = GetType(elem, hd.Depth, hd.Height, hd.Width);
            if (type is null)
            {
                ////log.Warn($"Не удалось найти тип '{hd}': {hd.Depth}, {hd.Height}, {hd.Width}");
            }
            else
            {
                if (type.Id.IntegerValue != elem.GetTypeId().IntegerValue)
                {
                    SetType(elem, type.Id);
                }
            }

            var val = UnitUtils.ConvertToInternalUnits(hd.Qpac, DisplayUnitType.DUT_WATTS);
            SetValue(elem, "ASML_Тепловая мощность", val);
            SetValue(elem, "ASML_Настройка клапана", hd.ValveProp);
            
            return true;
        }

        private Element FindHeater(Level level, string riser)
        {
            var heatersOnLaevel = hls.GetElementIds(allHeaters, level);
            var heatersOnRiser = hrs.GetElements(heatersOnLaevel, riser);
            switch (heatersOnRiser.Count)
            {
                case 0:
                    {
                        string msg = $"Не удалось найти прибор с параметрами: Этаж {level.Name}, Стояк {riser}";
                        ////log.Error(msg);
                        return null;
                    }
                case 1:
                    {
                        return heatersOnRiser.First();
                    }
                default:
                    {
                        string msg = $"Найдено несколько ({heatersOnRiser.Count}) приборов с параметрами: Этаж {level}, Стояк {riser}. Выбран первый";
                        ////log.Warn(msg);
                        return heatersOnRiser.First();
                    }
            }
        }

        private FamilySymbol GetType(Element elem, double d, double h, double l)
        {
            string pattern = $"{d}{sep}{h}{sep}{l}";


            var typeId = elem.GetTypeId();
            var fs = doc.GetElement(typeId) as FamilySymbol;

            ////log.Debug($"Element: '{fs.Name}' - '{fs.FamilyName}'");


            var fec = new FilteredElementCollector(doc)
                .WherePasses(new FamilySymbolFilter(fs.Family.Id))
                .WhereElementIsElementType();

            //foreach (var f in fec)
            //{
            //    //log.Debug($"{f.Name} - {Regex.IsMatch(f.Name, pattern)} - {pattern} - {d}_{h}_{l}");
            //}

            return fec
                .Cast<FamilySymbol>()
                .Where(t => Regex.IsMatch(t.Name, pattern))
                .FirstOrDefault();
        }

        private void SetType(Element heater, ElementId typeId)
        {
            heater.ChangeTypeId(typeId);
        }

        private void SetValue(Element heater, string paramName, double val)
        {
            var qParam = heater.LookupParameter(paramName);
            qParam.Set(val);
        }

        #endregion
    }
}
