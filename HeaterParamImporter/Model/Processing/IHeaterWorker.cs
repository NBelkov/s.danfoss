﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.HeaterParamImporter
{
    internal interface IHeaterWorker
    {
        bool DoWork(HeaterData hd);
    }
}
