﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace S.Danfoss.HeaterParamImporter
{
    interface IPipeline
    {
        bool BeginProcessing();
        int TotalCount { get; }
        CancellationTokenSource Cts { get; set; }
    }
}
