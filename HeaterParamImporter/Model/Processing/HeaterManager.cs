﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.ViewHelper;
using System;
using System.Threading;
using System.Windows.Threading;

namespace S.Danfoss.HeaterParamImporter
{
    internal class HeaterManager : Notifier, IManager
    {
        #region Fields

        //private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private CancellationTokenSource cts;
        private Document doc;
        private IPipeline worker;
        private MainViewModel vm;


        #endregion
        #region Properties


        #endregion
        #region Constructor

        public HeaterManager(Document doc, IPipeline pipeline, MainViewModel mainViewModel, CancellationTokenSource cancellationTokenSource)
        {
            vm = mainViewModel;
            this.doc = doc;
            cts = cancellationTokenSource;
            pipeline.Cts = cts;
            this.worker = pipeline;
            vm.TotalElementsCount = pipeline.TotalCount;
        }

        #endregion
        #region Methods

        public void Start()
        {
            using (Transaction tx = new Transaction(doc))
            {
                try
                {
                    tx.Start($"Процесс");
                    vm.Processing = true;
                    vm.StepName = $"Обработка...";


                    while (!Dispatcher.CurrentDispatcher.Invoke(new Func<bool>(worker.BeginProcessing), DispatcherPriority.Background, cts.Token))
                    {
                        if (cts.IsCancellationRequested)
                        {
                            vm.StepName = $"Отмена изменений";
                            tx.RollBack();
                            vm.StepName = $"Задание отменено";
                            vm.Processing = false;
                            return;
                        }
                        vm.HandledElementsCount++;
                    }

                    vm.StepName = $"Сохранение изменений";
                    tx.Commit();
                    vm.Processing = false;
                    vm.StepName = $"Задание выполнено";

                }
                catch (Exception ex)
                {
                    vm.StepName = $"Ошибка. Отмена изменений";
                    tx.RollBack();
                    vm.Processing = false;
                    vm.StepName = $"Задание отменено с ошибкой.\n{ex.Message}";
                    ////log.Error("Ошибка", ex);
                }
            }
        }

        public void Stop() => cts.Cancel();

        #endregion
    }
}
