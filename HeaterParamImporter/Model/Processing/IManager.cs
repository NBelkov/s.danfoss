﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.HeaterParamImporter
{
    public interface IManager : INotifyPropertyChanged
    {
        void Start();
        void Stop();
    }
}
