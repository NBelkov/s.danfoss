﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace S.Danfoss.HeaterParamImporter
{
    internal class HeaterPipeline : IPipeline
    {
        #region Fields

        //private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ConcurrentQueue<HeaterData> heaters;
        private IHeaterWorker worker;
        public CancellationTokenSource Cts { get; set; }


        #endregion
        #region Constructor

        public HeaterPipeline(IEnumerable<HeaterData> heaterData, IHeaterWorker worker)
        {
            this.worker = worker;
            heaters = new ConcurrentQueue<HeaterData>(heaterData);
            TotalCount = heaters.Count;
        }

        #endregion
        #region Methods

        public int TotalCount { get; private set; }

        public bool BeginProcessing()
        {
            if (heaters.Any() && !Cts.IsCancellationRequested)
            {
                HeaterData hd;
                if (heaters.TryDequeue(out hd))
                {
                    try
                    {
                        worker.DoWork(hd);
                    }
                    catch (Exception ex)
                    {
                        Cts.Cancel();
                        //log.Fatal("Revit extraction", ex);
                    }
                }
                return false;
            }
            else
            {
                //log.Info($"HeaterQueueWorker is done (elemets count: {heaters.Count})");
                return true;
            }
        }

        #endregion
    }

}
