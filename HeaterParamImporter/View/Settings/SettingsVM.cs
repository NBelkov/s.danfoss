﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.ExternalCommands;
using S.Danfoss.CommonTools.ViewHelper;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace S.Danfoss.HeaterParamImporter
{
    public class SettingsVM : Notifier
    {
        private Document doc;
        private IParameterCommutator cmt;
        private ObservableCollection<string> lostParameters;
        //private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public CheckableList<Family> FamilyList { get; set; }
        public ObservableCollection<string> LostParameters
        {
            get => lostParameters;
            set
            {
                lostParameters = value;
                NotifyPropertyChanged();
            }
        }

        public SettingsVM(Document document, IParameterCommutator commutator)
        {
            doc = document;
            cmt = commutator;

            var fams = new FilteredElementCollector(doc)
                .WhereElementIsElementType()
                .OfCategory(BuiltInCategory.OST_MechanicalEquipment)
                .OfClass(typeof(FamilySymbol))
                .Cast<FamilySymbol>()
                .GroupBy(fs => fs.Family.Name)
                .OrderBy(g => g.Key)
                .Select(g => g.First().Family);


            FamilyList = new CheckableList<Family>(fams.ToList(), f => f.Name);
            FamilyList.Filtered.CollectionChanged += OnFilteredChanged;
            SetSelection();
        }

        private void OnFilteredChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            cmt.Configuration = FamilyList.Filtered.Select(vm => vm.Item.Name);
        }

        public ICommand OkCommand => new RelayCommand
        {
            ExecuteAction = a => (a as Window)?.Close(),
            CanExecutePredicate = p => true
        };

        private void SetSelection()
        {
            LostParameters = new ObservableCollection<string>(cmt.Configuration);

            if (LostParameters.Any())
            {
                foreach (var item in FamilyList.Source)
                {
                    if (LostParameters.Any(c => c == item.Name))
                    {
                        item.IsChecked = true;
                        LostParameters.Remove(item.Name);
                    }

                }
            }
        }

        public IEnumerable<Family> GetSettings() => FamilyList.Filtered.Select(vm => vm.Item);
    }
}
