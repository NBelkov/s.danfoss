﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.ExternalCommands;
using S.Danfoss.CommonTools.ViewHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using WpfStyles;

namespace S.Danfoss.HeaterParamImporter
{
    internal class HeaterCheckVM : Notifier
    {
        
        //private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ObservableCollection<HeaterData> heatersSource;
        private ObservableCollection<LevelWrapper> levelsSource;
        public IEnumerable<HeaterData> Heaters { get; private set; }
        public ObservableCollection<HeaterData> HeatersSource
        {
            get => heatersSource;
            set
            {
                heatersSource = value;
                NotifyPropertyChanged();
            }
        }
        public ObservableCollection<LevelWrapper> LevelsSource
        {
            get => levelsSource;
            set
            {
                levelsSource = value;
                NotifyPropertyChanged();
            }
        }
        public HeaterCheckVM(IEnumerable<Level> levels, IEnumerable<HeaterData> heaters)
        {
            Heaters = heaters;
            HeatersSource = new ObservableCollection<HeaterData>(heaters.Select(h => new HeaterData(h)));
            LevelsSource = new ObservableCollection<LevelWrapper>(levels.Select(l => new LevelWrapper(l)));
        }
        public ICommand OkCommand => new RelayCommand
        {
            ExecuteAction = a =>
            {
                SaveChanges();
                (a as Window)?.Close();
            }
        };
        public ICommand CancelCommand => new RelayCommand { ExecuteAction = a => (a as Window)?.Close() };
        public ICommand ClearCommand => new RelayCommand { ExecuteAction = a => ClearData() };
        private void SaveChanges()
        {
            Heaters = new List<HeaterData>(HeatersSource);
        }
        private void ClearData()
        {
            string msg = $"Очистить все данные?";
            if (MessageBox.Show(msg, "Revit", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
            {
                HeatersSource.Clear();
            }
        }
    }
}
