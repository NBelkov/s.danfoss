﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using S.Danfoss.CommonTools.ExternalCommands;
using S.Danfoss.CommonTools.Utils;
using S.Danfoss.CommonTools.ViewHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
//Почему нет в интерфейсе параметра импорт настроек клапанов?

namespace S.Danfoss.HeaterParamImporter
{
    delegate void Message();
    internal class MainViewModel : Notifier
    {
        //private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);//логгер
              

        private IEnumerable<HeaterData> heaters = new List<HeaterData>(); //лист нагревательных приборов
       
        private List<Level> levels = new List<Level>();//модель уровней Ревит

        private Document doc;//ссылка на открытый проект Revit

        private LevelProvider lvl;//дергает из модели уровни
        private IElementProvider ep;//объекты по фильтру из модели

        private ExternalEvent externalEvent;//внешние события, наследник Autodesk.Revit.UI

        private CancellationTokenSource cts;//маркировка процесса для отмены
        /**/
        private int totalElementsCount; //счетчик элементов всего
        private int handledElementsCount;//счетчик элементов для прогресс бара
        private bool processing;//индикатор активного процесса
        private string stepName;//текст состояни под прогресс баром

        private string heaterPath;//путь к файлу экспорта нагревательных приборов
        private string valvePath;//путь к файлу экспорта параметров клапанов
        /**/
        public int TotalElementsCount
        {
            get => totalElementsCount;
            set
            {
                totalElementsCount = value;
                NotifyPropertyChanged();
            }
        }//получить счетчик элементов всего public
        public int HandledElementsCount
        {
            get => handledElementsCount;
            set
            {
                handledElementsCount = value;
                NotifyPropertyChanged();
            }
        }//получить счетчик элементов public
        public bool Processing
        {
            get => processing;
            set
            {
                processing = value;
                NotifyPropertyChanged();
            }
        }//получить индикатор активного процесса
        public string StepName
        {
            get => stepName;
            set
            {
                stepName = value;
                NotifyPropertyChanged();
            }
        }
        public string HeaterPath
        {
            get => heaterPath;
            set
            {
                heaterPath = value;
                GetHeaterData();
                NotifyPropertyChanged();
                ResetCount();
            }
        }//получить путь к файлу экспорта нагревательных приборов
        public string ValvePath
        {
            get => valvePath;
            set
            {
                valvePath = value;
                GetValveHeaterData();
                NotifyPropertyChanged();
                ResetCount();
            }
        }//получить путь к файлу экспорта параметров клапанов
        /**/
        public MainViewModel(Document document)
        {
            Processing = false;
            Message mes;
            mes = ErrFile;
            doc = document;
            externalEvent = ExternalEvent.Create(new RequestHandler());
            lvl = new LevelProvider(doc);
             levels = new FilteredElementCollector(doc)
                        .WhereElementIsNotElementType()
                        .OfClass(typeof(Level))
                        .Cast<Level>()
                        .OrderBy(l => l.Elevation)
                        .ToList();

            //HeaterPath = @"C:\Users\dev_w10\Desktop\SamWork\DanfossToRevit\Экспорт приборов.csv";
            //ValvePath = @"C:\Users\dev_w10\Desktop\SamWork\DanfossToRevit\Настройки на клапанах.csv";

            string path = EnvUtils.GetMyLocalAppPath("heaters.json", @"\Samolet\Config");// созранение параметров выборки смотри ParameterSettingsProvider
            var cfg = new DirectFamilyJsonConfigurator(path);
            ep = new HeaterProvider(cfg, doc);

            ResetCount();
        }
        //блок интерфейсов привязанных к исполнительным элементам MainWindow.xaml
        public ICommand OpenHeaterDataCommand => new RelayCommand { ExecuteAction = a => HeaterPath = OpenFile() };
        public ICommand OpenValveDataCommand => new RelayCommand { ExecuteAction = a => ValvePath = OpenFile() };
        public ICommand OpenFamSettingsCommand => new RelayCommand { ExecuteAction = a => OpenFamilySettings() };
        public ICommand OpenCheckTableCommand => new RelayCommand { ExecuteAction = a => OpenDataTable() };
        public ICommand ClearDataCommand => new RelayCommand { ExecuteAction = a => ClearData() };
        public ICommand StartCommand => new RelayCommand { ExecuteAction = a => RaiseExternalEvent(StartProcess, "Назначить параметры") };
        public ICommand StopCommand => new RelayCommand { ExecuteAction = a => StopProcess() };
        public ICommand OkCommand => new RelayCommand
        {
            ExecuteAction = a => (a as Window)?.Close(),
            CanExecutePredicate = p => !Processing
        };
        public ICommand CancelCommand => new RelayCommand
        {
            ExecuteAction = a =>
            {
                switch (a)
                {
                    case Window w:
                        {
                            w?.Close();
                            break;
                        }
                    case CancelEventArgs e:
                        {
                            if (!StopProcess())
                            {
                                e.Cancel = true;
                            }
                            break;
                        }
                    default:
                        break;
                }

            },
        };
        /*************************************************************************/
        public string OpenFile()//получить путь разбираемого файла
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel files (*.xlsx;)|*.xlsx;|CSV files (*.csv;)|*.csv;|All files (*.*)|*.*"; // это регулярные выражения для выпадающего списка
            List<string> formats = new List<string> { ".xlsx", ".csv", ".txt" };
            
            if (openFileDialog.ShowDialog() == true)
            {
                string file_extension = openFileDialog.FileName.Substring(openFileDialog.FileName.LastIndexOf("."));
                if (formats.Any(a => a.Equals(file_extension)))
                {
                    return openFileDialog.FileName;//не пустая
                }
            }
            ErrFileExtension();
            return string.Empty;//пустая
        }
        private void RaiseExternalEvent(Action action, string name)//о запуск внешних событий,
        {
            Request.MakeRequest(action, name);
            externalEvent.Raise();
        }
        private void GetHeaterData()
        {
            if (File.Exists(HeaterPath))
            {
                heaters = new DanfossHeaterParcer(new CsvReader(this,HeaterPath, 3), lvl, heaters).GetData();
            }
            else
            {
                //log.Warn($"Файл не найден: {HeaterPath}");
                //MessageBox.Show($"Файл не найден:\n{HeaterPath}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void GetValveHeaterData()
        {
            if (File.Exists(ValvePath))
            {
                heaters = new DanfossValveParcer(new CsvReader(this, ValvePath, 3), lvl, heaters).GetData();
            }
            else
            {
                //log.Warn($"Файл не найден: {ValvePath}");
                MessageBox.Show($"Файл не найден:\n{ValvePath}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void OpenDataTable()
        {
            var hw = new HeaterWindow();
            var vm = new HeaterCheckVM(levels, heaters);
            hw.DataContext = vm;
            hw.ShowDialog();
            heaters = new List<HeaterData>(vm.Heaters);
            ResetCount();
        }
        private void StartProcess()
        {
            cts = new CancellationTokenSource();//маркер процесса, участвует в отмене операции

            var paramId = SharedParameterElement.Lookup(doc, new Guid("09aa9e39-7cfb-4bf1-ae48-00e007942ec8")).Id;

            if (paramId is null)
            {
                string msg = $"Не удалось найти в проекте параметр (Guid ''), содержащий номер стояка";
                //log.Error(msg);
                MessageBox.Show(msg, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            IHeaterWorker worker = new AllParametersHeaterWorker(doc, ep.GetElementIds(), new ElementByLevelSearch(doc), new ElementByReiserSearch(doc, paramId));
            IPipeline pipeline = new HeaterPipeline(heaters.Where(h => h.IsUsing), worker);
            IManager manager = new HeaterManager(doc, pipeline, this, cts);
            manager.Start();
        }
        private bool StopProcess()
        {
            if (!Processing) return true;//если процесс уже остановлен/нет активного процесса
            //if (!cts.IsCancellationRequested) return true; 
            string msg = $"Остановить процесс?\nМодель будет возвращена к первоначальному состоянию до запуска процесса";
            if (MessageBox.Show(msg, "Revit", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
            {
                ResetCount();
                cts?.Cancel();//токен не null не пустой, отменить
                return true;
            }
            return false;
        }
        private void ResetCount()//сброс счетчика при остановке/отмене процесса
        {
            if (!Processing)
            {
                HandledElementsCount = 0;
                TotalElementsCount = heaters.Where(h => h.IsUsing).Count();
                StepName = "Нажмите 'Play'";
            }
        }
        private void OpenFamilySettings() //десереализация настроек семейств из Json
        {
            string path = EnvUtils.GetMyLocalAppPath("heaters.json", @"\Samolet\Config");

            var vm = new SettingsVM(doc, new DirectFamilyJsonConfigurator(path));
            var v = new SettingsView();
            v.DataContext = vm;
            v.ShowDialog();

            var cfg = new DirectFamilyJsonConfigurator(path);
            ep = new HeaterProvider(cfg, doc);
        }
        private void ClearData()//сброс
        {
            string msg = $"Очистить все данные?";
            if (MessageBox.Show(msg, "Revit", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
            {
                HeaterPath = string.Empty;
                heaters = new List<HeaterData>();
                ResetCount();
            }
        }
        public void ErrFile()
        {
            string msg = $"Файл не доступен или занят другим процессом, попробуйте закрыть Excel или откройте файл в Excel и разрешите редактирование";
            //log.Error(msg);
            MessageBox.Show(msg, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        public void ErrParse(Exception e)
        {

            MessageBox.Show(e.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        public void ErrFileExtension()
        {
            string msg = $"Указанный формат не поддерживается, используйте форматы XLSX,CSV для импорта данных";
            //log.Error(msg);
            MessageBox.Show(msg, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
