﻿using System.ComponentModel;
using System.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace S.Danfoss.HeaterParamImporter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Closing += MainViewClosing;
            Loaded += delegate
            {
                MinWidth = ActualWidth;
                MinHeight = ActualHeight;
                ClearValue(SizeToContentProperty);
                this.SizeToContent = SizeToContent.Height;
            };
        }

        private void MainViewClosing(object sender, CancelEventArgs e)
        {
            CancelBtn.Command.Execute(e);
        }
    }
}
