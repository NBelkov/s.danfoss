﻿#define test8
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using S.Danfoss.CommonTools.Utils;
using S.Danfoss.CommonTools.Xtensions;
using S.Danfoss.HeaterParamImporter;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace S.Danfoss.HeaterParamImporter
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    [Autodesk.Revit.Attributes.Regeneration(Autodesk.Revit.Attributes.RegenerationOption.Manual)]
    class HeaterTestCommand : IExternalCommand
    {
        //protected readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        UIApplication uiapp = null;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            uiapp = commandData.Application;
            UIDocument uidoc = uiapp.ActiveUIDocument;
            Document doc = uidoc.Document;
            Selection sel = uidoc.Selection;


            try
            {
                //log.Debug("------------ HeaterTestCommand begin ------------\n");
                var mainWatch = Stopwatch.StartNew();
                #region Time measure 
#if test1
                new DanfossHeaterParcer(new CsvReader(@"C:\Users\dev_w10\Desktop\SamWork\DanfossToRevit\Экспорт приборов.csv")).GetData();
#endif
#if test2

                var fec = new FilteredElementCollector(doc)
                    .WhereElementIsElementType()
                    .OfCategory(BuiltInCategory.OST_MechanicalEquipment)
                    .OfClass(typeof(FamilySymbol))
                    .Cast<FamilySymbol>()
                    .GroupBy(fs => fs.FamilyName)
                    ;

                //log.Debug($"{fec.Count()} elements found:");
                List<string> props = new List<string>();
                foreach (var item in fec)
                {
                    props.Add(item.Key.ToString());
                    //log.Debug($"\t{item.Key}");
                    foreach (var fs in item)
                    {
                        //log.Debug($"\t\t{fs.Name}");
                    }
                }
                //log.Debug(path);

                IParameterCommutator commutator = new DirectFamilyJsonConfigurator(path);
                commutator.Configuration = props;

                //log.Debug($"\n{commutator.Configuration.First()}");
#endif
#if test3
                string path = EnvUtils.GetMyLocalAppPath("heaters.json", @"\Samolet\Config");

                var vm = new SettingsVM(doc, new DirectFamilyJsonConfigurator(path));
                var v = new SettingsView();
                v.DataContext = vm;
                v.Show();

#endif
#if test4
                string path = EnvUtils.GetMyLocalAppPath("heaters.json", @"\Samolet\Config");
                var cfg = new DirectFamilyJsonConfigurator(path);

                IElementProvider ep = new HeaterProvider(cfg, doc);
                var ids = ep.GetElementIds();
                sel.SetElementIds(ids);


#endif
#if test5
                string path = EnvUtils.GetMyLocalAppPath("heaters.json", @"\Samolet\Config");
                var cfg = new DirectFamilyJsonConfigurator(path);
                var famNames = cfg.Configuration;

                var pvp = new ParameterValueProvider(new ElementId(BuiltInParameter.ELEM_FAMILY_PARAM));
                var fnrv = new FilterNumericEquals();

                var fec = new FilteredElementCollector(doc)
                    .OfClass(typeof(Family))
                    .Where(fs => famNames.Any(cfgName => cfgName.Equals(fs.Name)))
                    .Select(fs =>
                        {
                            var paramFr = new FilterElementIdRule(pvp, fnrv, fs.Id);
                            return new ElementParameterFilter(paramFr) as ElementFilter;
                        })
                    .ToList();

                //log.Debug($"filters {fec.Count}");

                var filter = new LogicalOrFilter(fec);

                var elems = new FilteredElementCollector(doc)
                    .WhereElementIsNotElementType()
                    .OfClass(typeof(FamilyInstance))
                    .OfCategory(BuiltInCategory.OST_MechanicalEquipment)
                    .Cast<FamilyInstance>()
                    .Where(e => e.GetTypeId().)


                sel.SetElementIds(elems.ToElementIds());

                //log.Debug($"Elements {elems.Count()}");

#endif
#if test6
                ElementByLevelSearch els = new ElementByLevelSearch(doc);

                var paramId = SharedParameterElement.Lookup(doc, new Guid("09aa9e39-7cfb-4bf1-ae48-00e007942ec8")).Id;
                ElementByReiserSearch ers = new ElementByReiserSearch(doc, paramId);


                string path = EnvUtils.GetMyLocalAppPath("heaters.json", @"\Samolet\Config");
                var cfg = new DirectFamilyJsonConfigurator(path);
                IElementProvider ep = new HeaterProvider(cfg, doc);

                HeaterWorker hw = new HeaterWorker(doc, ep.GetElementIds());

                var data = new DanfossHeaterParcer(new CsvReader(@"C:\Users\dev_w10\Desktop\SamWork\DanfossToRevit\Экспорт приборов.csv", 3)).GetData();

                List<ElementId> ids = new List<ElementId>();

                using (Transaction tx = new Transaction(doc))
                {
                    try
                    {
                        tx.Start("S.Danfoss test");
                        foreach (var item in data)
                        {
                            var elem = hw.HandleElement(item);
                            if (elem is null)
                            {
                                //log.Warn($"No element found for the data: {item}");
                            }
                            else
                            {
                                ids.Add(elem.Id);
                                //log.Debug($"HandleElement {item}: {elem.Name}, {elem.Id}");
                            }
                        }
                        tx.Commit();
                    }
                    catch
                    {
                        tx.RollBack();
                        throw;
                    }
                }

                sel.SetElementIds(ids);

#endif
#if test7
                var lvl = new LevelProvider(doc);
                ElementByLevelSearch els = new ElementByLevelSearch(doc);

                var paramId = SharedParameterElement.Lookup(doc, new Guid("09aa9e39-7cfb-4bf1-ae48-00e007942ec8")).Id;
                ElementByReiserSearch ers = new ElementByReiserSearch(doc, paramId);


                string path = EnvUtils.GetMyLocalAppPath("heaters.json", @"\Samolet\Config");
                var cfg = new DirectFamilyJsonConfigurator(path);
                IElementProvider ep = new HeaterProvider(cfg, doc);

                HeaterWorker hw = new HeaterWorker(doc, ep.GetElementIds(), els, ers);

                var data = new DanfossHeaterParcer(new CsvReader(@"C:\Users\dev_w10\Desktop\SamWork\DanfossToRevit\Экспорт приборов.csv", 3), lvl).GetData();

                HeaterWindow v = new HeaterWindow();

                var lvls = new FilteredElementCollector(doc)
                    .WhereElementIsNotElementType()
                    .OfClass(typeof(Level))
                    .Cast<Level>()
                    .OrderBy(l => l.Elevation)
                    .ToList();

                //log.Debug($"Levels count {lvls.Count}");

                v.DataContext = new HeaterCheckVM(lvls, ref data);
                v.Show();
#endif
#if test8

                var v = new MainWindow();
                v.DataContext = new MainViewModel(doc);
                v.Show();

#endif
#if test9

                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDia//log.Filter = "CSV files (*.csv;*.txt)|*.csv;*.txt|All files (*.*)|*.*";
                if (openFileDia//log.ShowDialog() == true)
                {
                    MessageBox.Show(openFileDia//log.FileName);
                }


#endif
#if test10
                var @ref = sel.PickObject(ObjectType.Element);
                var elem = doc.GetElement(@ref);

                var ids = elem.GetValidTypes();


                var typeId = elem.GetTypeId();

                var fs = doc.GetElement(typeId) as FamilySymbol;

                //log.Debug($"Element: {fs.Name} - {fs.FamilyName}");


                //var fec = new FilteredElementCollector(doc, ids)
                //    .WhereElementIsElementType();

                var fec = new FilteredElementCollector(doc)
                    .WherePasses(new FamilySymbolFilter(fs.Family.Id))
                    .WhereElementIsElementType();

                foreach (var f in fec)
                {
                    //log.Debug($"{f?.Name}");
                }

#endif
#if test11

#endif
#if test12

#endif

                #endregion
                mainWatch.Stop();
                var time = mainWatch.ElapsedMilliseconds;
                //log.Info($"Main command processing time: {time.GetElapsedTime()}");

                //log.Debug("------------ HeaterTestCommand ends ------------\n\n");
                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                //log.Error("Error", ex);
                //log.Debug("------------ HeaterTestCommand ends with error ------------\n\n");
                return Result.Failed;
            }
        }
    }
}
