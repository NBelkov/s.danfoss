﻿#define test3
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using S.Danfoss.CommonTools.Utils;
using S.Danfoss.CommonTools.Xtensions;
using S.Danfoss.HeaterParamImporter;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace S.Danfoss.HeaterParamImporter
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    [Autodesk.Revit.Attributes.Regeneration(Autodesk.Revit.Attributes.RegenerationOption.Manual)]
    class HeaterSettingsCommand : IExternalCommand
    {
        //protected readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        UIApplication uiapp = null;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            uiapp = commandData.Application;
            UIDocument uidoc = uiapp.ActiveUIDocument;
            Document doc = uidoc.Document;
            Selection sel = uidoc.Selection;


            try
            {
                //log.Debug("------------ HeaterTestCommand begin ------------\n");
                var mainWatch = Stopwatch.StartNew();
                #region Time measure 

#if test3
                string path = EnvUtils.GetMyLocalAppPath("heaters.json", @"\Samolet\Config");

                var vm = new SettingsVM(doc, new DirectFamilyJsonConfigurator(path));//подключение к исполняемому C# коду XAML разметки
                var v = new SettingsView();
                v.DataContext = vm;
                v.Show();

#endif
                #endregion
                mainWatch.Stop();
                var time = mainWatch.ElapsedMilliseconds;
                //log.Info($"Main command processing time: {time.GetElapsedTime()}");

                //log.Debug("------------ HeaterTestCommand ends ------------\n\n");
                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                //log.Error("Error", ex);
                //log.Debug("------------ HeaterTestCommand ends with error ------------\n\n");
                return Result.Failed;
            }
        }
    }
}
