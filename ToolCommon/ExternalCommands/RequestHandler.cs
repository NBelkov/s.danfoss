﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.ExternalCommands
{
    public class RequestHandler : IExternalEventHandler
    {
        public void Execute(UIApplication app)
        {
            Request.HandleRequest();
        }

        public string GetName()
        {
            return Request.Name;
        }
    }

    public class ParametrisedRequestHandler : IExternalEventHandler
    {
        private Action action;
        private string name;
        public ParametrisedRequestHandler(Action request, string requestName)
        {
            action = request;
            name = requestName;
        }
        public void Execute(UIApplication app) => action();
        public string GetName() => name;

    }
}
