﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace S.Danfoss.CommonTools
{
    public abstract class UIRibbonItem
    {
        protected UIControlledApplication application;
        protected string path;
        protected string location;
        protected string pathCurrent;
        protected string tabName;
        protected string panelName;

        protected readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        public UIRibbonItem(UIControlledApplication application, string tabName)
        {
            this.application = application ?? throw new ArgumentNullException("application");
            this.tabName = tabName ?? throw new ArgumentNullException("tabName");
        }

        protected abstract void CreateRibbonItems();

        /// <summary>
        /// Get image from resources
        /// </summary>
        /// <param name="bm"> Pointer to resource </param>
        /// <returns></returns>
        protected System.Windows.Media.Imaging.BitmapSource GetImage(IntPtr bm)
        {
            System.Windows.Media.Imaging.BitmapSource bmSource
              = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bm,
                IntPtr.Zero,
                System.Windows.Int32Rect.Empty,
                System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

            return bmSource;
        }

        /// <summary>
        /// Check if the Ribbon Tab with a given Name exists in current instanсe of application
        /// </summary>
        /// <param name="tabName"> Name of Ribbon Panel </param>
        /// <returns> If Tab exists <returns>
        protected bool TabExists(string tabName)
        {
            foreach (Autodesk.Windows.RibbonTab tab in Autodesk.Windows.ComponentManager.Ribbon.Tabs)
            {
                if (tab.Title.Equals(tabName))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Check if the Ribbon Panel with target Name exists on the Tab
        /// </summary>
        /// <param name="panelName"> Name of the Ribbon Panel </param>
        /// <param name="tabName"> Name of the target Ribbon Tab </param>
        /// <returns> If Panel exists on the Tab return true </returns>
        protected bool PanelExists(string tabName, string panelName)
        {
            foreach (Autodesk.Revit.UI.RibbonPanel panel in application.GetRibbonPanels(tabName))
            {
                if (panel.Name.Equals(panelName))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Create Ribbon Panel on Tab, if Tab does not exists it will be created
        /// </summary>
        /// <param name="panelName"> Name of the Ribbon Panel </param>
        /// <param name="tabName"> Name of the target Ribbon Tab </param>
        /// <returns> New or existed Panel </returns>
        public Autodesk.Revit.UI.RibbonPanel CreatePanel(string tabName, string panelName)
       {
            if (!TabExists(tabName)) application.CreateRibbonTab(tabName); // If Tab does not exist create it
            if (!PanelExists(tabName, panelName)) return application.CreateRibbonPanel(tabName, panelName); // If Pannel does not exist create it
            return application.GetRibbonPanels(tabName).Where(p => p.Name.Equals(panelName)).FirstOrDefault(); // If panel has been already created find it 
        }


    }
}
