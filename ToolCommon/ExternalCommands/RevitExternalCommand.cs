﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using S.Danfoss.CommonTools.Xtensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.ExternalCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    [Autodesk.Revit.Attributes.Regeneration(Autodesk.Revit.Attributes.RegenerationOption.Manual)]
    public abstract class RevitExternalCommand : IExternalCommand
    {
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected UIApplication uiapp;
        protected UIDocument uidoc;
        protected Document doc;
        protected Selection sel;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var cmdName = GetType().Name;

            uiapp = commandData.Application;
            uidoc = uiapp.ActiveUIDocument;
            doc = uidoc.Document;
            sel = uidoc.Selection;

            try
            {
                log.Debug($"------------ '{cmdName}' begin ------------");
                var mainWatch = Stopwatch.StartNew();
                #region Time measure 

                var result = CommandBody(ref message);

                #endregion
                mainWatch.Stop();
                var time = mainWatch.ElapsedMilliseconds;
                log.Info($"Processing time: {time.GetElapsedTime()} ({time} ms)");

                log.Debug($"------------ '{cmdName}' ends ------------\n\n");
                return result;
            }
            catch (Exception ex)
            {
                log.Error("Error", ex);
                log.Debug($"------------ '{cmdName}' ends with error ------------\n\n");
                return Result.Failed;
            }
        }

        protected abstract Result CommandBody(ref string msg);
    }
}
