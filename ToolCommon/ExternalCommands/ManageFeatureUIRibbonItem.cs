﻿using Autodesk.Revit.UI;
using S.Danfoss.CommonTools.Utils.SettingsUtils;
using System;

namespace S.Danfoss.CommonTools
{
    public abstract class ManageFeatureUIRibbonItem : UIRibbonItem
    {
        protected IFeatureManager featureManager;
        public ManageFeatureUIRibbonItem(UIControlledApplication application, string tabName, IFeatureManager featureManager) : base(application, tabName)
        {
            this.featureManager = featureManager ?? throw new ArgumentNullException("featureManager");
        }
    }
}
