﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.ExternalCommands
{
    public interface IExternalEventProvider : IExternalEventHandler
    {
        void SetExternalEvent(ExternalEvent externalEvent);
        void MakeRequest(Action request, string requestName);
    }

    public class ExternalHandler : IExternalEventProvider
    {
        private Action _request;
        private ExternalEvent _externalEvent;
        private string name;

        /// <summary>
        /// Make the request
        /// </summary>
        /// <param name="request">void Action that takes no parameters</param>
        /// <param name="requestName">Name of the request. Need for Revit</param>
        public void MakeRequest(Action request, string requestName)
        {
            _request = request;
            name = requestName;
            _externalEvent?.Raise();
        }

        public void Execute(UIApplication app)
        {
            _request?.Invoke();
        }

        public string GetName() => name;

        public void SetExternalEvent(ExternalEvent externalEvent)
        {
            _externalEvent = externalEvent;
        }
    }

}
