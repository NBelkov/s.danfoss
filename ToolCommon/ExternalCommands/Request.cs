﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.ExternalCommands
{
    public static class Request
    {
        private static Action _request;
        internal static string Name { get; private set; }

        /// <summary>
        /// Make the request
        /// </summary>
        /// <param name="request">void Action that takes no parameters</param>
        /// <param name="requestName">Name of the request. Need for Revit</param>
        public static void MakeRequest(Action request, string requestName)
        {
            _request = request;
            Name = requestName;
        }

        /// <summary>
        /// HandleRequest method must be executed inside IExternalEventHandler
        /// onley 
        /// </summary>
        internal static void HandleRequest()
        {
            _request?.Invoke();
        }
    }
}
