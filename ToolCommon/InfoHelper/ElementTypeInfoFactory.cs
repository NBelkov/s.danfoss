﻿using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class ElementTypeInfoFactory : TypeInfoAbstractFactory
    {
        public ElementTypeInfoFactory(IParameterInfoFactory parameterInfoFactory)
            : base(parameterInfoFactory)
        {
        }

        public override IItemInfo GetItemInfo(Element e)
        {
            ElementId typeId = e.GetTypeId();
            if (typeId == ElementId.InvalidElementId) return new TypeInfo() {Parameters = new List<IParameterInfo>() };

            Element type = e.Document.GetElement(typeId);
            return base.GetItemInfo(type);
        }
    }
}
