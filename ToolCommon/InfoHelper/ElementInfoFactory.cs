﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using S.Danfoss.CommonTools.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class ElementInfoFactory : IItemInfoFactory
    {
        protected enum RebarType
        {
            perUnitLength,
            straightBars,
            bendingDetails
        }
        protected ElementTypeInfoFactory etf;
        protected IParameterInfoFactory pif;
        protected IMaterialInfoFactory mifPaint;
        protected IMaterialInfoFactory mifVolume;
        protected string dateTime = DateHelper.NowDateTimeAsString;


        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ElementInfoFactory(IParameterInfoFactory parameterInfoFactory)
        {
#if DEBUG
            log.Debug("ElementInfoFactory init");
#endif
            pif = parameterInfoFactory;
            etf = new ElementTypeInfoFactory(pif);
            mifPaint = new PaintMaterialInfoFactory(pif);
            mifVolume = new VolumeMaterialInfoFactory(pif);
        }

        public IItemInfo GetItemInfo(Element e)
        {
            try
            {
#if DEBUG
                log.Debug($"Getting info from {e?.Name} - {e?.UniqueId}; {e.GetType()}");
#endif
                Document doc = e.Document;
                switch (e)
                {
                    case RebarInSystem r:
                        {
                            return new ElementInfo()
                            {
                                Name = e.Name,
                                UniqueId = e.UniqueId,
                                FileName = doc.Title,
                                Parameters = GetRebarParameters(r),
                                Date = dateTime,
                                TypeParameters = new List<IParameterInfo>(),
                                VolumeMateriales = mifVolume?.GetMaterialInfos(e),
                                PaintMateriales = mifPaint?.GetMaterialInfos(e)
                            };
                        }
                    case Rebar r:
                        {
                            return new ElementInfo()
                            {
                                Name = e.Name,
                                UniqueId = e.UniqueId,
                                FileName = doc.Title,
                                Date = dateTime,
                                Parameters =  GetRebarParameters(r),
                                TypeParameters = new List<IParameterInfo>(),
                                VolumeMateriales = mifVolume?.GetMaterialInfos(e),
                                PaintMateriales = mifPaint?.GetMaterialInfos(e)
                            };
                        }
                    case FamilyInstance fi:
                        {
                            return new FamilyInstanceInfo()
                            {
                                Name = fi.Name,
                                UniqueId = fi.UniqueId,
                                FileName = doc.Title,
                                Date = dateTime,
                                Parameters = pif?.GetParameterInfos(e),
                                TypeParameters = etf?.GetItemInfo(e).Parameters,
                                ParentId = GetParentId(fi),
                                //SubcomponentsIds = GetSubcomponentsIds(fi),
                                VolumeMateriales = mifVolume?.GetMaterialInfos(e),
                                PaintMateriales = mifPaint?.GetMaterialInfos(e)
                            };
                        }
                    default:
                        {
                            return new ElementInfo()
                            {
                                Name = e.Name,
                                UniqueId = e.UniqueId,
                                FileName = doc.Title,
                                Date = dateTime,
                                Parameters = pif?.GetParameterInfos(e),
                                TypeParameters = etf.GetItemInfo(e)?.Parameters,
                                VolumeMateriales = mifVolume?.GetMaterialInfos(e),
                                PaintMateriales = mifPaint?.GetMaterialInfos(e)
                            };
                        }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ElementInfoFactory", ex);
                throw;
            }
        }

        public IEnumerable<IItemInfo> GetItemInfos(IEnumerable<Element> elements)
        {
            if (elements is null || !elements.Any()) return new List<IItemInfo>();
            var result = new List<IItemInfo>();
            foreach (var e in elements)
            {
                result.Add(GetItemInfo(e));
            }
            return result;
        }

        protected string GetParentId(FamilyInstance family)
        {
            if (family is null) return string.Empty;
            var sup = family.SuperComponent;
            if (sup is null) return string.Empty;
            return sup.UniqueId;
        }

        protected IEnumerable<IParameterInfo> GetRebarParameters(Rebar r)
        {

//#if DEBUG
//            row["Class"] += r.ToString();
//#endif

            var allParameters = GetParameterSet(r);

            RebarType type = RebarType.straightBars;
            double coefficient = 1.0;
            var totalLength = GetRightTotalLength(allParameters, out type, out coefficient);

            if (totalLength == 0)
            {
                for (int i = r.IncludeFirstBar ? 0 : 1; i < r.NumberOfBarPositions - (r.IncludeLastBar ? 0 : 1); i++)
                {
                    ElementId paramId = new ElementId(BuiltInParameter.REBAR_ELEM_LENGTH);
                    var dpv = r.GetParameterValueAtIndex(paramId, i) as DoubleParameterValue;
                    var lng = UnitUtils.ConvertFromInternalUnits(dpv.Value, DisplayUnitType.DUT_MILLIMETERS);
                    totalLength += HandleLengthByRule(lng, type);
                }
                totalLength = totalLength * coefficient;
            }

            var totalLengthParam = allParameters.Where(p => p.Name.Equals("REBAR_ELEM_TOTAL_LENGTH")).FirstOrDefault();

            //#if DEBUG
            var massPerMeter = allParameters.Where(p => p.Name.Equals("ADSK_Масса на единицу длины")).FirstOrDefault() as DoubleParamInfo;
            var totalWeight = allParameters.Where(p => p.Name.Equals("EXTRACTOR_TOTAL_WEIGHT_KILOGRAM")).FirstOrDefault() as DoubleParamInfo;
            //#endif

            if (totalLengthParam is DoubleParamInfo dp)
            {
                dp.Value = totalLength;
                totalWeight.Value = massPerMeter.Value * totalLength / 1000;
//#if DEBUG
//                row[totalWeight.Name] = totalWeight.Value;
//                row[massPerMeter.Name] = massPerMeter.Value;
//                row[totalLengthParam.Name] = dp.Value;
//#endif
            }
            else
            {
                throw new Exception($"Cannot determine bar's length {r}, Id {r.Id}");
            }

            return allParameters;
        }

        protected IEnumerable<IParameterInfo> GetRebarParameters(RebarInSystem r)
        {

            var allParameters = GetParameterSet(r);

            RebarType type = RebarType.straightBars;
            double coefficient = 1.0;
            var totalLength = GetRightTotalLength(allParameters, out type, out coefficient);

            if (totalLength == 0.0)
            {
                throw new Exception($"Cannot determine bar length for RebarInSystem");
            }

            var totalLengthParam = allParameters.Where(p => p.Name.Equals("REBAR_ELEM_TOTAL_LENGTH")).FirstOrDefault();

            //#if DEBUG
            var massPerMeter = allParameters.Where(p => p.Name.Equals("ADSK_Масса на единицу длины")).FirstOrDefault() as DoubleParamInfo;
            var totalWeight = allParameters.Where(p => p.Name.Equals("EXTRACTOR_TOTAL_WEIGHT_KILOGRAM")).FirstOrDefault() as DoubleParamInfo;
            //#endif

            if (totalLength != 0 && totalLengthParam is DoubleParamInfo dp)
            {
                dp.Value = totalLength;
                totalWeight.Value = massPerMeter.Value * totalLength / 1000;
//#if DEBUG
//                row[totalWeight.Name] = totalWeight.Value;
//                row[massPerMeter.Name] = massPerMeter.Value;
//                row[totalLengthParam.Name] = dp.Value;

//#endif
            }
            else
            {
                throw new Exception($"Cannot determine bar's length {r}, Id {r.Id}");
            }

            return allParameters;
        }

        protected IEnumerable<IParameterInfo> GetParameterSet(Element e)
        {
            HashSet<IParameterInfo> allParameters = new HashSet<IParameterInfo>();
            allParameters.UnionWith(pif.GetParameterInfos(e));
            allParameters.UnionWith(etf.GetItemInfo(e).Parameters);
            allParameters.Add(new DoubleParamInfo() { Name = "EXTRACTOR_TOTAL_WEIGHT_KILOGRAM" });

            return allParameters.OrderBy(p => p.Name);
        }

        protected double HandleLengthByRule(double value, RebarType type)
        {
            switch (type)
            {
                case RebarType.perUnitLength:
                    {
#if DEBUG
                        log.Debug($"{type}: {value}");
#endif
                        return value;
                    }
                case RebarType.straightBars:
                    {
#if DEBUG
                        log.Debug($"{type}: round({value}, 5) = {Math.Round(value / 5) * 5}");
#endif
                        return Math.Round(value / 5) * 5;
                    }
                case RebarType.bendingDetails:
                    {
#if DEBUG
                        log.Debug($"{type}: round({value}) =  {Math.Round(value)}");
#endif
                        return Math.Round(value);
                    }
                default:
                    {
#if DEBUG
                        log.Warn($"{type}: {value} - unknown rebar type");
#endif
                        return value;
                    }
            }
        }

        protected double GetRightTotalLength(IEnumerable<IParameterInfo> allParameters, out RebarType type, out double coefficient)
        {
            type = RebarType.perUnitLength;
            var detaleType = RebarType.straightBars;
            int barCount = 0;
            double barLength = 0.0;
            coefficient = 1.0;

            foreach (var param in allParameters)
            {
                switch (param.Name)
                {
                    case "REBAR_ELEM_LENGTH":
                        {
                            barLength = (param as DoubleParamInfo).Value;
                            break;
                        }
                    case "REBAR_ELEM_QUANTITY_OF_BARS":
                        {
                            barCount = (param as IntParamInfo).Value;
                            break;
                        }
                    case "ADSK_Размер в погонных метрах":
                        {
                            if (param is IntParamInfo p)
                            {
                                type = p.Value == 0 ? RebarType.straightBars : RebarType.perUnitLength;
//#if DEBUG
//                                row[p.Name] = p.Value;
//#endif
                            }
                            break;
                        }
                    case "ADSK_Форма арматуры":
                        {
                            if (param is IntParamInfo p)
                            {
                                detaleType = p.Value == 1 ? RebarType.straightBars : RebarType.bendingDetails;
//#if DEBUG
//                                row[p.Name] = p.Value;
//#endif
                            }
                            break;
                        }
                    case "АРМ_Коэфф нахлеста растянутого стержня":
                        {
                            if (param is DoubleParamInfo p)
                            {
                                coefficient = p.Value == 0 ? 1 : p.Value;
//#if DEBUG
//                                row[p.Name] = p.Value;
//#endif
                            }
                            break;
                        }
//#if DEBUG
//                    case "ADSK_Код металлопроката":
//                        {
//                            if (param is DoubleParamInfo p)
//                            {
//                                row[p.Name] = p.Value;
//                            }
//                            break;
//                        }
//                    case "REBAR_BAR_DIAMETER":
//                        {
//                            if (param is DoubleParamInfo p)
//                            {
//                                row[p.Name] = p.Value;
//                            }
//                            break;
//                        }
//                    case "АРМ_Материал основы":
//                        {
//                            if (param is DoubleParamInfo p)
//                            {
//                                row[p.Name] = p.Value;
//                            }
//                            break;
//                        }
//                    case "ADSK_Деталь_Префикс":
//                        {
//                            if (param is StringParamInfo p)
//                            {
//                                row["Поз"] += p.Value;
//                            }
//                            break;
//                        }
//                    case "ADSK_Позиция":
//                        {
//                            if (param is StringParamInfo p)
//                            {

//                                row["Поз"] += p.Value;

//                            }
//                            break;
//                        }
//                    case "Масса пог.м.":
//                        {
//                            if (param is StringParamInfo p)
//                            {
//                                row[p.Name] = p.Value;
//                            }
//                            break;
//                        }
//#endif
                    default:
                        break;
                }
            }


            if (type != RebarType.perUnitLength) type = detaleType;

            if (barCount > 0 && barLength > 0)
                return coefficient * HandleLengthByRule(barLength, type) * barCount;
            else
                return 0;
        }
    }


    public class ParameterizedElementFactory : IItemInfoFactory 
    {

        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected Dictionary<Type, IElementHandler> h = new Dictionary<Type, IElementHandler>();

        public ParameterizedElementFactory(Dictionary<Type, IElementHandler> handlers)
        {
#if DEBUG
            log.Debug("ParameterizedElementFactory init");
#endif
            if (handlers.ContainsKey(typeof(Element))) h = handlers;
            else throw new ArgumentException($"handlers should contains default ElementHandler: handlers[typeof(element)] = new ElementHandler", "Dictionary<Type, IElementHandler> handlers"); 
        }

        public IItemInfo GetItemInfo(Element e)
        {
            var type = e.GetType();
            if (h.ContainsKey(type)) return h[type].HandleElement(e);
            else return h[typeof(Element)].HandleElement(e);
        }

        public IEnumerable<IItemInfo> GetItemInfos(IEnumerable<Element> elements)
        {
            if (elements is null || !elements.Any()) return new List<IItemInfo>();
            var result = new List<IItemInfo>();
            foreach (var e in elements)
            {
                result.Add(GetItemInfo(e));
            }
            return result;
        }
    }
}
