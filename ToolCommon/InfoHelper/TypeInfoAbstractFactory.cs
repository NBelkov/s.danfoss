﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class TypeInfoAbstractFactory : IItemInfoFactory
    {
        protected Dictionary<int, IItemInfo> typesDict = new Dictionary<int, IItemInfo>();
        protected IParameterInfoFactory pif;
        public TypeInfoAbstractFactory(IParameterInfoFactory parameterInfoFactory)
        {
            pif = parameterInfoFactory;
        }
        public IEnumerable<IItemInfo> GetItemInfos(IEnumerable<Element> elements)
        {
            var result = new List<IItemInfo>();
            foreach (var e in elements)
            {
                result.Add(GetItemInfo(e));
            }
            return result;
        }

        public virtual IItemInfo GetItemInfo(Element e)
        {
            int uId = e.Id.IntegerValue;
            if (typesDict.ContainsKey(uId)) return typesDict[uId];

            var typeInfo = new TypeInfo()
            {
                Name = e.Name,
                UniqueId = e.UniqueId,
                Parameters = pif.GetParameterInfos(e)
            };
            typesDict[uId] = typeInfo;
            return typeInfo;
        }
    }
}
