﻿using Autodesk.Revit.DB;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class MaterialTypeInfoFactory : TypeInfoAbstractFactory
    {
        public MaterialTypeInfoFactory(IParameterInfoFactory parameterInfoFactory)
            : base(parameterInfoFactory)
        {
        }

        public override IItemInfo GetItemInfo(Element e)
        {
            if (e is Material)
            {
                return base.GetItemInfo(e);
            }
            else
            {
                return null;
            }
        }
    }
}
