﻿using Autodesk.Revit.DB;

namespace S.Danfoss.CommonTools.InfoHelper
{
    //public class GeomElementFactory : IGeometryFactory
    //{
    //    protected Options opt;

    //    public enum RetrivingWay
    //    {
    //        origin,
    //        modified
    //    }

    //    public GeomElementFactory()
    //    {
    //        opt = new Options
    //        {
    //            ComputeReferences = true
    //        };
    //    }

    //    public GeometryObject Retrieve(Element e) => Retrieve(e, RetrivingWay.modified);
    //    public GeometryObject Retrieve(Element e, RetrivingWay way)
    //    {
    //        switch (way)
    //        {
    //            case RetrivingWay.origin:
    //                FamilyInstance fi = e as FamilyInstance;
    //                if (fi != null)
    //                {
    //                    GeometryElement ge = fi.GetOriginalGeometry(opt);
    //                    var ge_t = ge.GetTransformed(fi.GetTransform());
    //                    return GetSolid(ge_t);
    //                }
    //                else return GetSolid(e.get_Geometry(opt));
    //            default:
    //                return GetSolid(e.get_Geometry(opt));
    //        }
    //    }
    //    protected GeometryObject GetSolid(GeometryElement geomElem)
    //    {
    //        if (geomElem is null) return null;

    //        Solid globalSolid = null;
    //        foreach (GeometryObject geomObj in geomElem)
    //        {
    //            switch (geomObj)
    //            {
    //                case Solid go:
    //                    {
    //                        if (go.Volume == 0) continue;
    //                        if (globalSolid == null) globalSolid = go;
    //                        else globalSolid = BooleanOperationsUtils.ExecuteBooleanOperation(globalSolid, go, BooleanOperationsType.Union);
    //                        continue;
    //                    }
    //                    case GeometryInstance go:
    //                    {
    //                        GeometryElement transformedGeomElem = go.GetInstanceGeometry();
    //                        globalSolid = GetSolid(transformedGeomElem);
    //                    }
    //                default:
    //                    continue;
    //            }

    //        }
    //        foreach (GeometryObject geomObj in geomElem)
    //        {
    //            Solid solid = geomObj as Solid;
    //            if (null != solid && solid.Volume != 0)
    //            {
    //                if (globalSolid == null) globalSolid = solid;
    //                else globalSolid = BooleanOperationsUtils.ExecuteBooleanOperation(globalSolid, solid, BooleanOperationsType.Union);
    //                continue;
    //            }

    //            GeometryInstance geomInst = geomObj as GeometryInstance;
    //            if (null != geomInst)
    //            {
    //                GeometryElement transformedGeomElem = geomInst.GetInstanceGeometry();
    //                globalSolid = GetSolid(transformedGeomElem);
    //            }
    //        }
    //        return globalSolid;
    //    }

    //}

    public class SolidFactory : IGeometryFactory
    {
        protected Options opt;

        public SolidFactory()
        {
            opt = new Options{ComputeReferences = true};
        }

        public enum RetrivingWay
        {
            origin,
            modified
        }

        public GeometryObject Retrieve(Element e) => Retrieve(e, RetrivingWay.modified);
        public GeometryObject Retrieve(Element e, RetrivingWay way)
        {
            switch (way)
            {
                case RetrivingWay.origin:
                    FamilyInstance fi = e as FamilyInstance;
                    if (fi != null)
                    {
                        GeometryElement ge = fi.GetOriginalGeometry(opt);
                        var ge_t = ge.GetTransformed(fi.GetTransform());
                        return GetSolid(ge_t);
                    }
                    else return GetSolid(e.get_Geometry(opt));
                default:
                    return GetSolid(e.get_Geometry(opt));
            }
        }

        protected Solid GetSolid(GeometryElement geomElem)
        {
            if (geomElem is null) return null;

            Solid globalSolid = null;
            foreach (GeometryObject geomObj in geomElem)
            {
                Solid solid = geomObj as Solid;
                if (null != solid && solid.Volume != 0)
                {
                    if (globalSolid == null) globalSolid = solid;
                    else globalSolid = BooleanOperationsUtils.ExecuteBooleanOperation(globalSolid, solid, BooleanOperationsType.Union);
                    continue;
                }

                GeometryInstance geomInst = geomObj as GeometryInstance;
                if (null != geomInst)
                {
                    GeometryElement transformedGeomElem = geomInst.GetInstanceGeometry();
                    globalSolid = GetSolid(transformedGeomElem);
                }
            }
            return globalSolid;
        }
    }
}
