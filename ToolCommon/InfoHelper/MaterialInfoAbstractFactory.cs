﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public abstract class MaterialInfoAbstractFactory : IMaterialInfoFactory
    {
        protected Element currentElement;
        protected IParameterInfoFactory pif;
        protected MaterialTypeInfoFactory mtf;
        public MaterialInfoAbstractFactory(IParameterInfoFactory parameterInfoFactory)
        {
            pif = parameterInfoFactory;
            mtf = new MaterialTypeInfoFactory(pif); 
        }

        public IEnumerable<IItemInfo> GetMaterialInfos(Element element)
        {
            currentElement = element;
            Document doc = currentElement.Document;

            var mainMatsIds = RetrieveMaterialFromElement(element);
            if (mainMatsIds.Any())
            {
                return new FilteredElementCollector(doc, mainMatsIds)
                .WhereElementIsNotElementType()
                .Cast<Material>()
                .Select(m => GetMaterialInfo(m))
                .ToList();
            }
            return new List<IItemInfo>();
        }
        protected abstract ICollection<ElementId> RetrieveMaterialFromElement(Element e);
        protected abstract IItemInfo GetMaterialInfo(Material material);

        protected abstract double GetArea(ElementId id);
    }
}
