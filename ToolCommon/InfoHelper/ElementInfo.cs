﻿using S.Danfoss.CommonTools.JsonConverters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.InfoHelper
{
    [JsonConverter(typeof(ElementInfoConverter))]
    public class ElementInfo : IItemInfo
    {
        public string Name { get; set; }
        public string UniqueId { get; set; }
        public string FileName { get; set; }
        public string Date { get; set; }
        public IEnumerable<IItemInfo> VolumeMateriales { get; set; } = new List<IItemInfo>();
        public IEnumerable<IItemInfo> PaintMateriales { get; set; } = new List<IItemInfo>();
        public IEnumerable<IParameterInfo> Parameters { get; set; } = new List<IParameterInfo>();
        public IEnumerable<IParameterInfo> TypeParameters { get; set; } = new List<IParameterInfo>();
    }

    [JsonConverter(typeof(FamilyInstanceConverter))]
    public class FamilyInstanceInfo : ElementInfo
    {
        public string ParentId { get; set; }
        //public IEnumerable<int> SubcomponentsIds { get; set; }
    }

    //public class RebarInfo : ElementInfo
}
