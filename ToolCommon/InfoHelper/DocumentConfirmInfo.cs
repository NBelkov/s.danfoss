﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.InfoHelper
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DocumentConfirmInfo : IItemInfo
    {

        //[JsonProperty("file_Name")]
        [JsonProperty("fileName")]
        public string Name { get; set; }

        [JsonProperty("number")]
        public int ElementsCount { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        public string UniqueId => null;

        public IEnumerable<IParameterInfo> Parameters => null;
    }
}
