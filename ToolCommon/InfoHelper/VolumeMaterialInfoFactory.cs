﻿using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class VolumeMaterialInfoFactory : MaterialInfoAbstractFactory
    {
        public VolumeMaterialInfoFactory(IParameterInfoFactory parameterInfoFactory)
            : base(parameterInfoFactory)
        {
        }

        protected override double GetArea(ElementId id)
        {
            var internalVal = currentElement.GetMaterialArea(id, false);
            return (double)UnitUtils.ConvertFromInternalUnits(internalVal, DisplayUnitType.DUT_SQUARE_METERS);
        }
        private double GetVolume(ElementId id)
        {
            var internalVal = currentElement.GetMaterialVolume(id);
            return (double)UnitUtils.ConvertFromInternalUnits(internalVal, DisplayUnitType.DUT_CUBIC_METERS);
        }

        protected override IItemInfo GetMaterialInfo(Material material)
        {
            var type = mtf.GetItemInfo(material);
            return new VolumeMaterialInfo()
            {
                Name = type.Name,
                UniqueId = type.UniqueId,
                Parameters = type.Parameters,
                Area = (float)GetArea(material.Id),
                Volume = (float)GetVolume(material.Id)
            };
        }

        protected override ICollection<ElementId> RetrieveMaterialFromElement(Element e) => e.GetMaterialIds(false);
    }
}
