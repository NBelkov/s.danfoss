﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.InfoHelper
{
    [JsonObject(MemberSerialization.OptIn)]
    public class TypeInfo : IItemInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("uniqueId")]
        public string UniqueId { get; set; }
        [JsonProperty("parameters")]
        public IEnumerable<IParameterInfo> Parameters { get; set; } = new List<IParameterInfo>();
    }
}
