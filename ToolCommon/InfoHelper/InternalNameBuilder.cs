﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.Xtensions;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class InternalNameBuilder : INameBuilder
    {
        public string GetName(Parameter parameter)
        {
            if (parameter.IsShared)
            {
                return parameter.Definition.Name;
            }
            var definition = parameter.Definition as InternalDefinition;
            var par = definition.BuiltInParameter;
            if (par == BuiltInParameter.INVALID) return definition.Name;
            return definition.BuiltInParameter.ToStringFast();
        }
    }
}
