﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.InfoHelper
{
    [JsonObject(MemberSerialization.OptIn)]
    public class PaintMaterialInfo : IItemInfo
    {
        [JsonProperty("Name")]
        public virtual string Name { get; set; }

        [JsonProperty("Uniqueid")]
        public string UniqueId { get; set; }

        [JsonProperty("IsPaintMaterial")]
        public virtual bool IsPaintMaterial => true;

        [JsonProperty("Area")]
        public float Area { get; set; }

        [JsonProperty("Parameters")]
        public IEnumerable<IParameterInfo> Parameters { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class VolumeMaterialInfo : PaintMaterialInfo
    {

        [JsonProperty("IsPaintMaterial")]
        public override bool IsPaintMaterial => false;

        [JsonProperty("Volume")]
        public float Volume { get; set; }
    }
}
