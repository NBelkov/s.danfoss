﻿using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class PaintMaterialInfoFactory : MaterialInfoAbstractFactory
    {
        public PaintMaterialInfoFactory(IParameterInfoFactory parameterInfoFactory)
            : base(parameterInfoFactory)
        {
        }

        protected override double GetArea(ElementId id)
        {
            var internalVal = currentElement.GetMaterialArea(id, true);
            return (double)UnitUtils.ConvertFromInternalUnits(internalVal, DisplayUnitType.DUT_SQUARE_METERS);
        }

        protected override IItemInfo GetMaterialInfo(Material material)
        {
            var type = mtf.GetItemInfo(material);
            return new PaintMaterialInfo()
            {
                Name = type.Name,
                UniqueId = type.UniqueId,
                Parameters = type.Parameters,
                Area = (float)GetArea(material.Id)
            };
        }

        protected override ICollection<ElementId> RetrieveMaterialFromElement(Element e) => e.GetMaterialIds(true);

    }
}
