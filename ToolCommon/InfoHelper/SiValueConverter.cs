﻿using Autodesk.Revit.DB;
using System;


namespace S.Danfoss.CommonTools.InfoHelper
{
    public class SiValueConverter : IValueConverter
    {
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public double Convert(double value, UnitType unitTupe, DisplayUnitType displayUnitType)
        {
            switch (unitTupe)
            {
                // General types
                case UnitType.UT_Length: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Area: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_SQUARE_METERS);
                case UnitType.UT_Volume: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_CUBIC_METERS);
                case UnitType.UT_Mass: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_KILOGRAMS_MASS);
                case UnitType.UT_MassDensity: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_KILOGRAMS_MASS_PER_METER);
                case UnitType.UT_MassPerUnitArea: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_KILOGRAMS_MASS_PER_SQUARE_METER);
                case UnitType.UT_Weight: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_KILOGRAMS_FORCE);
                case UnitType.UT_Weight_per_Unit_Length: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_KILOGRAMS_FORCE_PER_METER);


                // Length types
                case UnitType.UT_PipeSize: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Electrical_CableTraySize: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Electrical_ConduitSize: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_HVAC_DuctSize: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_WireSize: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_HVAC_DuctInsulationThickness: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_HVAC_DuctLiningThickness: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_PipeInsulationThickness: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_HVAC_Roughness: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_DecSheetLength: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Reinforcement_Length: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Bar_Diameter: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Crack_Width: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Displacement_Deflection: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Reinforcement_Cover: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Reinforcement_Spacing: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Section_Dimension: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case UnitType.UT_Section_Property: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);

                // Area types
                case UnitType.UT_HVAC_CrossSection: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_SQUARE_METERS);
                case UnitType.UT_Reinforcement_Area: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_SQUARE_CENTIMETERS);

                // Volume types
                case UnitType.UT_Piping_Volume: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_LITERS);
                case UnitType.UT_Reinforcement_Volume: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_CUBIC_METERS);

                default: return (double)UnitUtils.ConvertFromInternalUnits(value, displayUnitType);
            }
        }

        public double Convert(Parameter p)
        {
            ParameterType pType = p.Definition.ParameterType;
            var value = p.AsDouble();
            //var value = CheckIfRebarLength(p);
            if (value == 0) return 0;

            switch (pType)
            {
                case ParameterType.Length: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_MILLIMETERS);
                case ParameterType.Area: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_SQUARE_METERS);
                case ParameterType.Volume: return (double)UnitUtils.ConvertFromInternalUnits(value, DisplayUnitType.DUT_CUBIC_METERS);
                default:
                    {
                        try
                        {
                            return (double)UnitUtils.ConvertFromInternalUnits(value, p.DisplayUnitType);

                        }
                        catch (Exception ex)
                        {
                            log.Error("Cannot convert parameters", ex);
                            return (double)value;
                        }
                    }
            }
        }
      

        //// handle parameters for rebar to calculate length
        //internal double CheckIfRebarLength(Parameter p)
        //{
        //    string msg = string.Empty;
        //    if (!p.IsShared)
        //    {
        //        InternalDefinition intDef = (InternalDefinition)p.Definition;
        //        if (intDef.BuiltInParameter == BuiltInParameter.REBAR_ELEM_TOTAL_LENGTH)
        //        {
        //            // get element from parameter
        //            var e = p.Element;
        //            msg += $"{e.Name}: Id {e.Id}, Unique Id {e.UniqueId}\t";


        //            // try to get one bar length and quantity of bar in set 
        //            var barLength = e.get_Parameter(BuiltInParameter.REBAR_ELEM_LENGTH);
        //            var barCount = e.get_Parameter(BuiltInParameter.REBAR_ELEM_QUANTITY_OF_BARS);

        //            var value = 0.0;
        //            if (barLength is null || barCount is null)
        //            {
        //                msg += $"barCount: {barCount}, barLength {barLength}";
        //                value = p.AsDouble();
        //            }
        //            else
        //            {
        //                var roundLength = Math.Round(barLength.AsDouble() * 304.8) / 304.8;
        //                msg += $"barCount: {barCount.AsInteger()}, barLength {barLength.AsDouble() * 304.8} -> {roundLength * 304.8}";
        //                value = roundLength * barCount.AsInteger();
        //            }


        //            OverlockStorage os = e.GetEntity<OverlockStorage>();
        //            if (os is null)
        //            {
        //                log.Warn($"Overlock storage hasn't been found. Length will be used without coefficient. {msg}");
        //                return value;
        //            }

        //            var k = os.AnchorageTensedCoefficient;
        //            var newVal = k == 0 ? value : value * k;

        //            log.Debug($"{msg}, {value * 304.8} x {k} = {newVal * 304.8} ");
        //            //log.Debug($"Rebar {p.Element.Id} total length detected: {intDef.BuiltInParameter}, {value} x {k} = {newVal} ");
        //            return newVal;
        //        }
        //        return p.AsDouble();
        //    }
        //    return p.AsDouble();
        //}
    }
}

