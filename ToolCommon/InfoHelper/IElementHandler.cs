﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Structure;
using S.Danfoss.CommonTools.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.InfoHelper
{


    public interface IElementHandler
    {
        IItemInfo HandleElement(Element e);
    }

    public class ElementHandler : IElementHandler
    {
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected TypeInfoAbstractFactory etf;
        protected IParameterInfoFactory pif;
        protected IMaterialInfoFactory mifPaint;
        protected IMaterialInfoFactory mifVolume;
        protected string dt;

        public ElementHandler(TypeInfoAbstractFactory typeInfoAbstractFactory, IParameterInfoFactory parameterInfoFactory, string dateTime)
        {
            dt = dateTime;
            etf = typeInfoAbstractFactory;
            pif = parameterInfoFactory;
            mifPaint = new PaintMaterialInfoFactory(pif);
            mifVolume = new VolumeMaterialInfoFactory(pif);
        }

        public virtual IItemInfo HandleElement(Element e)
        {
            return new ElementInfo()
            {
                Name = e?.Name,
                UniqueId = e?.UniqueId,
                FileName = e?.Document?.Title,
                Date = dt,
                Parameters = pif?.GetParameterInfos(e),
                TypeParameters = etf.GetItemInfo(e)?.Parameters,
                VolumeMateriales = mifVolume?.GetMaterialInfos(e),
                PaintMateriales = mifPaint?.GetMaterialInfos(e)
            };
        }
    }

    public class FamilyInstanceHandler : ElementHandler, IElementHandler
    {
        public FamilyInstanceHandler(TypeInfoAbstractFactory typeInfoAbstractFactory, IParameterInfoFactory parameterInfoFactory, string dateTime) : base(typeInfoAbstractFactory, parameterInfoFactory, dateTime) { }

        public override IItemInfo HandleElement(Element e)
        {
            if (e is FamilyInstance fi)
            {
                return new FamilyInstanceInfo()
                {
                    Name = e?.Name,
                    UniqueId = e?.UniqueId,
                    FileName = e?.Document?.Title,
                    Date = dt,
                    Parameters = pif?.GetParameterInfos(e),
                    TypeParameters = etf?.GetItemInfo(e).Parameters,
                    ParentId = GetParentId(fi),
                    VolumeMateriales = mifVolume?.GetMaterialInfos(e),
                    PaintMateriales = mifPaint?.GetMaterialInfos(e)
                };
            }
            else return base.HandleElement(e);
        }

        protected string GetParentId(FamilyInstance fi)
        {
            if (fi is null) return string.Empty;
            var sup = fi.SuperComponent;
            if (sup is null) return string.Empty;
            return sup.UniqueId;
        }
    }

    public class RebarHandler : ElementHandler, IElementHandler
    {
        public RebarHandler(TypeInfoAbstractFactory typeInfoAbstractFactory, IParameterInfoFactory parameterInfoFactory, string dateTime) : base(typeInfoAbstractFactory, parameterInfoFactory, dateTime) { }

        protected enum RebarType
        {
            perUnitLength,
            straightBars,
            bendingDetails
        }

        public override IItemInfo HandleElement(Element e)
        {
            Document doc = e.Document;
            switch (e)
            {
                case RebarInSystem r:
                    {
                        return new ElementInfo()
                        {
                            Name = e?.Name,
                            UniqueId = e?.UniqueId,
                            FileName = e?.Document?.Title,
                            Parameters = GetRebarParameters(r),
                            Date = dt,
                            TypeParameters = new List<IParameterInfo>(),
                            VolumeMateriales = mifVolume?.GetMaterialInfos(e),
                            PaintMateriales = mifPaint?.GetMaterialInfos(e)
                        };
                    }
                case Rebar r:
                    {
                        return new ElementInfo()
                        {
                            Name = e?.Name,
                            UniqueId = e?.UniqueId,
                            FileName = e?.Document?.Title,
                            Date = dt,
                            Parameters = GetRebarParameters(r),
                            TypeParameters = new List<IParameterInfo>(),
                            VolumeMateriales = mifVolume?.GetMaterialInfos(e),
                            PaintMateriales = mifPaint?.GetMaterialInfos(e)
                        };
                    }
                default: return base.HandleElement(e);
            }
        }

        protected IEnumerable<IParameterInfo> GetRebarParameters(Rebar r)
        {
            var allParameters = GetParameterSet(r);

            RebarType type = RebarType.straightBars;
            double coefficient = 1.0;
            var totalLength = GetRightTotalLength(allParameters, out type, out coefficient);

            if (totalLength == 0)
            {
                for (int i = r.IncludeFirstBar ? 0 : 1; i < r.NumberOfBarPositions - (r.IncludeLastBar ? 0 : 1); i++)
                {
                    ElementId paramId = new ElementId(BuiltInParameter.REBAR_ELEM_LENGTH);
                    var dpv = r.GetParameterValueAtIndex(paramId, i) as DoubleParameterValue;
                    var lng = UnitUtils.ConvertFromInternalUnits(dpv.Value, DisplayUnitType.DUT_MILLIMETERS);
                    totalLength += HandleLengthByRule(lng, type);
                }
                totalLength = totalLength * coefficient;
            }

            var totalLengthParam = allParameters.Where(p => p.Name.Equals("REBAR_ELEM_TOTAL_LENGTH")).FirstOrDefault();

            var massPerMeter = allParameters.Where(p => p.Name.Equals("ADSK_Масса на единицу длины")).FirstOrDefault() as DoubleParamInfo;
            var totalWeight = allParameters.Where(p => p.Name.Equals("EXTRACTOR_TOTAL_WEIGHT_KILOGRAM")).FirstOrDefault() as DoubleParamInfo;

            if (totalLengthParam is DoubleParamInfo dp)
            {
                dp.Value = totalLength;
                totalWeight.Value = massPerMeter.Value * totalLength / 1000;
            }
            else
            {
                throw new Exception($"Cannot determine bar's length {r}, Id {r.Id}");
            }

            return allParameters;
        }

        protected IEnumerable<IParameterInfo> GetRebarParameters(RebarInSystem r)
        {

            var allParameters = GetParameterSet(r);

            RebarType type = RebarType.straightBars;
            double coefficient = 1.0;
            var totalLength = GetRightTotalLength(allParameters, out type, out coefficient);

            if (totalLength == 0.0)
            {
                throw new Exception($"Cannot determine bar length for RebarInSystem");
            }

            var totalLengthParam = allParameters.Where(p => p.Name.Equals("REBAR_ELEM_TOTAL_LENGTH")).FirstOrDefault();

            var massPerMeter = allParameters.Where(p => p.Name.Equals("ADSK_Масса на единицу длины")).FirstOrDefault() as DoubleParamInfo;
            var totalWeight = allParameters.Where(p => p.Name.Equals("EXTRACTOR_TOTAL_WEIGHT_KILOGRAM")).FirstOrDefault() as DoubleParamInfo;

            if (totalLength != 0 && totalLengthParam is DoubleParamInfo dp)
            {
                dp.Value = totalLength;
                totalWeight.Value = massPerMeter.Value * totalLength / 1000;
            }
            else
            {
                throw new Exception($"Cannot determine bar's length {r}, Id {r.Id}");
            }

            return allParameters;
        }

        protected IEnumerable<IParameterInfo> GetParameterSet(Element e)
        {
            HashSet<IParameterInfo> allParameters = new HashSet<IParameterInfo>();
            allParameters.UnionWith(pif.GetParameterInfos(e));
            allParameters.UnionWith(etf.GetItemInfo(e).Parameters);
            allParameters.Add(new DoubleParamInfo() { Name = "EXTRACTOR_TOTAL_WEIGHT_KILOGRAM" });

            return allParameters.OrderBy(p => p.Name);
        }

        protected double HandleLengthByRule(double value, RebarType type)
        {
            switch (type)
            {
                case RebarType.perUnitLength:
                    {
#if DEBUG
                        log.Debug($"{type}: {value}");
#endif
                        return value;
                    }
                case RebarType.straightBars:
                    {
#if DEBUG
                        log.Debug($"{type}: round({value}, 5) = {Math.Round(value / 5) * 5}");
#endif
                        return Math.Round(value / 5) * 5;
                    }
                case RebarType.bendingDetails:
                    {
#if DEBUG
                        log.Debug($"{type}: round({value}) =  {Math.Round(value)}");
#endif
                        return Math.Round(value);
                    }
                default:
                    {
#if DEBUG
                        log.Warn($"{type}: {value} - unknown rebar type");
#endif
                        return value;
                    }
            }
        }

        protected double GetRightTotalLength(IEnumerable<IParameterInfo> allParameters, out RebarType type, out double coefficient)
        {
            type = RebarType.perUnitLength;
            var detaleType = RebarType.straightBars;
            int barCount = 0;
            double barLength = 0.0;
            coefficient = 1.0;

            foreach (var param in allParameters)
            {
                switch (param.Name)
                {
                    case "REBAR_ELEM_LENGTH":
                        {
                            barLength = (param as DoubleParamInfo).Value;
                            break;
                        }
                    case "REBAR_ELEM_QUANTITY_OF_BARS":
                        {
                            barCount = (param as IntParamInfo).Value;
                            break;
                        }
                    case "ADSK_Размер в погонных метрах":
                        {
                            if (param is IntParamInfo p)
                                type = p.Value == 0 ? RebarType.straightBars : RebarType.perUnitLength;
                            break;
                        }
                    case "ADSK_Форма арматуры":
                        {
                            if (param is IntParamInfo p)
                                detaleType = p.Value == 1 ? RebarType.straightBars : RebarType.bendingDetails;
                            break;
                        }
                    case "АРМ_Коэфф нахлеста растянутого стержня":
                        {
                            if (param is DoubleParamInfo p)
                                coefficient = p.Value == 0 ? 1 : p.Value;
                            break;
                        }
                    default:
                        break;
                }
            }


            if (type != RebarType.perUnitLength) type = detaleType;

            if (barCount > 0 && barLength > 0)
                return coefficient * HandleLengthByRule(barLength, type) * barCount;
            else
                return 0;
        }
    }
}
