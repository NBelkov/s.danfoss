﻿using Autodesk.Revit.DB;
using System;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class InternalNameValueHandler : INameValueHanler
    {
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IParameterInfo HandleParameter(Parameter p)
        {
            try
            {

                var valueType = p.StorageType;
                var def = p.Definition;
                switch (valueType)
                {
                    case StorageType.Double:
                        {
                            return new DoubleParamInfo()
                            {
                                Name = def.Name,
                                Value = (double)p.AsDouble(),
                            };
                        }
                    case StorageType.ElementId:
                        {
                            return new IdParamInfo()
                            {
                                Name = def.Name,
                                Value = p.AsElementId(),
                            };
                        }
                    case StorageType.Integer:
                        {
                            return new IntParamInfo()
                            {
                                Name = def.Name,
                                Value = p.AsInteger(),
                            };
                        }
                    case StorageType.String:
                        {
                            return new StringParamInfo()
                            {
                                Name = def.Name,
                                Value = p.AsString(),
                            };
                        }
                    default:
                        {
                            return new StringParamInfo()
                            {
                                Name = def.Name + " (AsValueString)",
                                Value = p.AsValueString(),
                            };
                        }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error:", ex);
                return new StringParamInfo() { Name = "Error", Value = "Error" };
            }

        }
    }
}
