﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public class ParameterInfoFactory : AbstractParameterFactory
    {
        public ParameterInfoFactory(INameValueHanler nameValueHanler) : base(nameValueHanler)
        {
        }
        public override IEnumerable<IParameterInfo> GetParameterInfos(Element element)
        {
            var result = new List<IParameterInfo>();
            var pars = element.Parameters.Cast<Parameter>();
            if (pars is null || !pars.Any()) return result;
            foreach (var p in pars)
            {
                result.Add(GetParameterInfo(p));
            }
            return result;
        }
    }

    public class SpecialParameterInfoFactory : AbstractParameterFactory
    {
        private IEnumerable<BuiltInParameter> bips;
        public SpecialParameterInfoFactory(INameValueHanler nameValueHanler, IEnumerable<BuiltInParameter> builtIns) : base(nameValueHanler)
        {
            bips = builtIns;
        }
        public override IEnumerable<IParameterInfo> GetParameterInfos(Element element)
        {
            var result = new List<IParameterInfo>();
            var pars = element.Parameters.Cast<Parameter>();
            if (pars is null || !pars.Any()) return result;
            foreach (var p in pars)
            {
                if (!Skip(p))
                    result.Add(GetParameterInfo(p));
            }
            result.AddRange(GetBip(element));
            return result;
        }

        private bool Skip(Parameter p) => bips.Any(bip => p.Id.IntegerValue == (int)bip);

        private IEnumerable<IParameterInfo> GetBip(Element elem)
        {
            var result = new List<IParameterInfo>();
            if (elem is ElementType) return result;

            foreach (var bip in bips)
            {
                var p = elem.get_Parameter(bip);
                if (p is null) throw new ArgumentException($"Не удалось считать параметр '{bip}': параметр не найден");
                if (p.AsValueString() is null) throw new ArgumentException($"Не удалось считать параметр '{bip}': параметр не имеет значения");

                result.Add(GetParameterInfo(p));
            }
            return result;
        }
    }
}
