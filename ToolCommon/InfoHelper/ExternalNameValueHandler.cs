﻿#define NOdebugParameters
using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.Xtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public abstract class AbstractNameValueHandler : INameValueHanler
    {
        protected INameBuilder nb;
        protected IValueConverter vc;
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AbstractNameValueHandler(INameBuilder nameBuilder, IValueConverter valueConverter)
        {
            nb = nameBuilder;
            vc = valueConverter;
        }

        public virtual IParameterInfo HandleParameter(Parameter p)
        {
            try
            {
                var valueType = p.StorageType;
                switch (valueType)
                {
                    case StorageType.Double:
                        {
                            return DoubleStorageHandler(p);
                        }
                    case StorageType.ElementId:
                        {
                            return IdStorageHandler(p);
                        }
                    case StorageType.Integer:
                        {
                            return IntStorageHandler(p);
                        }
                    case StorageType.String:
                        {
                            return StringStorageHandler(p);
                        }
                    case StorageType.None:
                        {
                            return NoneStorageHandler(p);
                        }
                    default:
                        {
                            return DefaultStorageHandler(p);
                        }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error:", ex);
                return ErrorStorageHandler(p);
            }
        }

        internal abstract IParameterInfo IntStorageHandler(Parameter p);
        internal abstract IParameterInfo DoubleStorageHandler(Parameter p);
        internal abstract IParameterInfo StringStorageHandler(Parameter p);
        internal abstract IParameterInfo IdStorageHandler(Parameter p);
        internal abstract IParameterInfo DefaultStorageHandler(Parameter p);
        internal abstract IParameterInfo NoneStorageHandler(Parameter p);
        internal abstract IParameterInfo ErrorStorageHandler(Parameter p);

    }

    public class ExternalNameAndIdValueHandler : AbstractNameValueHandler
    {
        public ExternalNameAndIdValueHandler(INameBuilder nameBuilder, IValueConverter valueConverter) : base(nameBuilder, valueConverter)
        {
        }

        internal override IParameterInfo DoubleStorageHandler(Parameter p)
        {
            return new DoubleParamInfo()
            {
                Name = nb.GetName(p),
                Value = vc.Convert(p),
            };
        }
        internal override IParameterInfo IntStorageHandler(Parameter p)
        {
            //КОСТЫЛЬ СМ_Смета
#if debugParameters
            log.Debug($"{p.Element.UniqueId}: p {p.Definition.Name}, HasValue {p.HasValue}, value {p.AsInteger()}");
#endif
            var name = nb.GetName(p);
            if (name == "СМ_Смета")
            {
                //if parameter has a value take it else take 1
                var value = p.HasValue ? p.AsInteger() : 1;
#if debugParameters
                log.Debug($"{p.Element.UniqueId}: result {value}, HasValue {p.HasValue}, value {p.AsInteger()}");
#endif
                return new IntParamInfo()
                {
                    Name = name,
                    Value = value,
                };
            }
            return new IntParamInfo()
            {
                Name = nb.GetName(p),
                Value = p.AsInteger(),
            };
        }
        internal override IParameterInfo StringStorageHandler(Parameter p)
        {
            var val = p.AsString();
            if (val is null) val = string.Empty;
            return new StringParamInfo()
            {
                Name = nb.GetName(p),
                Value = val,
            };
        }
        internal override IParameterInfo IdStorageHandler(Parameter p)
        {
            ElementId id = p.AsElementId();
           
            int idInt = id.IntegerValue;
            switch (idInt)
            {
                case -1:
                    {
                        return new StringParamInfo()
                        {
                            Name = nb.GetName(p),
                            Value = p.AsValueString(),
                        };
                    }
                case (int)BuiltInParameter.HOST_ID_PARAM:
                    {
                        var doc = p.Element.Document;
                        var val = doc.GetElement(id).UniqueId;
                        return new StringParamInfo()
                        {
                            Name = nb.GetName(p),
                            Value = val,
                        };
                    }
                case (int)BuiltInParameter.ELEM_CATEGORY_PARAM:
                case (int)BuiltInParameter.ELEM_CATEGORY_PARAM_MT:
                    {
                        return new StringParamInfo()
                        {
                            Name = nb.GetName(p),
                            Value = ((BuiltInCategory)idInt).ToStringFast(),
                        };
                    }
                default:
                    {
                        if (Enum.IsDefined(typeof(BuiltInCategory), idInt))
                        {
                            return new StringParamInfo()
                            {
                                Name = nb.GetName(p),
                                Value = ((BuiltInCategory)idInt).ToStringFast(),
                            };
                        }

                        var doc = p.Element.Document;
                        var elem = doc.GetElement(id);
                        return new StringParamInfo()
                        {
                            Name = nb.GetName(p),
                            Value = GetElementAsString(elem)
                        };

                    }
            }
        }
        internal override IParameterInfo DefaultStorageHandler(Parameter p)
        {
            return new StringParamInfo()
            {
                Name = nb.GetName(p),
                Value = "N/A",
            };
        }
        internal override IParameterInfo ErrorStorageHandler(Parameter p)
        {
            string nam = "Error";
            string val = "Error";
            try
            {
                nam = p.Definition.Name;
            }
            catch (Exception ex)
            {
                log.Error("Error:", ex);
            }
            try
            {
                val = p.AsValueString();
            }
            catch (Exception ex)
            {
                log.Error("Error:", ex);
            }
            return new StringParamInfo() { Name = nam, Value = val };
        }

        internal override IParameterInfo NoneStorageHandler(Parameter p)
        {
            return new StringParamInfo()
            {
                Name = nb.GetName(p),
                Value = "None",
            };
        }
        private string GetElementAsString(Element elem)
        {
            switch (elem)
            {
                case null:
                    {
                        return string.Empty;
                    }
                case ElementType e:
                    {
                        return e.FamilyName;
                    }
                default: return elem.Name;
            }
        }
    }
}