﻿using Autodesk.Revit.DB;
using System;

namespace S.Danfoss.CommonTools.Xtensions
{
    public static class GeometryExtension
    {
        public static string PrintX(this XYZ pnt, int precision = 1) => (Math.Round(pnt.X, precision) * 304.8).ToString();
        public static string PrintY(this XYZ pnt, int precision = 1) => (Math.Round(pnt.Y, precision) * 304.8).ToString();
        public static string PrintZ(this XYZ pnt, int precision = 1) => (Math.Round(pnt.Z, precision) * 304.8).ToString();

        public static string Print(this XYZ pnt, int precision = 1) =>
            $"[" +
            $"{Math.Round(pnt.X, precision) * 304.8}, " +
            $"{Math.Round(pnt.Y, precision) * 304.8}, " +
            $"{Math.Round(pnt.Z, precision) * 304.8}" +
            $"]";

        public static string PrintFeet(this XYZ pnt, int precision = 1) =>
            $"[" +
            $"{Math.Round(pnt.X, precision)}, " +
            $"{Math.Round(pnt.Y, precision)}, " +
            $"{Math.Round(pnt.Z, precision)}" +
            $"]";

        public static double Projection(this XYZ vector, XYZ other) =>
            vector.DotProduct(other) / other.GetLength();
    }
}
