﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.Xtensions
{
    public static class Enumxtentions
    {
        private static Dictionary<int, string> bips = new Dictionary<int, string>();
        private static Dictionary<int, string> bics = new Dictionary<int, string>();

        public static string ToStringFast(this BuiltInParameter parameter)
        {
            int i = (int)parameter;
            if (bips.ContainsKey(i)) return bips[i];

            var p = parameter.ToString();
            bips[i] = p;
            return p;
        }

        public static string ToStringFast(this BuiltInCategory cat)
        {
            int i = (int)cat;
            if (bics.ContainsKey(i)) return bics[i];

            var c = cat.ToString();
            bics[i] = c;
            return c;
        }


        public static Dictionary<ParameterType, StorageType> ParameterToStorageMap = new Dictionary<ParameterType, StorageType>()
            {

                //{ParameterType.YesNo, StorageType.Integer},
                //{ParameterType.Integer, StorageType.Integer},

                //{ParameterType.Number, StorageType.Double},
                //{ParameterType.Length, StorageType.Double},
                //{ParameterType.Currency, StorageType.Double},
                //{ParameterType.Angle, StorageType.Double},
                //{ParameterType.ElectricalPotential, StorageType.Double},
                //{ParameterType.Stress, StorageType.Double},
                //{ParameterType.UnitWeight, StorageType.Double},
                //{ParameterType.Area, StorageType.Double},
                //{ParameterType.Volume, StorageType.Double},
                //{ParameterType.Slope, StorageType.Double},
                //{ParameterType.PipingRoughness, StorageType.Double},
                //{ParameterType.HVACAreaDividedByHeatingLoad, StorageType.Double},
                //{ParameterType.HVACCoolingLoadDividedByArea, StorageType.Double},
                //{ParameterType.HVACCoolingLoad, StorageType.Double},
                //{ParameterType.HVACHeatingLoadDividedByArea, StorageType.Double},
                //{ParameterType.HVACAirflow, StorageType.Double},
                //{ParameterType.HVACAirflowDensity, StorageType.Double},
                //{ParameterType.HVACHeatingLoad, StorageType.Double},
                //{ParameterType.HVACAreaDividedByCoolingLoad, StorageType.Double},
                //{ParameterType.HVACFactor, StorageType.Double},
                //{ParameterType.ElectricalPowerDensity, StorageType.Double},
                //{ParameterType.HVACHeatGain, StorageType.Double},
                //{ParameterType.HVACTemperature, StorageType.Double},

                //{ParameterType.Invalid, StorageType.ElementId},
                //{ParameterType.Text, StorageType.ElementId},
                //{ParameterType.Material, StorageType.ElementId},
                //{ParameterType.FamilyType, StorageType.ElementId},

                //{ParameterType.Invalid, StorageType.None},

                //{ParameterType.Text, StorageType.String},
                //{ParameterType.URL, StorageType.String},
                //{ParameterType.Invalid, StorageType.String},

{ParameterType.LoadClassification, StorageType.None},

{ParameterType.Material, StorageType.ElementId},
{ParameterType.FamilyType, StorageType.ElementId},
{ParameterType.Image, StorageType.ElementId},
{ParameterType.Text, StorageType.ElementId},

{ParameterType.Integer, StorageType.Integer},
{ParameterType.YesNo, StorageType.Integer},
{ParameterType.Invalid, StorageType.Integer},

{ParameterType.URL, StorageType.String},
//{ParameterType.Text, StorageType.String},
{ParameterType.MultilineText, StorageType.String},

{ParameterType.Number, StorageType.Double},
{ParameterType.Length, StorageType.Double},
{ParameterType.Area, StorageType.Double},
{ParameterType.Volume, StorageType.Double},
{ParameterType.Angle, StorageType.Double},
{ParameterType.Force, StorageType.Double},
{ParameterType.LinearForce, StorageType.Double},
{ParameterType.AreaForce, StorageType.Double},
{ParameterType.Moment, StorageType.Double},
{ParameterType.NumberOfPoles, StorageType.Double},
{ParameterType.FixtureUnit, StorageType.Double},
{ParameterType.HVACDensity, StorageType.Double},
{ParameterType.HVACEnergy, StorageType.Double},
{ParameterType.HVACFriction, StorageType.Double},
{ParameterType.HVACPower, StorageType.Double},
{ParameterType.HVACPowerDensity, StorageType.Double},
{ParameterType.HVACPressure, StorageType.Double},
{ParameterType.HVACTemperature, StorageType.Double},
{ParameterType.HVACVelocity, StorageType.Double},
{ParameterType.HVACAirflow, StorageType.Double},
{ParameterType.HVACDuctSize, StorageType.Double},
{ParameterType.HVACCrossSection, StorageType.Double},
{ParameterType.HVACHeatGain, StorageType.Double},
{ParameterType.ElectricalCurrent, StorageType.Double},
{ParameterType.ElectricalPotential, StorageType.Double},
{ParameterType.ElectricalFrequency, StorageType.Double},
{ParameterType.ElectricalIlluminance, StorageType.Double},
{ParameterType.ElectricalLuminousFlux, StorageType.Double},
{ParameterType.ElectricalPower, StorageType.Double},
{ParameterType.HVACRoughness, StorageType.Double},
{ParameterType.ElectricalApparentPower, StorageType.Double},
{ParameterType.ElectricalPowerDensity, StorageType.Double},
{ParameterType.PipingDensity, StorageType.Double},
{ParameterType.PipingFlow, StorageType.Double},
{ParameterType.PipingFriction, StorageType.Double},
{ParameterType.PipingPressure, StorageType.Double},
{ParameterType.PipingTemperature, StorageType.Double},
{ParameterType.PipingVelocity, StorageType.Double},
{ParameterType.PipingViscosity, StorageType.Double},
{ParameterType.PipeSize, StorageType.Double},
{ParameterType.PipingRoughness, StorageType.Double},
{ParameterType.Stress, StorageType.Double},
{ParameterType.UnitWeight, StorageType.Double},
{ParameterType.ThermalExpansion, StorageType.Double},
{ParameterType.LinearMoment, StorageType.Double},
{ParameterType.ForcePerLength, StorageType.Double},
{ParameterType.ForceLengthPerAngle, StorageType.Double},
{ParameterType.LinearForcePerLength, StorageType.Double},
{ParameterType.LinearForceLengthPerAngle, StorageType.Double},
{ParameterType.AreaForcePerLength, StorageType.Double},
{ParameterType.PipingVolume, StorageType.Double},
{ParameterType.HVACViscosity, StorageType.Double},
{ParameterType.HVACCoefficientOfHeatTransfer, StorageType.Double},
{ParameterType.HVACAirflowDensity, StorageType.Double},
{ParameterType.Slope, StorageType.Double},
{ParameterType.HVACCoolingLoad, StorageType.Double},
{ParameterType.HVACCoolingLoadDividedByArea, StorageType.Double},
{ParameterType.HVACCoolingLoadDividedByVolume, StorageType.Double},
{ParameterType.HVACHeatingLoad, StorageType.Double},
{ParameterType.HVACHeatingLoadDividedByArea, StorageType.Double},
{ParameterType.HVACHeatingLoadDividedByVolume, StorageType.Double},
{ParameterType.HVACAirflowDividedByVolume, StorageType.Double},
{ParameterType.HVACAirflowDividedByCoolingLoad, StorageType.Double},
{ParameterType.HVACAreaDividedByCoolingLoad, StorageType.Double},
{ParameterType.WireSize, StorageType.Double},
{ParameterType.HVACSlope, StorageType.Double},
{ParameterType.PipingSlope, StorageType.Double},
{ParameterType.Currency, StorageType.Double},
{ParameterType.ElectricalEfficacy, StorageType.Double},
{ParameterType.ElectricalWattage, StorageType.Double},
{ParameterType.ColorTemperature, StorageType.Double},
{ParameterType.ElectricalLuminousIntensity, StorageType.Double},
{ParameterType.ElectricalLuminance, StorageType.Double},
{ParameterType.HVACAreaDividedByHeatingLoad, StorageType.Double},
{ParameterType.HVACFactor, StorageType.Double},
{ParameterType.ElectricalTemperature, StorageType.Double},
{ParameterType.ElectricalCableTraySize, StorageType.Double},
{ParameterType.ElectricalConduitSize, StorageType.Double},
{ParameterType.ReinforcementVolume, StorageType.Double},
{ParameterType.ReinforcementLength, StorageType.Double},
{ParameterType.ElectricalDemandFactor, StorageType.Double},
{ParameterType.HVACDuctInsulationThickness, StorageType.Double},
{ParameterType.HVACDuctLiningThickness, StorageType.Double},
{ParameterType.PipeInsulationThickness, StorageType.Double},
{ParameterType.HVACThermalResistance, StorageType.Double},
{ParameterType.HVACThermalMass, StorageType.Double},
{ParameterType.Acceleration, StorageType.Double},
{ParameterType.BarDiameter, StorageType.Double},
{ParameterType.CrackWidth, StorageType.Double},
{ParameterType.DisplacementDeflection, StorageType.Double},
{ParameterType.Energy, StorageType.Double},
{ParameterType.StructuralFrequency, StorageType.Double},
{ParameterType.Mass, StorageType.Double},
{ParameterType.MassPerUnitLength, StorageType.Double},
{ParameterType.MomentOfInertia, StorageType.Double},
{ParameterType.SurfaceArea, StorageType.Double},
{ParameterType.Period, StorageType.Double},
{ParameterType.Pulsation, StorageType.Double},
{ParameterType.ReinforcementArea, StorageType.Double},
{ParameterType.ReinforcementAreaPerUnitLength, StorageType.Double},
{ParameterType.ReinforcementCover, StorageType.Double},
{ParameterType.ReinforcementSpacing, StorageType.Double},
{ParameterType.Rotation, StorageType.Double},
{ParameterType.SectionArea, StorageType.Double},
{ParameterType.SectionDimension, StorageType.Double},
{ParameterType.SectionModulus, StorageType.Double},
{ParameterType.SectionProperty, StorageType.Double},
{ParameterType.StructuralVelocity, StorageType.Double},
{ParameterType.WarpingConstant, StorageType.Double},
{ParameterType.Weight, StorageType.Double},
{ParameterType.WeightPerUnitLength, StorageType.Double},
{ParameterType.HVACThermalConductivity, StorageType.Double},
{ParameterType.HVACSpecificHeat, StorageType.Double},
{ParameterType.HVACSpecificHeatOfVaporization, StorageType.Double},
{ParameterType.HVACPermeability, StorageType.Double},
{ParameterType.ElectricalResistivity, StorageType.Double},
{ParameterType.MassDensity, StorageType.Double},
{ParameterType.MassPerUnitArea, StorageType.Double},
{ParameterType.PipeDimension, StorageType.Double},
{ParameterType.PipeMass, StorageType.Double},
{ParameterType.PipeMassPerUnitLength, StorageType.Double},
{ParameterType.HVACTemperatureDifference, StorageType.Double},
{ParameterType.PipingTemperatureDifference, StorageType.Double},
{ParameterType.ElectricalTemperatureDifference, StorageType.Double},
          
};
        /*
    {ParameterType.Invalid			, StorageType.None},	
    
    {ParameterType.Text		, StorageType.ElementId},																	
    {ParameterType.Integer		, StorageType.Integer},																	
    {ParameterType.Number		, StorageType.Integer},	
    
    {ParameterType.Length	, StorageType.Double},																		
    {ParameterType.Area		, StorageType.Double},																	
    {ParameterType.Volume	, StorageType.Double},																		
    {ParameterType.Angle	, StorageType.Double},																		
    {ParameterType.URL	, StorageType.String},																			
    {ParameterType.Material		, StorageType.ElementId},																
    {ParameterType.YesNo			, StorageType.Integer},																	
    {ParameterType.Force	, StorageType.Double},																		
    {ParameterType.LinearForce	, StorageType.Double},																	
    {ParameterType.AreaForce	, StorageType.Double},																	
    {ParameterType.Moment		, StorageType.Double},																	
    {ParameterType.NumberOfPoles	, StorageType.Double},																
    {ParameterType.FixtureUnit	, StorageType.Double},																	
    {ParameterType.FamilyType	, StorageType.Double},																	
    {ParameterType.LoadClassification		, StorageType.None},														
    {ParameterType.Image					, StorageType.None},														
    {ParameterType.MultilineText	, StorageType.String},																
    {ParameterType.HVACDensity						, StorageType.Double},											
    {ParameterType.HVACEnergy						, StorageType.Double},											
    {ParameterType.HVACFriction						, StorageType.Double},										
    {ParameterType.HVACPower						, StorageType.Double},											
    {ParameterType.HVACPowerDensity					, StorageType.Double},										
    {ParameterType.HVACPressure						, StorageType.Double},										
    {ParameterType.HVACTemperature					, StorageType.Double},											
    {ParameterType.HVACVelocity						, StorageType.Double},										
    {ParameterType.HVACAirflow						, StorageType.Double},											
    {ParameterType.HVACDuctSize						, StorageType.Double},										
    {ParameterType.HVACCrossSection					, StorageType.Double},										
    {ParameterType.HVACHeatGain						, StorageType.Double},										
    {ParameterType.ElectricalCurrent				, StorageType.Double},											
    {ParameterType.ElectricalPotential				, StorageType.Double},											
    {ParameterType.ElectricalFrequency				, StorageType.Double},											
    {ParameterType.ElectricalIlluminance			, StorageType.Double},											
    {ParameterType.ElectricalLuminousFlux			, StorageType.Double},											
    {ParameterType.ElectricalPower					, StorageType.Double},											
    {ParameterType.HVACRoughness					, StorageType.Double},											
    {ParameterType.ElectricalApparentPower			, StorageType.Double},											
    {ParameterType.ElectricalPowerDensity			, StorageType.Double},											
    {ParameterType.PipingDensity					, StorageType.Double},											
    {ParameterType.PipingFlow						, StorageType.Double},											
    {ParameterType.PipingFriction					, StorageType.Double},											
    {ParameterType.PipingPressure					, StorageType.Double},											
    {ParameterType.PipingTemperature				, StorageType.Double},											
    {ParameterType.PipingVelocity					, StorageType.Double},											
    {ParameterType.PipingViscosity					, StorageType.Double},											
    {ParameterType.PipeSize							, StorageType.Double},										
    {ParameterType.PipingRoughness					, StorageType.Double},											
    {ParameterType.Stress							, StorageType.Double},											
    {ParameterType.UnitWeight						, StorageType.Double},											
    {ParameterType.ThermalExpansion					, StorageType.Double},										
    {ParameterType.LinearMoment						, StorageType.Double},										
    {ParameterType.ForcePerLength					, StorageType.Double},											
    {ParameterType.ForceLengthPerAngle				, StorageType.Double},											
    {ParameterType.LinearForcePerLength				, StorageType.Double},										
    {ParameterType.LinearForceLengthPerAngle		, StorageType.Double},											
    {ParameterType.AreaForcePerLength				, StorageType.Double},											
    {ParameterType.PipingVolume						, StorageType.Double},										
    {ParameterType.HVACViscosity					, StorageType.Double},											
    {ParameterType.HVACCoefficientOfHeatTransfer	, StorageType.Double},											
    {ParameterType.HVACAirflowDensity				, StorageType.Double},											
    {ParameterType.Slope							, StorageType.Double},											
    {ParameterType.HVACCoolingLoad					, StorageType.Double},											
    {ParameterType.HVACCoolingLoadDividedByArea		, StorageType.Double},										
    {ParameterType.HVACCoolingLoadDividedByVolume	, StorageType.Double},											
    {ParameterType.HVACHeatingLoad					, StorageType.Double},											
    {ParameterType.HVACHeatingLoadDividedByArea		, StorageType.Double},										
    {ParameterType.HVACHeatingLoadDividedByVolume	, StorageType.Double},											
    {ParameterType.HVACAirflowDividedByVolume		, StorageType.Double},											
    {ParameterType.HVACAirflowDividedByCoolingLoad	, StorageType.Double},											
    {ParameterType.HVACAreaDividedByCoolingLoad		, StorageType.Double},										
    {ParameterType.WireSize							, StorageType.Double},										
    {ParameterType.HVACSlope						, StorageType.Double},											
    {ParameterType.PipingSlope						, StorageType.Double},											
    {ParameterType.Currency							, StorageType.Double},										
    {ParameterType.ElectricalEfficacy				, StorageType.Double},											
    {ParameterType.ElectricalWattage				, StorageType.Double},											
    {ParameterType.ColorTemperature					, StorageType.Double},										
    {ParameterType.ElectricalLuminousIntensity		, StorageType.Double},											
    {ParameterType.ElectricalLuminance				, StorageType.Double},											
    {ParameterType.HVACAreaDividedByHeatingLoad		, StorageType.Double},										
    {ParameterType.HVACFactor						, StorageType.Double},											
    {ParameterType.ElectricalTemperature			, StorageType.Double},											
    {ParameterType.ElectricalCableTraySize			, StorageType.Double},											
    {ParameterType.ElectricalConduitSize			, StorageType.Double},											
    {ParameterType.ReinforcementVolume				, StorageType.Double},											
    {ParameterType.ReinforcementLength				, StorageType.Double},											
    {ParameterType.ElectricalDemandFactor			, StorageType.Double},											
    {ParameterType.HVACDuctInsulationThickness		, StorageType.Double},											
    {ParameterType.HVACDuctLiningThickness			, StorageType.Double},											
    {ParameterType.PipeInsulationThickness			, StorageType.Double},											
    {ParameterType.HVACThermalResistance			, StorageType.Double},											
    {ParameterType.HVACThermalMass					, StorageType.Double},											
    {ParameterType.Acceleration						, StorageType.Double},										
    {ParameterType.BarDiameter						, StorageType.Double},											
    {ParameterType.CrackWidth						, StorageType.Double},											
    {ParameterType.DisplacementDeflection			, StorageType.Double},											
    {ParameterType.Energy							, StorageType.Double},											
    {ParameterType.StructuralFrequency				, StorageType.Double},											
    {ParameterType.Mass								, StorageType.Double},										
    {ParameterType.MassPerUnitLength				, StorageType.Double},											
    {ParameterType.MomentOfInertia					, StorageType.Double},											
    {ParameterType.SurfaceArea						, StorageType.Double},											
    {ParameterType.Period							, StorageType.Double},											
    {ParameterType.Pulsation						, StorageType.Double},											
    {ParameterType.ReinforcementArea				, StorageType.Double},											
    {ParameterType.ReinforcementAreaPerUnitLength	, StorageType.Double},											
    {ParameterType.ReinforcementCover				, StorageType.Double},											
    {ParameterType.ReinforcementSpacing				, StorageType.Double},										
    {ParameterType.Rotation							, StorageType.Double},										
    {ParameterType.SectionArea						, StorageType.Double},											
    {ParameterType.SectionDimension					, StorageType.Double},										
    {ParameterType.SectionModulus					, StorageType.Double},											
    {ParameterType.SectionProperty					, StorageType.Double},											
    {ParameterType.StructuralVelocity				, StorageType.Double},											
    {ParameterType.WarpingConstant					, StorageType.Double},											
    {ParameterType.Weight							, StorageType.Double},											
    {ParameterType.WeightPerUnitLength				, StorageType.Double},											
    {ParameterType.HVACThermalConductivity			, StorageType.Double},											
    {ParameterType.HVACSpecificHeat					, StorageType.Double},										
    {ParameterType.HVACSpecificHeatOfVaporization	, StorageType.Double},											
    {ParameterType.HVACPermeability					, StorageType.Double},										
    {ParameterType.ElectricalResistivity			, StorageType.Double},											
    {ParameterType.MassDensity						, StorageType.Double},											
    {ParameterType.MassPerUnitArea					, StorageType.Double},											
    {ParameterType.PipeDimension					, StorageType.Double},											
    {ParameterType.PipeMass							, StorageType.Double},										
    {ParameterType.PipeMassPerUnitLength			, StorageType.Double},											
    {ParameterType.HVACTemperatureDifference		, StorageType.Double},											
    {ParameterType.PipingTemperatureDifference		, StorageType.Double},											
    {ParameterType.ElectricalTemperatureDifference	, StorageType.Double},											
          */



        public static StorageType GetStorageType(this ParameterType parameterType) => ParameterToStorageMap[parameterType];

    }
}
