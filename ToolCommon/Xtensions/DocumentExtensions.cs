﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using S.Danfoss.CommonTools.RevitObjectsProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.Xtensions
{
    public static class LinkExtentions
    {
        static readonly string type_param_name = "СМ_Смета";
        static readonly string instance_param_name = "СМ_Смета (экземпляр)";

        static bool IsAllowed(this RevitLinkType type)
        {
            if (type.AttachmentType is AttachmentType.Overlay)
            {
                return type.AllowElement(paramName: type_param_name, value: 1, allowIfNoValue: false);
            }
            else
            {
                return type.AllowElement(paramName: type_param_name, value: 1, allowIfNoValue: true);
            }
        }

        public static FilteredElementCollector WhereLinkTypesAreExtracting(this FilteredElementCollector fec, bool isExtracting)
        {
            var lnkTypeFec = fec.WhereElementIsElementType()
                .OfClass(typeof(RevitLinkType))
                .Cast<RevitLinkType>()
                .Where(t => (!t.IsNestedLink && t.IsAllowed()) == isExtracting);

            if (lnkTypeFec.Any())
            {
                var doc = lnkTypeFec.First().Document;

                if (doc.IsLinked)
                {
                    lnkTypeFec = lnkTypeFec
                        .Where(t => t.AttachmentType == AttachmentType.Attachment);

                }

                if (lnkTypeFec != null && lnkTypeFec.Any())
                {
                    return new FilteredElementCollector(doc, lnkTypeFec.Select(t => t.Id).ToList())
                        .WhereElementIsElementType();
                }
            }

            return fec.SetCollectorToEmpty();
        }

        public static FilteredElementCollector WhereLinkInsatncesAreExtracting(this FilteredElementCollector fec, bool isExtracting)
        {
            var lnkTypeFec = fec
                .WhereLinkTypesAreExtracting(isExtracting);

            var filters = lnkTypeFec.Select(t => new ElementParameterFilter(ParameterFilterRuleFactory
                .CreateEqualsRule(new ElementId(BuiltInParameter.ELEM_TYPE_PARAM), t.Id)) as ElementFilter)
                .ToList();

            if (lnkTypeFec.Any())
            {
                var filter = filters.Count > 1 ? new LogicalOrFilter(filters) : filters.First();
                var doc = lnkTypeFec.First().Document;

                var insts = new FilteredElementCollector(doc)
                    .WhereElementIsNotElementType()
                    .WherePasses(filter)
                    .Where(inst =>
                    {
                        return
                            !inst.GetParameters(instance_param_name).Any() == isExtracting || // если параметра нет, то выгружаем
                            inst.AllowElement(paramName: instance_param_name, value: 1, allowIfNoValue: true) == isExtracting;
                    })
                    .Select(inst => inst.Id)
                    .ToList();

                if (insts.Any())
                {
                    return new FilteredElementCollector(doc, insts)
                        .WhereElementIsNotElementType();
                }
            }
            return fec.SetCollectorToEmpty();
        }

    }

    public static class DocumentExtensions
    {

        public static ICollection<Document> GetLinkDocs(this Document document)
        {
            return new FilteredElementCollector(document)
                .OfCategory(BuiltInCategory.OST_RvtLinks)
                .WhereElementIsNotElementType()
                .OfType<RevitLinkInstance>()
                .Select(link => link.GetLinkDocument())
                .ToList();
        }

        public static ICollection<Document> GetDocAndLinkDocs(this Document document)
        {
            ICollection<Document> result = document.GetLinkDocs();
            result.Add(document);
            return result;
        }

    }

    public static class FilteredElementCollectorExtensions
    {

        static readonly Type[] classesToExclude = new Type[]
        {
            typeof(DirectShape)
        };

        static readonly BuiltInCategory[] bicsToAdd = new BuiltInCategory[]
        {
                //BuiltInCategory.OST_RvtLinks,
                BuiltInCategory.OST_Coupler,
                BuiltInCategory.OST_StructConnections,
                BuiltInCategory.OST_FabricReinforcementWire,
                BuiltInCategory.OST_FabricReinforcement,
                BuiltInCategory.OST_RebarShape,
                BuiltInCategory.OST_Rebar,
                BuiltInCategory.OST_FabricationContainment,
                BuiltInCategory.OST_FabricationPipework,
                BuiltInCategory.OST_FabricationHangers,
                BuiltInCategory.OST_FabricationDuctwork,
                BuiltInCategory.OST_PlaceHolderPipes,
                BuiltInCategory.OST_PlaceHolderDucts,
                BuiltInCategory.OST_CableTrayRun,
                BuiltInCategory.OST_ConduitRun,
                BuiltInCategory.OST_Conduit,
                BuiltInCategory.OST_CableTray,
                BuiltInCategory.OST_ConduitFitting,
                BuiltInCategory.OST_CableTrayFitting,
                BuiltInCategory.OST_DuctLinings,
                BuiltInCategory.OST_DuctInsulations,
                BuiltInCategory.OST_PipeInsulations,
                BuiltInCategory.OST_Sprinklers,
                BuiltInCategory.OST_LightingDevices,
                BuiltInCategory.OST_FireAlarmDevices,
                BuiltInCategory.OST_DataDevices,
                BuiltInCategory.OST_CommunicationDevices,
                BuiltInCategory.OST_SecurityDevices,
                BuiltInCategory.OST_NurseCallDevices,
                BuiltInCategory.OST_TelephoneDevices,
                BuiltInCategory.OST_PipeAccessory,
                BuiltInCategory.OST_FlexPipeCurves,
                BuiltInCategory.OST_PipeFitting,
                BuiltInCategory.OST_PipeCurves,
                BuiltInCategory.OST_PipingSystem,
                BuiltInCategory.OST_Wire,
                BuiltInCategory.OST_FlexDuctCurves,
                BuiltInCategory.OST_DuctAccessory,
                BuiltInCategory.OST_DuctSystem,
                BuiltInCategory.OST_DuctTerminal,
                BuiltInCategory.OST_DuctFitting,
                BuiltInCategory.OST_DuctCurves,
                BuiltInCategory.OST_RoofSoffit,
                BuiltInCategory.OST_EdgeSlab,
                BuiltInCategory.OST_Gutter,
                BuiltInCategory.OST_Fascia,
                BuiltInCategory.OST_Entourage,
                BuiltInCategory.OST_Planting,
                BuiltInCategory.OST_StructuralStiffener,
                BuiltInCategory.OST_SpecialityEquipment,
                BuiltInCategory.OST_Topography,
                BuiltInCategory.OST_TopographyLink,
                BuiltInCategory.OST_StructuralTruss,
                BuiltInCategory.OST_StructuralColumns,
                BuiltInCategory.OST_StructuralFramingSystem,
                BuiltInCategory.OST_StructuralFraming,
                BuiltInCategory.OST_StructuralFoundation,
                BuiltInCategory.OST_BuildingPad,
                BuiltInCategory.OST_Site,
                BuiltInCategory.OST_Parking,
                BuiltInCategory.OST_PlumbingFixtures,
                BuiltInCategory.OST_MechanicalEquipment,
                BuiltInCategory.OST_LightingFixtures,
                BuiltInCategory.OST_FurnitureSystems,
                BuiltInCategory.OST_ElectricalFixtures,
                BuiltInCategory.OST_ElectricalEquipment,
                BuiltInCategory.OST_Casework,
                BuiltInCategory.OST_MechanicalEquipmentSet,
                BuiltInCategory.OST_AnalyticalPipeConnections,
                BuiltInCategory.OST_RailingTermination,
                BuiltInCategory.OST_RailingSupport,
                BuiltInCategory.OST_RailingHandRail,
                BuiltInCategory.OST_RailingTopRail,
                BuiltInCategory.OST_StairsLandings,
                BuiltInCategory.OST_StairsRuns,
                BuiltInCategory.OST_CurtaSystem,
                BuiltInCategory.OST_Assemblies,
                BuiltInCategory.OST_Cornices,
                BuiltInCategory.OST_Ramps,
                BuiltInCategory.OST_CurtainWallMullions,
                BuiltInCategory.OST_CurtainWallPanels,
                BuiltInCategory.OST_GenericModel,
                BuiltInCategory.OST_StairsRailing,
                BuiltInCategory.OST_StairsStringerCarriage,
                BuiltInCategory.OST_Stairs,
                BuiltInCategory.OST_Columns,
                BuiltInCategory.OST_Furniture,
                BuiltInCategory.OST_Ceilings,
                BuiltInCategory.OST_Roofs,
                BuiltInCategory.OST_Floors,
                BuiltInCategory.OST_Doors,
                BuiltInCategory.OST_Windows,
                BuiltInCategory.OST_Walls,
        };

        /// <summary>
        /// Only those elements to which the parameter "Sm_Smeta" is assigned and the parameter has no a value or value = 1 pass through this filter.
        /// </summary>
        public static FilteredElementCollector WherePassesSmSmeta(this FilteredElementCollector fec)
        {
            var paramName = "СМ_Смета";

            var doc = fec
                .WherePasses(new ElementParameterFilter(new SharedParameterApplicableRule(paramName)))
                .FirstOrDefault()?.Document;

            if (doc is null)
            {
#if logging
                Log.Error("Cannot get document from collector. The input collector sets to empty");
#endif
                return fec.SetCollectorToEmpty();
            }

            // group by key, where key is Id of the type
            var groups = fec.GroupBy(e => e is ElementType ? e.Id : e?.GetTypeId());

            // get from fec all elements where type has SM_Smeta seted to Yes
            List<ElementId> allowed = new List<ElementId>();
            foreach (var group in groups)
            {
                if (group.Key is null)
                    allowed.AddRange(group.Where(t => t is ElementType && t.CheckForSmSmeta()).Select(e => e.Id));

                else if (doc.GetElement(group.Key) is ElementType t && t.CheckForSmSmeta())
                    allowed.AddRange(group.Select(e => e.Id));
            }

            if (allowed.Any()) fec.Excluding(allowed);

            if (fec != null && fec.Any())
            {
                foreach (var inst in fec)
                {
                    if (inst.CheckForSmSmeta()) allowed.Add(inst.Id);
                }
            }

            if (allowed.Any())
                return new FilteredElementCollector(doc, allowed).AllElements();
            else
                return new FilteredElementCollector(doc).SetCollectorToEmpty();
        }

        /// <summary>
        /// Makes ihe input collector empty
        /// </summary>
        /// <param name="fec">input collector</param>
        /// <returns></returns>
        public static FilteredElementCollector SetCollectorToEmpty(this FilteredElementCollector fec)
        {
            var filter = new LogicalAndFilter(new ElementIsElementTypeFilter(), new ElementIsElementTypeFilter(true));
            return fec.WherePasses(filter);
        }

        /// <summary>
        /// Leaves the input collector unaltered
        /// </summary>
        /// <param name="fec">input collector</param>
        /// <returns></returns>
        public static FilteredElementCollector AllElements(this FilteredElementCollector fec)
        {
            var filter = new LogicalOrFilter(new ElementIsElementTypeFilter(), new ElementIsElementTypeFilter(true));
            return fec.WherePasses(filter);
        }

        /// <summary>
        /// Only those elements to which the parameter "Sm_Smeta" is assigned and the parameter has no a value or value = 1 pass through this filter.
        /// </summary>
        public static IEnumerable<Element> WherePassesSmSmeta(this IEnumerable<Element> elements)
        {
            var output = new List<Element>();
            var byDocGroup = elements.GroupBy(e => e.Document);

            foreach (var group in byDocGroup)
            {
                var fec = new FilteredElementCollector(group.Key, group.Select(e => e.Id).ToList())
                    .WherePassesSmSmeta();
                output.AddRange(fec);
            }
            return output;
        }


        public static bool AllowElement(this Element e, string paramName, int value, bool allowIfNoValue = false)
        {
            var smParams = e.GetParameters(paramName);
            if (smParams.Any())
            {
                return smParams.Any(smp =>
                {
                    var result = smp.AsInteger();
                    if (allowIfNoValue)
                    {
                        return !smp.HasValue || result == value;
                    }
                    else
                    {
                        return smp.HasValue && result == value;
                    }
                });
            };

            return false;
        }

        public static bool CheckForSmSmeta(this Element e) => e.AllowElement(paramName: "СМ_Смета", value: 1, allowIfNoValue: true);


        public static FilteredElementCollector WhereElementsAreModel(this FilteredElementCollector fec)
        {
            var mClassFilter = new ElementMulticlassFilter(classesToExclude, true);
            var mCatFilter = new ElementMulticategoryFilter(bicsToAdd);
            return fec.WherePasses(mCatFilter).WherePasses(mClassFilter);
        }

        [Obsolete("WhereElementsHasParameter is deprecated, please use SharedParameterApplicableRule instead.")]
        public static FilteredElementCollector WhereElementsHasParameter(this FilteredElementCollector fec, Document doc, string name) =>
            WhereElementsHasParameter(fec, doc, new List<string> { name });

        [Obsolete("WhereElementsHasParameter is deprecated, please use SharedParameterApplicableRule instead.")]
        public static FilteredElementCollector WhereElementsHasParameter(this FilteredElementCollector fec, Document doc, IEnumerable<string> names)
        {
            var mcat = new UnionBicsProvider(doc).GetBics(new StringBicRequest(names)).ToList();
            return fec.WherePasses(new ElementMulticategoryFilter(mcat));
        }

        public static string LogElements(this IEnumerable<Element> elements)
        {
            string msg = $"{elements.Count()} elements\n";
            var docgroup = elements.GroupBy(e => e?.Document?.Title);
            foreach (var docs in docgroup)
            {
                msg += $"{docs.Key} - {docs.Count()}\n";
                var catgroup = docs.GroupBy(e => e?.Category?.Name);
                foreach (var cats in catgroup)
                {
                    msg += $"\t{cats.Key} - {cats.Count()}\n";
                    var typeGroup = cats.GroupBy(e => e?.Name);
                    foreach (var types in typeGroup)
                    {
                        msg += $"\t\t{types.Key} - {types.Count()}\n";
                    }
                }
            }

            return msg;
        }
    }
}
