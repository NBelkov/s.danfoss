﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.Xtensions
{
    public static class CompareExtention
    {
        public static bool IsEqual<T>(this IEnumerable<T> source, IEnumerable<T> target)
        {
            HashSet<T> srs = new HashSet<T>(source);

            if (srs.Count != target.Count()) return false;
            foreach (T t in target)
            {
                if (!srs.Contains(t))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsEquals(this XYZ a, XYZ b)
        {
            return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
        }
        public static int Hash(this XYZ a) 
        {
            return a.X.GetHashCode() + a.Y.GetHashCode() + a.Z.GetHashCode();
        }
    }
}