﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.Xtensions
{
    public static class MathExtension
    {
        public static double RoundOff(this double i, double num)
        {
            return Math.Round(i / num) * num;
        }
        public static double RoundUp(this double i, double num)
        {
            return Math.Ceiling(i / num) * num;
        }
        public static double RoundDown(this double i, double num)
        {
            return Math.Floor(i / num) * num;
        }

        public static string GetElapsedTime(this Int64 ms)
        {
            TimeSpan t = TimeSpan.FromMilliseconds(ms);
            string hour = t.Hours == 0 ? string.Empty : string.Format("{0:D2}h:", t.Hours);
            string min = t.Minutes == 0 ? string.Empty : string.Format("{0:D2}m:", t.Minutes);
            string sec = t.Seconds == 0 ? string.Empty : string.Format("{0:D2}s:", t.Seconds);
            string msc = string.Format("{0:D3}ms", t.Milliseconds);

            return $"{hour}{min}{sec}{msc}";
        }
    }
}
