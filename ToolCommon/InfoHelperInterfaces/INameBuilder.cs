﻿using Autodesk.Revit.DB;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public interface INameBuilder
    {
        string GetName(Parameter parameter);
    }
}
