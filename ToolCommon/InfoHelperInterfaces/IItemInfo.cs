﻿using System.Collections.Generic;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public interface IItemInfo
    {
        string Name { get; }
        string UniqueId { get; }
        IEnumerable<IParameterInfo> Parameters { get; }
    }
}
