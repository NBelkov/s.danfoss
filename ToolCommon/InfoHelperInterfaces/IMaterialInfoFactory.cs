﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.InfoHelper;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public interface IMaterialInfoFactory
    {
        IEnumerable<IItemInfo> GetMaterialInfos(Element element);
    }
}
