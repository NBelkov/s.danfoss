﻿using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public interface IItemInfoFactory
    {
        IItemInfo GetItemInfo(Element e);
        IEnumerable<IItemInfo> GetItemInfos(IEnumerable<Element> elements);
    }
}
