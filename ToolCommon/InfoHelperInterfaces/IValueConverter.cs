﻿using Autodesk.Revit.DB;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public interface IValueConverter
    {
        double Convert(double value, UnitType unitTupe, DisplayUnitType displayUnitType);
        double Convert(Parameter p);
    }
}
