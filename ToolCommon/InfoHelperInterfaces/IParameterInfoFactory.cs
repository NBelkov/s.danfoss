﻿#define prod
using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public interface IParameterInfoFactory
    {
        IParameterInfo GetParameterInfo(Parameter parameter);
        IEnumerable<IParameterInfo> GetParameterInfos(Element element);
    }

    public abstract class AbstractParameterFactory : IParameterInfoFactory
    {
        protected INameValueHanler nvh;
        public AbstractParameterFactory(INameValueHanler nameValueHanler)
        {
            nvh = nameValueHanler;
        }
        public virtual IParameterInfo GetParameterInfo(Parameter p) => nvh.HandleParameter(p);
#if test
        public virtual IParameterInfo GetParameterInfo(Parameter p)
        {
                if ((BuiltInParameter)p.Id.IntegerValue == BuiltInParameter.ELEM_FAMILY_PARAM)
                {
                    return new StringParamInfo()
                    {
                        Name = p.Definition.Name,
                        Value = "",
                    };
                }
            return nvh.HandleParameter(p);
        }
#endif
        public abstract IEnumerable<IParameterInfo> GetParameterInfos(Element element);
    }
}
