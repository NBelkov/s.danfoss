﻿using Autodesk.Revit.DB;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public interface INameValueHanler
    {
        IParameterInfo HandleParameter(Parameter p);
    }
}
