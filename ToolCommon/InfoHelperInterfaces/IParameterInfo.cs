﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.JsonConverters;
using Newtonsoft.Json;

namespace S.Danfoss.CommonTools.InfoHelper
{
    public interface IParameterInfo
    {
        string Name { get; set; }
    }

    [JsonConverter(typeof(DoubleParamInfoJsonConverter))]
    public class DoubleParamInfo : IParameterInfo
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public DoubleParamInfo()
        {
            Name = string.Empty;
            Value = 0;
        }
        public override bool Equals(object o)
        {
            DoubleParamInfo other = o as DoubleParamInfo;
            if (other is null) return false;
            return Name.Equals(other.Name) & Value.Equals(other.Value);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Value.GetHashCode();
        }
    }

    [JsonConverter(typeof(IntParamInfoJsonConverter))]
    public class IntParamInfo : IParameterInfo
    {
        public string Name { get; set; }
        public int Value { get; set; }
        public IntParamInfo()
        {
            Name = string.Empty;
            Value = 0;
        }
        public override bool Equals(object o)
        {
            IntParamInfo other = o as IntParamInfo;
            if (other is null) return false;
            return Name.Equals(other.Name) & Value.Equals(other.Value);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Value.GetHashCode();
        }
    }

    [JsonConverter(typeof(IdParamInfoJsonConverter))]
    public class IdParamInfo : IParameterInfo
    {
        public string Name { get; set; }
        public ElementId Value { get; set; }
        public IdParamInfo()
        {
            Name = string.Empty;
        }
        public override bool Equals(object o)
        {
            IdParamInfo other = o as IdParamInfo;
            if (other is null) return false;
            return Name.Equals(other.Name) & Value.Equals(other.Value);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Value.GetHashCode();
        }
    }

    [JsonConverter(typeof(StringParamInfoJsonConverter))]
    public class StringParamInfo : IParameterInfo
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public StringParamInfo()
        {
            Name = string.Empty;
            Value = string.Empty;
        }
        public override bool Equals(object o)
        {
            StringParamInfo other = o as StringParamInfo;
            if (other is null) return false;
            return Name == other.Name & Value == other.Value;
        }

        public override int GetHashCode()
        {
            return Value is null ? Name.GetHashCode() : Name.GetHashCode() ^ Value.GetHashCode();
        }

    }


    //[JsonConverter(typeof(DoubleParamInfoJsonConverter))]
    //public class DoubleParamInfo : IParameterInfo
    //{
    //    public string Name { get; set; }
    //    public double Value { get; set; }
    //}

    //[JsonConverter(typeof(IntParamInfoJsonConverter))]
    //public class IntParamInfo : IParameterInfo
    //{
    //    public string Name { get; set; }
    //    public int Value { get; set; }
    //}

    //[JsonConverter(typeof(StringParamInfoJsonConverter))]
    //public class StringParamInfo : IParameterInfo
    //{
    //    public string Name { get; set; }
    //    public string Value { get; set; }
    //}

    //[JsonConverter(typeof(IdParamInfoJsonConverter))]
    //public class IdParamInfo : IParameterInfo
    //{
    //    public string Name { get; set; }
    //    public ElementId Value { get; set; }
    //}
}
