﻿using S.Danfoss.CommonTools.InfoHelper;
using S.Danfoss.CommonTools.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.JsonConverters
{
    public abstract class IInfoConverter : JsonConverter
    {
        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer) => throw new NotImplementedException();
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null) return;

            writer.WriteStartObject();
            WriteBody(writer, value);
            writer.WriteEndObject();
        }

        protected abstract void WriteBody(JsonWriter writer, object value);
    }

    public class ElementInfoConverter : IInfoConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(ElementInfo);

        protected override void WriteBody(JsonWriter writer, object value)
        {
            var val = (ElementInfo)value;

            HashSet<IParameterInfo> parameters = new HashSet<IParameterInfo>(val.Parameters);
            parameters.UnionWith(val.TypeParameters);
            string paramConvert = JsonConvert.SerializeObject(parameters.OrderBy(p => p.Name));

            List<IItemInfo> materials = new List<IItemInfo>(val.VolumeMateriales.OrderBy(m => m.Name));
            materials.AddRange(val.PaintMateriales.OrderBy(m => m.Name));
            string materialConvert = JsonConvert.SerializeObject(materials);

            writer.WritePropertyName("name");
            writer.WriteValue(val.Name);

            writer.WritePropertyName("uniqueId");
            writer.WriteValue(val.UniqueId);

            writer.WritePropertyName("date");
            //writer.WriteValue(DateHelper.NowDateTimeAsString);
            writer.WriteValue(val.Date);

            //writer.WritePropertyName("file_Name");
            writer.WritePropertyName("fileName");
            writer.WriteValue(val.FileName);

            writer.WritePropertyName("parameters");
            writer.WriteRawValue(paramConvert);

            writer.WritePropertyName("materials");
            writer.WriteRawValue(materialConvert);
        }
    }

    public class FamilyInstanceConverter : ElementInfoConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(FamilyInstanceInfo);

        protected override void WriteBody(JsonWriter writer, object value)
        {
            var val = (FamilyInstanceInfo)value;

            HashSet<IParameterInfo> parameters = new HashSet<IParameterInfo>(val.Parameters);
            parameters.UnionWith(val.TypeParameters);
            string paramConvert = JsonConvert.SerializeObject(parameters.OrderBy(p => p.Name));

            List<IItemInfo> materials = new List<IItemInfo>(val.VolumeMateriales.OrderBy(m => m.Name));
            materials.AddRange(val.PaintMateriales.OrderBy(m => m.Name));
            string materialConvert = JsonConvert.SerializeObject(materials);

            writer.WritePropertyName("name");
            writer.WriteValue(val.Name);

            writer.WritePropertyName("uniqueId");
            writer.WriteValue(val.UniqueId);

            writer.WritePropertyName("date");
            //writer.WriteValue(DateHelper.NowDateTimeAsString);
            writer.WriteValue(val.Date);

            //writer.WritePropertyName("file_Name");
            writer.WritePropertyName("fileName"); 
            writer.WriteValue(val.FileName);

            writer.WritePropertyName("parentId");
            writer.WriteValue(val.ParentId);

            //writer.WritePropertyName("SubcomponentsIds");
            //writer.WriteStartArray();
            //foreach (var item in val.SubcomponentsIds)
            //{
            //    writer.WriteValue(item);
            //}
            //writer.WriteEndArray();

            writer.WritePropertyName("parameters");
            writer.WriteRawValue(paramConvert);

            writer.WritePropertyName("materials");
            writer.WriteRawValue(materialConvert);
        }
    }
}
