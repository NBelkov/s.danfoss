﻿using S.Danfoss.CommonTools.InfoHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.JsonConverters
{

    public abstract class ParameterInfoJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(IParameterInfo);

        public override object ReadJson(JsonReader reader,
                                        Type objectType,
                                        object existingValue,
                                        JsonSerializer serializer) => throw new NotImplementedException();
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null) return;

            writer.WriteStartObject();
            var val = (IParameterInfo)value;
            writer.WritePropertyName(val.Name);
            writer.WriteEndObject();
        }
    }

    public class DoubleParamInfoJsonConverter : ParameterInfoJsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(DoubleParamInfo);
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null) return;
            var val = (DoubleParamInfo)value;

            writer.WriteStartObject();
            writer.WritePropertyName(val.Name);
            writer.WriteValue((float)val.Value);
            writer.WriteEndObject();
        }
    }

    public class IntParamInfoJsonConverter : ParameterInfoJsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(IntParamInfo);
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null) return;

            writer.WriteStartObject();
            var val = (IntParamInfo)value;
            writer.WritePropertyName(val.Name);
            writer.WriteValue(val.Value);
            writer.WriteEndObject();
        }
    }

    public class StringParamInfoJsonConverter : ParameterInfoJsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(StringParamInfo);
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null) return;

            writer.WriteStartObject();
            var val = (StringParamInfo)value;
            writer.WritePropertyName(val.Name);
            writer.WriteValue(val.Value);
            writer.WriteEndObject();
        }
    }

    public class IdParamInfoJsonConverter : ParameterInfoJsonConverter
    {
        public override bool CanConvert(Type objectType) => objectType == typeof(IdParamInfo);
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null) return;

            writer.WriteStartObject();
            var val = (IdParamInfo)value;
            writer.WritePropertyName(val.Name);
            writer.WriteValue(val.Value.IntegerValue);
            writer.WriteEndObject();
        }
    }
}
