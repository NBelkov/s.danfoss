﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.RevitObjectsProvider
{
    public class DefinitionBicRequest : IBicRequest
    {
        private IEnumerable<InternalDefinition> r;
        public DefinitionBicRequest(IEnumerable<InternalDefinition> request)
        {
            r = request;
        }

        public bool Contains(ParameterElement pe) => r.Any(def => pe.GetDefinition() == def);
        public bool Contains(InternalDefinition inDef) => r.Any(def => def.Equals(inDef));
    }

}