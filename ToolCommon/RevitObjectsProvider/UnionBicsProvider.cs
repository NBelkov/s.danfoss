﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.RevitObjectsProvider
{
    public class UnionBicsProvider : BicsProvider
    {
        public UnionBicsProvider(Document document) : base(document)
        {
        }

        protected override IEnumerable<BuiltInCategory> Combine(HashSet<BuiltInCategory> main, IEnumerable<BuiltInCategory> input)
        {
            if (main.Any())
            {
                if (input.Any())
                {
                    return main.Union(input);
                }
                else
                {
                    return main;
                }
            }
            else
            {
                if (input.Any()) return input;
                else return new List<BuiltInCategory>();
            }
        }
    }

}