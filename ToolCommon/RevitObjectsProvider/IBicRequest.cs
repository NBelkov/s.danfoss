﻿using Autodesk.Revit.DB;

namespace S.Danfoss.CommonTools.RevitObjectsProvider
{
    public interface IBicRequest
    {
        bool Contains(ParameterElement pe);
    }

}