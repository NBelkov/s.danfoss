﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.RevitObjectsProvider
{
    public class GuidBicRequest : IBicRequest
    {
        private IEnumerable<Guid> r;
        public GuidBicRequest(IEnumerable<Guid> request)
        {
            r = request;
        }

        public bool Contains(ParameterElement pe) => r.Any(guid => Compare(pe, guid));
        private bool Compare(ParameterElement p, Guid guid) => p is SharedParameterElement s ? s.GuidValue == guid : false;

    }
}