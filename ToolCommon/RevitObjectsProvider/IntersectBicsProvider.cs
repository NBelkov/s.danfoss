﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.RevitObjectsProvider
{
    public class IntersectBicsProvider : BicsProvider
    {
        public IntersectBicsProvider(Document document) : base(document)
        {
        }

        protected override IEnumerable<BuiltInCategory> Combine(HashSet<BuiltInCategory> main, IEnumerable<BuiltInCategory> input)
        {
            if (main.Any())
            {
                if (input.Any())
                {
                    return main.Intersect(input);
                }
                else
                {
                    return main;
                }
            }
            else
            {
                if (input.Any()) return input;
                else return new List<BuiltInCategory>();
            }
        }
    }

}