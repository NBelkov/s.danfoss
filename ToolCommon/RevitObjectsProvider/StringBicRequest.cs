﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.RevitObjectsProvider
{
    public class StringBicRequest : IBicRequest
    {
        private IEnumerable<string> r;
        public StringBicRequest(IEnumerable<string> request)
        {
            r = request;
        }

        public bool Contains(ParameterElement pe) => r.Any(name => name.Equals(pe.Name));

    }

}