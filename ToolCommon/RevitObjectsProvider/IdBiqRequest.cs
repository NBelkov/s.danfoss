﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.RevitObjectsProvider
{
    public class IdBiqRequest : IBicRequest
    {
        private IEnumerable<ElementId> r;
        public IdBiqRequest(IEnumerable<ElementId> request)
        {
            r = request;
        }
        public bool Contains(ParameterElement pe) => r.Any(id => pe.Id.Equals(id));

    }
}