﻿using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.RevitObjectsProvider
{
    public abstract class BicsProvider : IBicsProvider
    {
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected Document doc;

        public BicsProvider(Document document)
        {
            doc = document;
        }

        public IEnumerable<BuiltInCategory> GetBics(IBicRequest request)
        {
            var it = GetBindingIterator();

            HashSet<BuiltInCategory> output = new HashSet<BuiltInCategory>();

            while (it.MoveNext())
            {
                InternalDefinition definition = (InternalDefinition)it.Key;
                ParameterElement paramElem = doc.GetElement(definition.Id) as ParameterElement;

                if (paramElem is null) continue;

                if (request.Contains(paramElem))
                {
                    var binding = (ElementBinding)it.Current;
                    output = new HashSet<BuiltInCategory>(Combine(output, GetBics(binding)));
                }
            }

            return output;
        }

        protected DefinitionBindingMapIterator GetBindingIterator()
        {
            BindingMap bindingMap = doc.ParameterBindings;
            DefinitionBindingMapIterator it = bindingMap.ForwardIterator();
            it.Reset();
            return it;
        }
        protected IEnumerable<BuiltInCategory> GetBics(ElementBinding binding)
        {
            List<BuiltInCategory> result = new List<BuiltInCategory>();
            foreach (var obj in binding.Categories)
            {
                var cat = obj as Category;
                result.Add((BuiltInCategory)(cat.Id.IntegerValue));
            }
            return result;
        }

        //protected bool Compare(ParameterElement p, InternalDefinition definition) => p.GetDefinition() == definition;
        //protected bool Compare(InternalDefinition definition1, InternalDefinition definition2) => definition1 == definition2;
        //protected bool Compare(ParameterElement p, string name) => p.Name == name;
        //protected bool Compare(ParameterElement p, Guid guid) => p is SharedParameterElement s ? s.GuidValue == guid : false;
        //protected bool Compare(ParameterElement p, object o)
        //{
        //    switch (o)
        //    {
        //        case Guid guid: return Compare(p, guid);
        //        case InternalDefinition definition: return Compare(p, definition);
        //        case string name: return Compare(p, name);
        //        default: return false;
        //    }
        //}

        protected abstract IEnumerable<BuiltInCategory> Combine(HashSet<BuiltInCategory> main, IEnumerable<BuiltInCategory> input);
    }

}