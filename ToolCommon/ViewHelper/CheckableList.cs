﻿using S.Danfoss.CommonTools.ExternalCommands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace S.Danfoss.CommonTools.ViewHelper
{
    /// <summary>
    /// The CheckableList<T> class 
    /// This class use when it needs to save the binary state of element in collection
    /// Source collection contains all elements
    /// Filtered collection contains only elements with true state (Checked)
    /// </summary>
    /// <typeparam name="T">type of element</typeparam>
    public class CheckableList<T> 
    {
        #region Variables

        protected Func<T, string> NameBuilder;
        protected ObservableCollection<CheckableItem<T>> _filtered =
            new ObservableCollection<CheckableItem<T>>();

        public ObservableCollection<CheckableItem<T>> Source { get; set; }
        public ObservableCollection<CheckableItem<T>> Filtered { get => _filtered; }

        #endregion
        #region Constructor

        /// <summary>
        /// Constructor.
        /// Readable name take as element.ToString()
        /// </summary>
        public CheckableList()
        {
            Source = new ObservableCollection<CheckableItem<T>>();
            NameBuilder = i => i.ToString();
        }

        /// <summary>
        /// Constructor with object.
        /// Readable name take as element.ToString()
        /// </summary>
        /// <param name="_source">Element</param>
        public CheckableList(ICollection<T> _source)
        {
            Source = new ObservableCollection<CheckableItem<T>>();
            NameBuilder = i => i?.ToString();
            UpdateSource(_source, NameBuilder);
        }

        /// <summary>
        /// Constructor with object and function that describe how name of element is set up
        /// </summary>
        /// <param name="_source">Element</param>
        /// <param name="nameBuilder">Function to convert object to readable name</param>
        public CheckableList(ICollection<T> _source, Func<T, string> nameBuilder)
        {
            Source = new ObservableCollection<CheckableItem<T>>();
            NameBuilder = nameBuilder;
            UpdateSource(_source, NameBuilder);
        }

        #endregion
        #region Commands

        public ICommand SelectAllCommand
        {
            get
            {
                return new RelayCommand
                {
                    ExecuteAction = a =>
                    {
                        SelectAll();
                    },
                    CanExecutePredicate = p =>
                    {
                        return Source.Count > Filtered.Count;
                    }
                };
            }
        }
        public ICommand SelectNoneCommand
        {
            get
            {
                return new RelayCommand
                {
                    ExecuteAction = a =>
                    {

                        ClearAll();
                    },
                    CanExecutePredicate = p =>
                    {
                        return Filtered.Any();
                    }
                };
            }
        }

        #endregion
        #region Methods

        protected void UpdateSource(ICollection<T> _source, Func<T, string> nameBuilder)
        {
            if (_source is null) return;
            foreach (var item in _source)
            {
                Source.Add(new CheckableItem<T>(ref _filtered, nameBuilder)
                { Item = item, IsChecked = false });
            }
        }

        public void SetSelection(CheckableItem<T> sourceItem, bool flag)
        {
            if (sourceItem.IsChecked != flag)
            {
                sourceItem.IsChecked = flag;
            }
        }

        public void SelectAll()
        {
            foreach (var item in Source)
            {
                item.IsChecked = true;
            }
        }

        public void ClearAll()
        {
            foreach (var item in Source)
            {
                item.IsChecked = false;
            }
        }

        public void InvertSelection()
        {
            foreach (var item in Source)
            {
                if (item.IsChecked) item.IsChecked = false;
                else item.IsChecked = true;
            }
        }

        public bool IsAllSelected()
        {
            return Source.Count == Filtered.Count;
        }

        /// <summary>
        /// Refresh collection by new one. 
        /// Add new elements with state false.
        /// Remove old from Source and Filtered collection
        /// </summary>
        /// <param name="newSource">a new source to the collection</param>
        public void RefreshSource(ICollection<T> newSource)
        {
            if (Source is null || newSource is null) return;
            List<CheckableItem<T>> toAdd = new List<CheckableItem<T>>();
            List<CheckableItem<T>> toRemove = new List<CheckableItem<T>>();

            toRemove = Source
                .Where(x => !newSource.Any(c => c.Equals(x.Item)))
                .Select(x => { x.IsChecked = false; return x; })
                .ToList();

            toAdd = newSource
                .Where(c => !Source.Any(x => c.Equals(x.ToString())))
                .Select(c => new CheckableItem<T>(ref _filtered, NameBuilder) { Item = c, IsChecked = false })
                .ToList();

            toAdd.ForEach(Source.Add);
            toRemove.ForEach(x => Source.Remove(x));
        }

        #endregion
    }

    /// <summary>
    /// CheckableItem class has in keep element, it's readable Name and a state
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CheckableItem<T> : INotifyPropertyChanged
    {
        #region Variables

        protected bool _isChecked;
        protected T _item;
        protected ObservableCollection<CheckableItem<T>> _checked;
        protected Func<T, string> nameBuilder;

        #endregion
        #region Contructor

        public CheckableItem(ref ObservableCollection<CheckableItem<T>> Checked)
        {
            _checked = Checked;
            this.nameBuilder = i => i.ToString();
        }

        public CheckableItem(ref ObservableCollection<CheckableItem<T>> Checked, Func<T, string> nameBuilder)
        {
            _checked = Checked;
            this.nameBuilder = nameBuilder;
        }

        #endregion
        #region Properties

        public T Item
        {
            get => _item;
            set
            {
                _item = value;
                Name = nameBuilder(value);
            }
        }
        public string Name { get; private set; }

        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                if (value)
                {
                    if (!_checked.Contains(this))
                    {
                        _checked.Add(this);
                        NotifyPropertyChanged("IsChecked");
                    }
                }
                else
                {
                    if (_checked.Contains(this))
                    {
                        _checked.Remove(this);
                        NotifyPropertyChanged("IsChecked");
                    }
                }
            }
        }

        #endregion
        #region Methods

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            return obj is CheckableItem<T> item &&
                   EqualityComparer<T>.Default.Equals(Item, item.Item);
        }

        public override int GetHashCode()
        {
            return -979861770 + EqualityComparer<T>.Default.GetHashCode(Item);
        }

        #endregion
    }
}