﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;

namespace S.Danfoss.CommonTools.ViewHelper.UniqueChoseItemCollection
{
    public abstract class IACollectionViewModel<T> : ICollectionViewModel<T> where T : class
    {
        #region Fields

        protected ObservableCollection<T> source;

        public ObservableCollection<IAItemVM<T>> ItemCollection { get; protected set; }

        #endregion
        #region Constructor

        public IACollectionViewModel(IEnumerable<T> sourceCollection)
        {
            ItemCollection = new ObservableCollection<IAItemVM<T>>();
            source = new ObservableCollection<T>(sourceCollection);
        }

        #endregion
        #region Methods

        public virtual IEnumerable<T> GetSelected() => ItemCollection.Select(s => s.Selected);

        public virtual void AddItem(IAItemVM<T> item)
        {
            item.ItemSource = new ListCollectionView(source);
            item.SetFirstSelection();
            ItemCollection.Add(item);
        }

        public virtual void RemoveItem(IAItemVM<T> item)
        {
            if (ItemCollection.Contains(item))
            {
                ItemCollection.Remove(item);
            }
        }

        public virtual void RemoveAll()
        {
            if (ItemCollection.Any())
            {
                ItemCollection.Clear();
            }
        }

        #endregion
    }
}
