﻿using System.Collections.ObjectModel;

namespace S.Danfoss.CommonTools.ViewHelper.UniqueChoseItemCollection
{
    public interface ICollectionViewModel<T> where T : class
    {
        ObservableCollection<IAItemVM<T>> ItemCollection { get; }
    }
}
