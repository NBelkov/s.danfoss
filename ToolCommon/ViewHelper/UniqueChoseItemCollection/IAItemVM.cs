﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;

namespace S.Danfoss.CommonTools.ViewHelper.UniqueChoseItemCollection
{
    public abstract class IAItemVM<T> : INotifyPropertyChanged where T : class
    {
        #region Fields

        public event PropertyChangedEventHandler PropertyChanged;

        protected T selected;
        protected ObservableCollection<T> excluding = new ObservableCollection<T>();

        #endregion
        #region Properties

        public T Selected
        {
            get => selected;
            set
            {
                if (selected != value)
                {
                    var old = selected;
                    selected = value;
                    NotifyPropertyChanged(old, value);
                }
            }
        }
        public ListCollectionView ItemSource { get; set; }

        #endregion
        #region Methods

        public void SetFirstSelection()
        {
            foreach (var item in ItemSource)
            {
                if (item is T t)
                {
                    Selected = t;
                    return;
                }
            }
        }

        public void UpdateFilter(ObservableCollection<T> usedItems)
        {
            excluding = usedItems;
            ItemSource.Filter = null;
            ItemSource.Filter += FilterCollection;
        }


        protected bool FilterCollection(object obj)
        {
            if (obj is T item)
            {
                if (item != selected && excluding.Contains(item)) return false;
                else return true;
            }
            return false;
        }

        protected void NotifyPropertyChanged(T oldValue, T newValue, [CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new NewOldPropertyChangedEventArgs<T>(oldValue, newValue, propertyName));
        }

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
