﻿using System.Collections.Generic;

namespace S.Danfoss.CommonTools.ViewHelper.UniqueChoseItemCollection
{
    public class DuplicateItemCollectionVM<T> : IACollectionViewModel<T> where T : class
    {
        #region Constructor

        public DuplicateItemCollectionVM(IEnumerable<T> sourceCollection) : base(sourceCollection) { }

        #endregion
    }
}
