﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace S.Danfoss.CommonTools.ViewHelper.UniqueChoseItemCollection
{
    public class UniqueItemCollectionVM<T> : IACollectionViewModel<T> where T : class
    {
        #region Fields

        protected int MaxCount = 0;
        protected ObservableCollection<T> excluding = new ObservableCollection<T>();

        #endregion
        #region Constructor

        public UniqueItemCollectionVM(IEnumerable<T> sourceCollection) : base(sourceCollection)
        {
            MaxCount = sourceCollection.Count();
            excluding.CollectionChanged += OnExcludingChanged;
            ItemCollection.CollectionChanged += OnItemCollectionChanged;
        }

        #endregion
        #region Methods

        public override void AddItem(IAItemVM<T> item)
        {
            if (ItemCollection.Count < MaxCount)
            {
                ItemCollection.Add(item);
            }
        }

        public override void RemoveAll()
        {
            if (ItemCollection.Any())
            {
                foreach (var item in ItemCollection)
                {
                    ItemRemoved(item);
                }
                ItemCollection.Clear();
            }
        }

        protected virtual void OnItemCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (var newItem in e.NewItems)
                        {
                            if (newItem is IAItemVM<T> item)
                            {
                                item.ItemSource = new ListCollectionView(source);
                                item.PropertyChanged += OnItemChanged;
                                item.UpdateFilter(excluding);
                                if (item.Selected is null) item.SetFirstSelection();
                            }
                        }
                        break;
                    }
                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (var newItem in e.OldItems)
                        {
                            if (newItem is IAItemVM<T> item)
                            {
                                ItemRemoved(item);
                            }
                        }
                        break;
                    }
                default:
                    break;
            }
        }

        private void ItemRemoved(IAItemVM<T> item)
        {
            item.PropertyChanged -= OnItemChanged;
            excluding.Remove(item.Selected);
        }

        private void OnItemChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Selected")
            {
                var snd = sender as IAItemVM<T>;
                if (e is NewOldPropertyChangedEventArgs<T> ea)
                {
                    UpdateExcludings(ea.OldValue, ea.NewValue);
                }
                else
                {
                    throw new NotImplementedException("Item should notify about its changes with NewOldPropertyChangedEventArgs");
                }
            }
        }

        private void OnExcludingChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (ItemCollection is null) return;
            foreach (var job in ItemCollection)
            {
                job.UpdateFilter(excluding);
            }
        }

        private void UpdateExcludings(T oldValue, T newValue)
        {
            if (newValue == oldValue) return;
            if (newValue != null && oldValue != null)
            {
                if (excluding.Contains(oldValue))
                {
                    // if the previously selected value is contained in the exclusion replace it with new one
                    var index = excluding.IndexOf(oldValue);
                    excluding[index] = newValue;
                }
            }
            else if (newValue == null)
            {
                // if the newValue is null remove previously selected value fron exclusion
                if (excluding.Contains(oldValue))
                {
                    excluding.Remove(oldValue);
                }
            }
            else if (oldValue == null)
            {
                // if there is no selection has been done yet add the new Value to exclusion
                if (!excluding.Contains(newValue))
                {
                    excluding.Add(newValue);
                }
            }
        }

        #endregion
    }
}
