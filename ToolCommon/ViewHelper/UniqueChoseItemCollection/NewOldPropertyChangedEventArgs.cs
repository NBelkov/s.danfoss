﻿using System.ComponentModel;

namespace S.Danfoss.CommonTools.ViewHelper.UniqueChoseItemCollection
{
    public class NewOldPropertyChangedEventArgs<Person> : PropertyChangedEventArgs
    {
        public Person OldValue { get; private set; }
        public Person NewValue { get; private set; }
        public NewOldPropertyChangedEventArgs(Person oldItem, Person newItem, string propertyName) : base(propertyName)
        {
            OldValue = oldItem;
            NewValue = newItem;
        }
    }
}
