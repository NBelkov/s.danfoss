﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.ViewHelper
{
    public interface ITreeItem<T>
    {
        string Name { get; set; }
        bool IsExpanded { get; set; }
        bool IsSelected { get; set; }
        ICollection<T> Elements { get; set; }
        int Count { get; }
    }


    public class Branch<T> : ITreeItem<T>
    {
        public string Name { get; set; }
        public bool IsExpanded { get; set; } = true;
        public bool IsSelected { get; set; } = false;
        public int Count { get => Elements.Count; }
        public ICollection<T> Elements { get; set; }
        public List<ITreeItem<T>> ElementsOnBranch { get; set; } = new List<ITreeItem<T>>();
        public override string ToString() => $"{Name}  ({Count})";
    }


    public class Leaf<T> : ITreeItem<T>
    {
        public string Name { get; set; }
        public bool IsExpanded { get; set; } = false;
        public bool IsSelected { get; set; } = false;
        public int Count { get => Elements.Count; }
        public ICollection<T> Elements { get; set; }
        public override string ToString() => $"{Name}  ({Count})";
    }


    public static class ItemTreeExtentions
    {
        public static ICollection<T> GetAllElementsOnBranch<T>(this ITreeItem<T> treeItem)
        {
            HashSet<T> result = new HashSet<T>();
            switch (treeItem)
            {
                case Leaf<T> l:
                    {
                        return l.Elements;
                    }
                case Branch<T> b:
                    {
                        foreach (var item in b.ElementsOnBranch)
                        {
                            result.UnionWith(GetAllElementsOnBranch<T>(item));
                        }
                        break;
                    }
                default: break;
            }
            return result;
        }

        public static ICollection<T> GetAllElementsOnBranch<T>(this IEnumerable<ITreeItem<T>> treeItems)
        {
            HashSet<T> result = new HashSet<T>();
            foreach (var treeItem in treeItems)
            {
                result.UnionWith(treeItem.GetAllElementsOnBranch<T>());
            }
            return result;
        }
    }
}
