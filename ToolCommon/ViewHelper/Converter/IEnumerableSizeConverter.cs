﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace S.Danfoss.CommonTools.ViewHelper.Converter
{
    public class IEnumerableSizeConverter<T, U> : IValueConverter
    {
        public IEnumerableSizeConverter(T trueValue, T falseValue)
        {
            True = trueValue;
            False = falseValue;
        }

        public T True { get; set; }
        public T False { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is IEnumerable<U> c && c.Any() ? True : False;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public sealed class IEnumerableToVisibilityConverter : IEnumerableSizeConverter<Visibility, DataGridCellInfo>
    {
        public IEnumerableToVisibilityConverter() :
            base(Visibility.Visible, Visibility.Collapsed)
        { }
    }
}
