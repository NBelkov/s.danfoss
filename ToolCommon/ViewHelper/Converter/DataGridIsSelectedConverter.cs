﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace S.Danfoss.CommonTools.ViewHelper.Converter
{
    public class DataGridIsSelectedConverter<T> : IValueConverter
    {
        public DataGridIsSelectedConverter(T trueValue, T falseValue)
        {
            True = trueValue;
            False = falseValue;
        }

        public T True { get; set; }
        public T False { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is System.Windows.Controls.DataGrid dg && (dg.SelectedCells.Any()) ? True : False;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class DataGridIsSelectedToVisibilityConverter : BooleanConverter<Visibility>
    {
        public DataGridIsSelectedToVisibilityConverter() :
            base(Visibility.Visible, Visibility.Collapsed)
        { }
    }

}
