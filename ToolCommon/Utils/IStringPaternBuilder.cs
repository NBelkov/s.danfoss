﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.Utils
{
    public interface IStringPaternBuilder
    {
        string Patern { get; }
    }


    public class ConcretePatternBuilder : IStringPaternBuilder
    {
        private string patern;
        public ConcretePatternBuilder()
        {
            string concreteName = @"[bвб]";
            string nameDevidor = @"[\s\-_]*?";
            string decimalSymbol = @"[\s\.,-_]";
            string concreteArray = $@"(3{decimalSymbol}5|5|7{decimalSymbol}5|10|12{decimalSymbol}5|15|20|22{decimalSymbol}5|25|30|35|40|45|50|55|60|70|80|90|100)\D?";
            patern = $@"{concreteName}{nameDevidor}{concreteArray}";
        }

        public string Patern => patern;
    }

    public class RebarSteelPatternBuilder : IStringPaternBuilder
    {
        private string patern;
        public RebarSteelPatternBuilder()
        {
            string rebarDevidor = @"[\s\-_]*?";
            patern = $@"([aа]{rebarDevidor}240|[aа]{rebarDevidor}300|[aа]{rebarDevidor}400|[abав]{rebarDevidor}500|[aа]{rebarDevidor}600|[aа]{rebarDevidor}800|[aа]{rebarDevidor}1000)\D?";
        }

        public string Patern => patern;
    }



    public interface IStringParcer
    {
        string[] GetMatches(string input);
    }

    public class AllMatchesParser : IStringParcer
    {
        private Regex rx;
        public AllMatchesParser(IStringPaternBuilder paternBuilder)
        {
            rx = new Regex(paternBuilder.Patern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        }

        public string[] GetMatches(string input)
        {
            MatchCollection matches = rx.Matches(input);
            if (matches.Count == 0) return new string[0];

            return matches
                .OfType<Match>()
                .Select(m => m.Value)
                .ToArray();
        }
    }

    public interface INumericStringParcer
    {
        double[] GetNumbers(string input);
    }

    public class AllNumbersInStringParcer : INumericStringParcer
    {
        protected string patern;
        protected string systemSeparator;
        protected string decimalSeparator;
        protected Regex rx;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


        public AllNumbersInStringParcer()
        {
            decimalSeparator = @"[\.\,\-_]";
            systemSeparator = Regex.Escape(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            patern = $@"\d+({decimalSeparator}\d+)?";
            rx = new Regex(patern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        }

        public double[] GetNumbers(string input)
        {
            ICollection<double> result = new List<double>();
            MatchCollection matches = rx.Matches(input);

            foreach (var part in matches)
            {
                double output;
                string st = Regex.Replace(part.ToString(), decimalSeparator, systemSeparator);
                if (Double.TryParse(st, out output))
                {
                    result.Add(output);
                }
                else
                {
                    log.Debug($"Cannot convert string {part} to double");
                }

            }
            return result.ToArray();
        }
    }

}
