﻿namespace S.Danfoss.CommonTools.Utils
{
    public interface ISettingsProvider : ISettingsReader, ISettingsWriter { }
}
