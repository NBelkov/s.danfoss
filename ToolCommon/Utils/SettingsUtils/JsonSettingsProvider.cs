﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace S.Danfoss.CommonTools.Utils
{
    public class JsonSettingsProvider : ISettingsProvider
    {
        private readonly string _configurationFilePath;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            ContractResolver = new SettingsReaderContractResolver(),
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        public JsonSettingsProvider(string configurationFilePath)
        {
            _configurationFilePath = configurationFilePath;
        }

        public T Load<T>() where T : class
        {
            if (ReadAll() is string jsonFile)
            {
                try
                {
                    return JsonConvert.DeserializeObject<T>(jsonFile, JsonSerializerSettings);
                }
                catch (JsonReaderException ex)
                {
                    log.Error($"Ошибка при ЧТЕНИИ конфигурации из файла '{_configurationFilePath}': data: '{jsonFile}'", ex);
                    throw;
                }
            }
            else return null;
        }

        public T LoadSection<T>(string sectionName) where T : class
        {
            if (ReadAll() is string jsonFile)
            {
                try
                {
                    JObject allFile = JObject.Parse(jsonFile);
                    return allFile[sectionName]?.ToObject<T>();
                }
                catch (JsonReaderException ex)
                {
                    log.Error($"Ошибка при ЧТЕНИИ секции {sectionName} из файла конфигурации  '{_configurationFilePath}': data: '{jsonFile}'", ex);
                    throw;
                }
            }
            return null;
        }

        public void Save<T>(T source) where T : class
        {
            using (StreamWriter file = File.CreateText(_configurationFilePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, source);
                file.Close();
            }
        }

        public void SaveSection<T>(T source, string sectionName) where T : class
        {
            if (ReadAll() is string jsonFile)
            {
                try
                {
                    JToken allFile = JToken.Parse(jsonFile);
                    var newVal = JToken.FromObject(source);
                    switch (allFile)
                    {
                        case JObject jo:
                            {
                                if (jo.Property(sectionName) is JProperty jProp) jProp.Remove();
                                jo.Add(sectionName, newVal);
                                Save(allFile);
                                break;
                            }
                        default: break;
                    }
                }
                catch (JsonReaderException ex)
                {
                    log.Error($"Ошибка при ЗАПИСИ секции {sectionName} из файла конфигурации  '{_configurationFilePath}': data: '{jsonFile}'", ex);
                    throw;
                }
            }
        }

        private string ReadAll()
        {
            if (!File.Exists(_configurationFilePath))
            {
                using (StreamWriter file = File.CreateText(_configurationFilePath))
                {
                    file.WriteLine("{}");
                    file.Close();
                }
            }
            return File.ReadAllText(_configurationFilePath);
        }

        private class SettingsReaderContractResolver : DefaultContractResolver
        {
            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                var props = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                    .Select(p => CreateProperty(p, memberSerialization))
                    .Union(type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                        .Select(f => CreateProperty(f, memberSerialization)))
                    .ToList();
                props.ForEach(p =>
                {
                    p.Writable = true;
                    p.Readable = true;
                });

                return props;
            }
        }

    }
}
