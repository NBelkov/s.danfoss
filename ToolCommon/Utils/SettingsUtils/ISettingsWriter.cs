﻿using System;

namespace S.Danfoss.CommonTools.Utils
{
    public interface ISettingsWriter
    {
        void Save<T>(T source) where T : class;
        void SaveSection<T>(T source, string sectionName) where T : class;
    }
}
