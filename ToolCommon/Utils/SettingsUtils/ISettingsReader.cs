﻿namespace S.Danfoss.CommonTools.Utils
{
    public interface ISettingsReader
    {
        T Load<T>() where T : class;
        T LoadSection<T>(string sectionName) where T : class;
    }
}
