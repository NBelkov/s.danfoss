﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.Utils
{
    public class EnvUtils
    {
        public static string GetFilePath(string path, string fileName)
        {
            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }

            return path + $"\\{fileName}";
        }

        public static string GetMyTestPath(string fileName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + $"\\Test";
            return GetFilePath(path, fileName);
        }

        public static string GetMyDocPath(string fileName, string subfolder)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + subfolder;
            return GetFilePath(path, fileName);
        }

        public static string GetMyLocalAppPath(string fileName, string subfolder)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + subfolder;
            return GetFilePath(path, fileName);
        }
    }
}
