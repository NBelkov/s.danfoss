﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.Utils.OutlineUtils
{
    public class PlannarRatioSlicer 
    {
        public List<XYZ[]> Slice(XYZ[] bounds, double k = 1.6)
        {
            var pnt = Ratio(bounds[0], bounds[1], k);
            var result = (new OutlineSlicer()).Slice(bounds, pnt);
            return result;
        }
        private int[] Ratio(XYZ p1, XYZ p2, double k)
        {
            double xLength = Math.Abs(p2.X - p1.X);
            double yLength = Math.Abs(p2.Y - p1.Y);

            double ratio = xLength / yLength;

            int xCount = 2;
            int yCount = 2;

            if (ratio > k)
            {
                xCount = (int)Math.Ceiling(ratio);
                yCount = 1;
            }
            else if (ratio < 1 / k)
            {
                xCount = 1;
                yCount = (int)Math.Ceiling(1 / ratio);
            }
            return new int[3] { xCount, yCount, 1 };
        }
    }
}
