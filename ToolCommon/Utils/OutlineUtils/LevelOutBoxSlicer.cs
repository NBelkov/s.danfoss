﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.Utils.OutlineUtils
{
    public class LevelOutBoxSlicer
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Document doc;
        private double t;

        public LevelOutBoxSlicer(Document document, double tolerance)
        {
            doc = document;
            t = tolerance;
        }

        public List<XYZ[]> Slice(XYZ[] bounds)
        {
            XYZ min = bounds[0];
            XYZ max = bounds[1];
            List<XYZ[]> result = new List<XYZ[]>();
            List<double> input = new List<double>() { min.Z, max.Z };
            input.AddRange(GetLevelMarks(doc).Where(e => min.Z < e && e < max.Z));

            List<double> levels = DistinctAndOrder(input, t).ToList();
            //log.Debug($"Slice. LevelCount {levels.Count}");

            for (int iz = 0; iz < levels.Count - 1; iz++)
            {
                XYZ mn = new XYZ(min.X, min.Y, levels[iz]);
                XYZ mx = new XYZ(max.X, max.Y, levels[iz + 1]);
                result.Add(new XYZ[] { mn, mx });
            }
            return result;
        }


        private IEnumerable<double> GetLevelMarks(Document doc)
        {
            return new FilteredElementCollector(doc)
                .WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_Levels)
                .Cast<Autodesk.Revit.DB.Level>()
                .Select(l => l.Elevation)
                .ToList();
        }
        private SortedSet<double> DistinctAndOrder(IEnumerable<double> target, double tolerance)
        {
            List<double> input = new List<double>(target.OrderBy(d => d));
            //log.Debug($"Input {input.Count}");

            if (tolerance < 0)
                throw new ArgumentException($"Tolerance should be more or equals 0, now it is {tolerance}", "double tolerance");
            if (input.Count < 2)
                throw new ArgumentException($"The input range should contains at least 2 elements, now it is {input.Count}", "IEnumerable<double> target");
            if (input.Last() - input.First() <= tolerance)
                return new SortedSet<double>() { input.First(), input.Last() };

            double last = input.First();
            SortedSet<double> result = new SortedSet<double>() { last };

            for (int i = 1; i < input.Count; i++)
            {
                //log.Debug($"\t{last} - {input[i]} = {Math.Abs(last - input[i])} < {tolerance}");

                if (Math.Abs(last - input[i]) < tolerance /*&& input.Count - 1 != i*/)
                {
                    if (input.Count - 1 == i && result.Any())
                    {
                        result.Remove(result.Last());
                        result.Add(input[i]);
                    }
                    else
                    {
                        continue;
                    }
                }
                last = input[i];
                result.Add(input[i]);
            }
            //log.Debug($"Result {result.Count}");
            return result;
        }
    }
}
