﻿using Autodesk.Revit.DB;
using S.Danfoss.CommonTools.Xtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.Utils.OutlineUtils
{
    public enum Direction
    {
        X,
        Y,
        Z
    }
    public interface BoundariesSearch
    {

    }
    public class BinaryUpperLowerBoundsSearch : BoundariesSearch
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Document doc;

        private double precision;
        private XYZ min;
        private XYZ max;
        private XYZ direction;

        private int iterationCount = 0;

        public BinaryUpperLowerBoundsSearch(Document document, double precision)
        {
            doc = document;
            this.precision = precision;
        }

        /// <summary>
        /// Searches for an area that completely includes all elements within a given precision on XY plane.
        /// The minimum and maximum points are used for the initial assessment. 
        /// The outline build on this points must contain all elements.
        /// </summary>
        /// <param name="minPoint">The minimum point of the BoundBox used for the first approximation.</param>
        /// <param name="maxPoint">The maximum point of the BoundBox used for the first approximation.</param>
        /// <param name="elements">Set of elements</param>
        /// <returns>Returns two points: first is the lower bound, second is the upper bound on XY plane.</returns>
        public XYZ[] GetBoundariesXY(XYZ minPoint, XYZ maxPoint, ICollection<ElementId> elements)
        {
            XYZ[] rx = GetBoundaries(minPoint, maxPoint, elements, Direction.X);
            return GetBoundaries(rx[0], rx[1], elements, Direction.Y);
        }

        /// <summary>
        /// Searches for an area that completely includes all elements within a given precision in 3d-space.
        /// The minimum and maximum points are used for the initial assessment. 
        /// The outline build on this points must contain all elements.
        /// </summary>
        /// <param name="minPoint">The minimum point of the BoundBox used for the first approximation.</param>
        /// <param name="maxPoint">The maximum point of the BoundBox used for the first approximation.</param>
        /// <param name="elements">Set of elements</param>
        /// <returns>Returns two points: first is the lower bound, second is the upper bound in 3d-space.</returns>
        public XYZ[] GetBoundaries(XYZ minPoint, XYZ maxPoint, ICollection<ElementId> elements)
        {
            XYZ[] rx = GetBoundaries(minPoint, maxPoint, elements, Direction.X);
            rx = GetBoundaries(rx[0], rx[1], elements, Direction.Y);
            return GetBoundaries(rx[0], rx[1], elements, Direction.Z);
        }

        /// <summary>
        /// Searches for an area that completely includes all elements within a given precision on a given direction.
        /// The minimum and maximum points are used for the initial assessment. 
        /// The outline build on this points must contain all elements.
        /// </summary>
        /// <param name="minPoint">The minimum point of the BoundBox used for the first approximation.</param>
        /// <param name="maxPoint">The maximum point of the BoundBox used for the first approximation.</param>
        /// <param name="elements">Set of elements</param>
        /// <param name="axe">The direction along which the boundaries will be searched</param>
        /// <returns>Returns two points: first is the lower bound, second is the upper bound on a given direction.</returns>
        public XYZ[] GetBoundaries(XYZ minPoint, XYZ maxPoint, ICollection<ElementId> elements, Direction axe)
        {
            //log.Debug($"direction {axe} begin with [{minPoint.Print()}, {maxPoint.Print()}] {elements.Count} elementd");

            // Since Outline is not derived from an Element class there 
            // is no possibility to apply transformation, so
            // we have use as a possible directions only three vectors of basis 
            switch (axe)
            {
                case Direction.X:
                    direction = XYZ.BasisX;
                    break;
                case Direction.Y:
                    direction = XYZ.BasisY;
                    break;
                case Direction.Z:
                    direction = XYZ.BasisZ;
                    break;
                default:
                    break;
            }

            // Get the lower and upper bounds as a projection on a direction vector
            // Projection is an extention method
            double lowerBound = minPoint.Projection(direction);
            double upperBound = maxPoint.Projection(direction);

            // Set the boundary points in the plane perpendicular to the direction vector. 
            // These points are needed to create BoundingBoxIntersectsFilter when IsContainsElements calls.
            min = minPoint - lowerBound * direction;
            max = maxPoint - upperBound * direction;


            double[] res = UpperLower(lowerBound, upperBound, elements);
            //log.Debug($"On a direction {axe} {iterationCount} iteration");
            return new XYZ[2]
            {
                res[0] * direction + min,
                res[1] * direction + max,
            };
        }

        /// <summary>
        /// Check if there are any elements contains in the segment [lower, upper]
        /// </summary>
        /// <returns>True if any elements are in the segment</returns>
        private ICollection<ElementId> IsContainsElements(double lower, double upper, ICollection<ElementId> ids)
        {
            //log.Debug($"IsContainsElements: iteration {iterationCount++}, [{lower}, {upper}],  ids Count {ids.Count()}");
            var outline = new Outline(min + direction * lower, max + direction * upper);
            //if (outline.IsEmpty) return new List<ElementId>();
            return new FilteredElementCollector(doc, ids)
                .WhereElementIsNotElementType()
                .WherePasses(new BoundingBoxIntersectsFilter(outline))
                .ToElementIds();
        }


        private double[] UpperLower(double lower, double upper, ICollection<ElementId> ids)
        {
            // Get the Midpoint for segment mid = lower + 0.5 * (upper - lower)
            var mid = Midpoint(lower, upper);

            // Сheck if the first segment contains elements 
            ICollection<ElementId> idsFirst = IsContainsElements(lower, mid, ids);
            bool first = idsFirst.Any();

            // Сheck if the second segment contains elements 
            ICollection<ElementId> idsSecond = IsContainsElements(mid, upper, ids);
            bool second = idsSecond.Any();

            // If elements are in both segments 
            // then the first segment contains the lower border 
            // and the second contains the upper
            // ---------**|***--------
            if (first && second)
            {
                return new double[2]
                {
                    Lower(lower, mid, idsFirst),
                    Upper(mid, upper, idsSecond),
                };
            }

            // If elements are only in the first segment it contains both borders. 
            // We recursively call the method UpperLower until 
            // the lower border turn out in the first segment and 
            // the upper border is in the second
            // ---*****---|-----------
            else if (first && !second)
                return UpperLower(lower, mid, idsFirst);

            // Do the same with the second segment
            // -----------|---*****---
            else if (!first && second)
                return UpperLower(mid, upper, idsSecond);

            // Elements are out of the segment
            // ** -----------|----------- **
            else
                throw new ArgumentException("Segment does not contains elements. Try to make initial boundaries wider", "lower, upper");
        }

        /// <summary>
        /// Search the lower boundary of a segment containing elements
        /// </summary>
        /// <returns>Lower boundary</returns>
        private double Lower(double lower, double upper, ICollection<ElementId> ids)
        {
            // If the boundaries are within tolerance return lower bound
            if (IsInTolerance(lower, upper))
                return lower;

            // Get the Midpoint for segment mid = lower + 0.5 * (upper - lower)
            var mid = Midpoint(lower, upper);

            // Сheck if the segment contains elements 
            ICollection<ElementId> idsFirst = IsContainsElements(lower, mid, ids);
            bool first = idsFirst.Any();

            // ---*****---|-----------
            if (first)
                return Lower(lower, mid, idsFirst);
            // -----------|-----***---
            else
                return Lower(mid, upper, ids);

        }

        /// <summary>
        /// Search the upper boundary of a segment containing elements
        /// </summary>
        /// <returns>Upper boundary</returns>
        private double Upper(double lower, double upper, ICollection<ElementId> ids)
        {
            // If the boundaries are within tolerance return upper bound
            if (IsInTolerance(lower, upper))
                return upper;

            // Get the Midpoint for segment mid = lower + 0.5 * (upper - lower)
            var mid = Midpoint(lower, upper);

            // Сheck if the segment contains elements 
            ICollection<ElementId> idsSecond = IsContainsElements(mid, upper, ids);
            bool second = idsSecond.Any();

            //log.Debug($"UpperLower: Low: {lower}\t Mid: {mid}\t Up: {upper}\tsecond is {second}");

            // -----------|----*****--
            if (second)
                return Upper(mid, upper, idsSecond);
            // ---*****---|-----------
            else
                return Upper(lower, mid, ids);
        }

        private double Midpoint(double lower, double upper) => lower + 0.5 * (upper - lower);
        private bool IsInTolerance(double lower, double upper) => upper - lower <= precision;
    }
}
