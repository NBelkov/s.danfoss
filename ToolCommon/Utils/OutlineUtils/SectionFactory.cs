﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.Utils.OutlineUtils
{
    public class SectionFactory
    {
        private log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Document doc;
        private BinaryUpperLowerBoundsSearch uls;
        private PlannarRatioSlicer ps;
        private LevelOutBoxSlicer ls;
        private int maxOp;
        private XYZ minB;
        private XYZ maxB;

        public SectionFactory(Document document, XYZ minBoundary, XYZ maxBoundary, int maxOperationCount = 10000, double precision = 1000, double levelMergingTolerance = 10000)
        {
            maxOp = maxOperationCount;
            doc = document;
            uls = new BinaryUpperLowerBoundsSearch(doc, precision / 304.8);
            ps = new PlannarRatioSlicer();
            ls = new LevelOutBoxSlicer(doc, levelMergingTolerance / 304.8);
            minB = minBoundary;
            maxB = maxBoundary;
        }

        public IEnumerable<Box> GetBoxes(ICollection<ElementId> basements) => GetBoxes(basements, new Dictionary<Document, List<ElementId>> { { doc, basements.ToList() } });
        public IEnumerable<Box> GetBoxes(ICollection<ElementId> basements, Dictionary<Document, List<ElementId>> cuttings)
        {
            List<Box> result = new List<Box>();
            XYZ[] bnd = new XYZ[2];

            foreach (var kvp in cuttings)
            {
                var linkedElems = kvp.Value;
                if (TooMuch(basements, linkedElems))
                {
                    if (bnd[0] is null && bnd[1] is null)
                    {
                        //double b = 1000000 / 304.8;
                        //bnd = uls.GetBoundaries(new XYZ(-b, -b, -b), new XYZ(b, b, b), basements);
                        bnd = uls.GetBoundaries(minB, maxB, basements);
                    }
                    int level = 0;
                    foreach (var b in ls.Slice(bnd))
                    {
                        //log.Debug($"------------------------ Level {level} ------------------------");
                        result.AddRange(Splitter(b, basements, linkedElems, kvp.Key, level));
                        level = level + 1000;
                    }
                }
                else
                {
                    result.Add(new Box(bnd, kvp.Key, basements, linkedElems));
                }
            }
            return result;
        }

        private bool TooMuch<T>(ICollection<T> c1, ICollection<T> c2) => c1.Count * c2.Count > maxOp;

        private List<Box> Splitter(XYZ[] bnd, ICollection<ElementId> basements, ICollection<ElementId> cuttings, Document linkDoc, int level = 0, int part = 1)
        {
            var f = new BoundingBoxIntersectsFilter(new Outline(bnd[0], bnd[1]));

            var be = new FilteredElementCollector(doc, basements)
                .WhereElementIsNotElementType()
                .WherePasses(f)
                .ToElementIds()
                .ToList();

            var ce = new FilteredElementCollector(linkDoc, cuttings)
                .WhereElementIsNotElementType()
                .WherePasses(f)
                .ToElementIds()
                .ToList();

            if (be.Count * ce.Count > 0)
            {
                if (TooMuch(be, ce))
                {
                    //log.Debug($"{be.Count * ce.Count}\tlevel {level} part {part} is splitting into peaces, operation count is {be.Count} * {ce.Count} = {be.Count * ce.Count} > {maxOp}");
                    List<Box> result = new List<Box>();
                    level++;
                    int p = 0;
                    foreach (var b in ps.Slice(bnd))
                    {
                        p++;
                        result.AddRange(Splitter(b, be, ce, linkDoc, level, p));
                    }
                    return result;
                }
                else
                {
                    //log.Debug($"{be.Count * ce.Count}\t\t>1 level {level} part {part} stay as it is, operation count is {be.Count} * {ce.Count} = {be.Count * ce.Count} <= {maxOp}");
                    return new List<Box>() { new Box(bnd, linkDoc, be, ce) };
                }
            }
            else
            {
                //log.Debug($"{be.Count * ce.Count}\t\t\t>0 level {level} part {part} zero elements, {be.Count} * {ce.Count} = {be.Count * ce.Count}, {maxOp}");
                return new List<Box>();
            }
        }
    }


}
