﻿using Autodesk.Revit.DB;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.Utils.OutlineUtils
{
    public class Box
    {
        public Box(
            XYZ[] bounds,
            Document linkDoc,
            IEnumerable<ElementId> basements,
            IEnumerable<ElementId> cuttings)
        {
            Bounds = bounds;
            LinkDoc = linkDoc;
            Basements = basements;
            Cuttings = cuttings;
        }

        public XYZ[] Bounds { get; private set; }
        public Document LinkDoc { get; private set; }
        public IEnumerable<ElementId> Basements { get; private set; }
        public IEnumerable<ElementId> Cuttings { get; private set; }

        public override bool Equals(object obj) =>
            obj is Box box &&
                   Bounds[0].Equals(box.Bounds[0]) &&
                   Bounds[1].Equals(box.Bounds[1]) &&
                   LinkDoc.Title.Equals(box.LinkDoc.Title);

        public override int GetHashCode()
        {
            int hashCode = -1832375335;
            hashCode = hashCode * -1521134295 + Bounds[0].GetHashCode();
            hashCode = hashCode * -1521134295 + Bounds[1].GetHashCode();
            hashCode = hashCode * -1521134295 + LinkDoc.Title.GetHashCode();
            return hashCode;
        }
    }
}
