﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;

namespace S.Danfoss.CommonTools.Utils.OutlineUtils
{
    public class OutlineSlicer
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        /// Slice the the outline into peaces in each direction. Two bounds are expected {min, max}, three count are expected {cx, cy, cz}
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public List<XYZ[]> Slice(XYZ[] bounds, int[] count)
        {
            foreach (var c in count)
                if (c < 1) throw new ArgumentException($"The number of parts must be greater than zero, now it is {c} on one of the directions");

            List<XYZ[]> result = new List<XYZ[]>();

            int cx = count[0];
            int cy = count[1];
            int cz = count[2];

            XYZ min = bounds[0];
            XYZ max = bounds[1];

            XYZ d = max - min;
            XYZ dx = d.X / cx * XYZ.BasisX;
            XYZ dy = d.Y / cy * XYZ.BasisY;
            XYZ dz = d.Z / cz * XYZ.BasisZ;
            XYZ dp = dx + dy + dz;

            for (int ix = 0; ix < cx; ix++)
            {
                var minX = ix * dx;
                for (int iy = 0; iy < cy; iy++)
                {
                    var minY = iy * dy;
                    for (int iz = 0; iz < cz; iz++)
                    {
                        var minZ = iz * dz;
                        var mn = min + minX + minY + minZ;
                        result.Add(new XYZ[2] { mn, mn + dp });
                    }
                }
            }
            return result;
        }
    }
}
