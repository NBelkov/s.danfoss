﻿namespace S.Danfoss.CommonTools.Utils.SettingsUtils
{
    public enum Feature
    {
        [FeatureAttributes("Образмеривание отверстий", "Автоматическая простановка размеров и привязок отверстиям", DisciplineGroup.STR, AccessGroup.USR, PhaseGroup.DEV)]
        AutoDimensions = 101,

        [FeatureAttributes("Проверка на дубликаты", "Бытрая проверка модели на наличие элементов в одном и том же месте", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        DuplicatesCheck = 201,

        [FeatureAttributes("Пакетные булевы операции", "Пакетное объединение и выризание элементов", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        BatchOperations = 301,

        [FeatureAttributes("Поиск по UniqueId", "Поиск элементов по одному или нескольким UniqueId", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        GuidSearch = 302,

        [FeatureAttributes("Объединение слоев стен", "Объединение многослойных стен для прорезания в них отверстий под двери и окна", DisciplineGroup.ARC, AccessGroup.USR, PhaseGroup.PROD)]
        AutoJoining = 303,

        [FeatureAttributes("Просмотр логов", "Открыть папку с логами", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        OpenLog = 304,

        [FeatureAttributes("Текстовый редактор", "Выполняет поиск и автозамену фрагментов текста в текстовых примечаниях на выбранных листах", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        BatchTextRedactor = 305,

        [FeatureAttributes("Утилита для удаления нулевых размеров", "Выполняет поиск и удаление нулевых размеров в проекте или на активном листе", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        PurgeZeroSizesUtility = 306,

        [FeatureAttributes("Утилита для удаления задублированных типовых форм арматурных стержней", "Выполняет поиск и удаление задублированных типовых форм арматурных стержней", DisciplineGroup.STR, AccessGroup.USR, PhaseGroup.PROD)]
        PurgeDoubleRebarUtility = 307,

        [FeatureAttributes("Утилита для автоматической маркировки лотков/трасс ЭОМ и СС", "Выполняет автоматическую маркировку лотков/трасс по заданным параметрам", DisciplineGroup.MEP, AccessGroup.USR, PhaseGroup.PROD)]
        PurgeTryTagsUtility = 308,

        [FeatureAttributes("Утилита для автоматического создания марок ЭОМ", "Создает марки ЭОМ на листе по заданным параметрам", DisciplineGroup.MEP, AccessGroup.USR, PhaseGroup.PROD)]
        PurgeElectricalTagsUtility = 309,

        [FeatureAttributes("Утилита для простановки размерных цепочек электроустановочных ихделий", "Создает размерные цепочки на листе по заданным параметрам", DisciplineGroup.MEP, AccessGroup.USR, PhaseGroup.PROD)]
        PurgeElectricalSizeUtility = 310,

        [FeatureAttributes("Утилита для поиска элемента по UniqueId", "Выделяет элементы по введенным UniqueId и выводит информацию по элементам", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        UtilityIdSearch = 311,

        [FeatureAttributes("Пакетная печать Зуева", "Печать файлов в pdf и на бумагу", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        ZooPrint = 312,

        [FeatureAttributes("Выгрузчик v1", "Выгрузка элементов в S.Finance или в файл", DisciplineGroup.ALL, AccessGroup.PRO, PhaseGroup.DEV)]
        Extractor = 401,

        [FeatureAttributes("Выгрузчик v1 Выбор элементов", "Выгрузка выбранных элементов в S.Finance", DisciplineGroup.ALL, AccessGroup.PRO, PhaseGroup.DEV)]
        ExtractorSelectToSF = 402,

        [FeatureAttributes("Выгрузчик v1 Выбор элементов", "Выгрузка выбранных элементов в файл", DisciplineGroup.ALL, AccessGroup.PRO, PhaseGroup.DEV)]
        ExtractorSelectToFile = 403,

        [FeatureAttributes("Выгрузчик v2", "Выгрузка элементов через шину kafka", DisciplineGroup.ALL, AccessGroup.PRO, PhaseGroup.DEV)]
        Extractor2 = 501,

        [FeatureAttributes("Выгрузчик v2 Выбор элементов", "Возможность указать выгружаемые элементы в модели или линке", DisciplineGroup.ALL, AccessGroup.PRO, PhaseGroup.DEV)]
        Extractor2Select = 502,

        [FeatureAttributes("Выгрузчик v3", "Выгрузка элементов через шину kafka, выгрузка линков", DisciplineGroup.ALL, AccessGroup.PRO, PhaseGroup.PROD)]
        Extractor3 = 551,

        [FeatureAttributes("Выгрузчик v3 Выбор элементов", "Возможность указать выгружаемые элементы в модели или линке", DisciplineGroup.ALL, AccessGroup.PRO, PhaseGroup.DEV)]
        Extactor3Select = 552,

        [FeatureAttributes("Импорт приборов отопления", "Импорт параметров приборов из excel", DisciplineGroup.MEP, AccessGroup.USR, PhaseGroup.STG)]
        HeaterParamExporter = 601,

        [FeatureAttributes("Автопростановка отверстий", "Возможность проставлять отверстия в местах пересечения труб и воздуховодов со стенами", DisciplineGroup.STR, DisciplineGroup.ARC, AccessGroup.USR, PhaseGroup.DEV)]
        OpeningMaker = 701,

        [FeatureAttributes("Расставить\nарх. отв.", "Расставить архитектурные отверстия в местах размещения инженерных", DisciplineGroup.ARC, DisciplineGroup.STR, AccessGroup.USR, PhaseGroup.DEV)]
        Openings = 702,

        [FeatureAttributes("Передача параметров из комнат", " Передача параметров из комнат элементам, которые в них находятся", DisciplineGroup.MEP, AccessGroup.USR, PhaseGroup.PROD)]
        ParameterFromRoom = 801,

        [FeatureAttributes("Передача параметров между элементами", "Передача параметров от одних элементам элементам, которые в них находятся", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        ParameterFromElement = 851,

        [FeatureAttributes("Панель параметров S.Finance", "Панель инструментов для назначения параметров из S.Finance", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        SfinanceAssignePanel = 901,

        [FeatureAttributes("Валидатор", "Проверка соответствия значений параметров системе S.Finance", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        SfinanceValidator = 902,

        [FeatureAttributes("Простановка этажей", "Автоматическая простановка параметра СМ_Этаж", DisciplineGroup.ALL, AccessGroup.USR, PhaseGroup.PROD)]
        SfinanceAutoFloor = 903,

        [FeatureAttributes("Коэффициент нахлеста", "Назначает коэффициент нахлеста арматурным стержням", DisciplineGroup.STR, AccessGroup.USR, PhaseGroup.PROD)]
        RebarCoefficient = 1001,

        [FeatureAttributes("Сброс коэфф. нахлеста", "Сбрасывает коэффициент нахлеста", DisciplineGroup.STR, AccessGroup.USR, PhaseGroup.PROD)]
        RebarParametersReset = 1002,

        [FeatureAttributes("Расстановка \"Лягушек\"", "Расставляет фиксаторы в плите", DisciplineGroup.STR, AccessGroup.USR, PhaseGroup.PROD)]
        RebarChairPlacement = 1003,

        [FeatureAttributes("Автопростановка отметок окон", "Проставляет отметки окнам", DisciplineGroup.STR, AccessGroup.USR, PhaseGroup.PROD)]
        AutoLevelMarker = 1101,

        [FeatureAttributes("WaterMarker", "Проставляет семействам ватермарку", DisciplineGroup.ALL, AccessGroup.PRO, PhaseGroup.DEV)]
        WaterMarker = 1202,

       
    }


}