﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace S.Danfoss.CommonTools.Utils.SettingsUtils
{
    public class DefaultFeatureManager : IFeatureManager
    {
        private HashSet<Feature> features = new HashSet<Feature>();
#if DEBUG
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
#endif
        public DefaultFeatureManager(IEnumerable<Feature> features)
        {
            this.features = new HashSet<Feature>(features);
        }
        public bool IsEnable(Feature feature) => features.Contains(feature);
    }

    public class SimpleFeatureManager : IFeatureManager
    {
#if DEBUG
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
#endif
        private Dictionary<Feature, bool?> features;
        private Dictionary<Feature, bool?> defaultFeatures = new Dictionary<Feature, bool?>
            {
                 {Feature.AutoDimensions        , false},
                 {Feature.DuplicatesCheck       , true},
                 {Feature.BatchOperations       , true},
                 {Feature.GuidSearch            , true},
                 {Feature.OpenLog               , true},
                 {Feature.AutoJoining           , true},
                 {Feature.Extractor             , false},
                 {Feature.ExtractorSelectToSF    , false},
                 {Feature.ExtractorSelectToFile      , false},
                 {Feature.Extractor2            , false},
                 {Feature.Extractor2Select      , false},
                 {Feature.Extractor3            , true},
                 {Feature.Extactor3Select       , false},
                 {Feature.HeaterParamExporter   , true},
                 {Feature.OpeningMaker          , false},
                 {Feature.ParameterFromRoom     , true},
                 {Feature.ParameterFromElement  , true},
                 {Feature.SfinanceAssignePanel  , true},
                 {Feature.SfinanceValidator     , true},
                 {Feature.SfinanceAutoFloor     , true},
                 {Feature.RebarCoefficient      , true},
                 {Feature.RebarParametersReset  , true},
                 {Feature.RebarChairPlacement   , true},
                 {Feature.AutoLevelMarker       , true},
                 {Feature.WaterMarker           , false},
                 {Feature.UtilityIdSearch       , true}
            };
        private ISettingsProvider sp;

        public SimpleFeatureManager()
        {
            var currentPath = Assembly.GetExecutingAssembly().Location;
            var featurePath = currentPath.Substring(0, currentPath.LastIndexOf('\\') + 1) + "features.json";
#if DEBUG
            log.Debug($"feature config path :'{featurePath}'");
#endif
            sp = new JsonSettingsProvider(featurePath);
            Load();
        }

        private void Load()
        {
            features = sp.Load<Dictionary<Feature, bool?>>();
            if (!features.Any())
            {
                features = defaultFeatures;
                Save(defaultFeatures);
            }
#if DEBUG
            foreach (var item in features)
            {
                log.Debug($"{item.Key} - {item.Value}");
            }
#endif
        }

        private void Save(Dictionary<Feature, bool?> features)
        {
            foreach (var feature in features)
            {
                sp.SaveSection(feature.Value == true ? "true" : "false", feature.Key.ToString());
            }
        }

        public bool IsEnable(Feature feature) =>
            features.ContainsKey(feature) && features[feature] == true;
    }
}