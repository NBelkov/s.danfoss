﻿namespace S.Danfoss.CommonTools.Utils.SettingsUtils
{
    public interface IFeatureManager
    {
        bool IsEnable(Feature feature);
    }
}