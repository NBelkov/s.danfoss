﻿using System;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.Utils.SettingsUtils
{
    [AttributeUsage(AttributeTargets.Field)]
    public class FeatureAttributes : Attribute
    {
        public FeatureAttributes(string name, string description, DisciplineGroup featureGroup, AccessGroup accessGroup, PhaseGroup phaseGroup)
        {
            Name = name;
            Description = description;
            DisciplineGroups = new[] { featureGroup };
            AccessGroup = accessGroup;
            PhaseGroup = phaseGroup;
        }
        public FeatureAttributes(string name, string description, DisciplineGroup featureGroup0, DisciplineGroup featureGroup1, AccessGroup accessGroup, PhaseGroup phaseGroup)
        {
            Name = name;
            Description = description;
            DisciplineGroups = new[] { featureGroup0, featureGroup1 };
            AccessGroup = accessGroup;
            PhaseGroup = phaseGroup;
        }
        public FeatureAttributes(string name, string description, DisciplineGroup[] featureGroups, AccessGroup accessGroup, PhaseGroup phaseGroup)
        {
            Name = name;
            Description = description;
            DisciplineGroups = featureGroups;
            AccessGroup = accessGroup;
            PhaseGroup = phaseGroup;
        }

        public FeatureAttributes(string name, DisciplineGroup featureGroup, AccessGroup accessGroup, PhaseGroup phaseGroup)
        {
            Name = name;
            Description = string.Empty;
            DisciplineGroups = new[] { featureGroup };
            AccessGroup = accessGroup;
            PhaseGroup = phaseGroup;
        }

        public FeatureAttributes(string name, DisciplineGroup featureGroup0, DisciplineGroup featureGroup1, AccessGroup accessGroup, PhaseGroup phaseGroup)
        {
            Name = name;
            Description = string.Empty;
            DisciplineGroups = new[] { featureGroup0, featureGroup1 };
            AccessGroup = accessGroup;
            PhaseGroup = phaseGroup;
        }

        public FeatureAttributes(string name, DisciplineGroup[] featureGroups, AccessGroup accessGroup, PhaseGroup phaseGroup)
        {
            Name = name;
            Description = string.Empty;
            DisciplineGroups = featureGroups;
            AccessGroup = accessGroup;
            PhaseGroup = phaseGroup;
        }

        public DisciplineGroup[] DisciplineGroups { get; }
        public AccessGroup AccessGroup { get; }
        public PhaseGroup PhaseGroup { get; }
        public string Name { get; }
        public string Description { get; }
        public bool Enable { get; }
    }

    public static class FeatureAttributesExtentions
    {
        private static Dictionary<Feature, FeatureAttributes> attrDict = new Dictionary<Feature, FeatureAttributes>();

        public static bool Check(this Feature f, DisciplineGroup g)
        {
            if (attrDict.ContainsKey(f))
                return attrDict[f].DisciplineGroups.Any(dg => g == dg);
            else
            {
                if (f.GetFeatureAttribute() is FeatureAttributes attr)
                {
                    attrDict[f] = attr;
                    return attr.DisciplineGroups.Any(dg => g == dg);
                }
            }
            return false;
        }

        public static bool Check(this Feature f, AccessGroup g)
        {
            if (attrDict.ContainsKey(f))
                return attrDict[f].AccessGroup == g;
            else
            {
                if (f.GetFeatureAttribute() is FeatureAttributes attr)
                {
                    attrDict[f] = attr;
                    return attr.AccessGroup == g;
                }
            }
            return false;
        }

        public static bool Check(this Feature f, PhaseGroup g)
        {
            if (attrDict.ContainsKey(f))
                return attrDict[f].PhaseGroup == g;
            else
            {
                if (f.GetFeatureAttribute() is FeatureAttributes attr)
                {
                    attrDict[f] = attr;
                    return attr.PhaseGroup == g;
                }
            }
            return false;
        }


        public static FeatureAttributes GetFeatureAttribute(this Feature f)
            => (FeatureAttributes)Attribute.GetCustomAttribute(ForValue(f), typeof(FeatureAttributes));
        private static MemberInfo ForValue(this Feature f)
            => typeof(Feature).GetField(Enum.GetName(typeof(Feature), f));


        public static DescriptionAttribute GetDescription<T>(this T t) where T : Enum
            => (DescriptionAttribute)Attribute.GetCustomAttribute(ForValue(t), typeof(DescriptionAttribute));
        private static MemberInfo ForValue<T>(this T f) where T : Enum
            => typeof(T).GetField(Enum.GetName(typeof(T), f));

    }
}