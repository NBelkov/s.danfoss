﻿using System.ComponentModel;

namespace S.Danfoss.CommonTools.Utils.SettingsUtils
{
    public enum DisciplineGroup
    {
        [Description("Для всех")]
        ALL,
        [Description("Архитектура")]
        ARC,
        [Description("Конструктив")]
        STR,
        [Description("Инженерные сети")]
        MEP
    }

    public enum AccessGroup
    {
        [Description("Проектировщики")]
        USR,
        [Description("Координаторы")]
        PRO
    }

    public enum PhaseGroup
    {
        [Description("В разработке")]
        DEV,
        [Description("Тестовые функции")]
        STG,
        [Description("Готовые функции")]
        PROD
    }
}