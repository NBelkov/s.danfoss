﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S.Danfoss.CommonTools.Utils
{
    public class CreationUtils
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void CreateDirectShape(Document doc, GeometryObject go, BuiltInCategory bic = BuiltInCategory.OST_Topography) =>
            CreateDirectShape(doc, new List<GeometryObject> { go }, bic);
        public static void CreateDirectShape(Document doc, IEnumerable<GeometryObject> geometries, BuiltInCategory bic = BuiltInCategory.OST_Topography)
        {
            using (Transaction tx = new Transaction(doc))
            {

                try
                {
                    tx.Start($"Create geometry: {geometries.Count()}");

                    DirectShape ds = DirectShape.CreateElement(doc, new ElementId(bic));
                    var good = geometries.Where(g => ds.IsValidShape(new List<GeometryObject>() { g })).ToList();
                    //log.Debug($"Valid for direct shape elements: { good.Count}");
                    ds.ApplicationId = "DirectShape builder";
                    ds.ApplicationDataId = "Geometry";
                    ds.SetShape(good);

                    tx.Commit();
                }
                catch (Exception ex)
                {
                    TaskDialog.Show("Error in direct shape creator", ex.Message);
                    tx.RollBack();
                }

            }
        }

        public static List<GeometryObject> OutlineBox(XYZ min, XYZ max)
        {
            List<GeometryObject> result = new List<GeometryObject>();

            XYZ p1 = new XYZ(min.X, min.Y, min.Z);
            XYZ p2 = new XYZ(min.X, min.Y, max.Z);
            XYZ p3 = new XYZ(min.X, max.Y, max.Z);
            XYZ p4 = new XYZ(min.X, max.Y, min.Z);

            XYZ p5 = new XYZ(max.X, min.Y, min.Z);
            XYZ p6 = new XYZ(max.X, min.Y, max.Z);
            XYZ p7 = new XYZ(max.X, max.Y, max.Z);
            XYZ p8 = new XYZ(max.X, max.Y, min.Z);

            result.AddRange(PlanarFace(new XYZ[] { p1, p2, p3, p4 })); // front
            result.AddRange(PlanarFace(new XYZ[] { p1, p2, p6, p5 })); // left
            result.AddRange(PlanarFace(new XYZ[] { p4, p3, p7, p8 })); // right
            result.AddRange(PlanarFace(new XYZ[] { p5, p6, p7, p8 })); // rear
            result.AddRange(PlanarFace(new XYZ[] { p1, p4, p8, p5 })); // bottom
            result.AddRange(PlanarFace(new XYZ[] { p2, p3, p7, p6 })); // top
            return result;
        }

        public static IList<GeometryObject> PlanarFace(XYZ[] points)
        {
            try
            {
                using (TessellatedShapeBuilder builder = new TessellatedShapeBuilder())
                {
                    builder.OpenConnectedFaceSet(false);
                    List<XYZ> args = new List<XYZ>(4);
                    args.Clear();
                    args.AddRange(points);

                    TessellatedFace tesseFace = new TessellatedFace(args, ElementId.InvalidElementId);

                    if (builder.DoesFaceHaveEnoughLoopsAndVertices(tesseFace))
                    {
                        builder.AddFace(tesseFace);
                    }

                    builder.CloseConnectedFaceSet();
                    builder.Build();

                    return builder.GetBuildResult().GetGeometricalObjects();
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error", ex);
                return new List<GeometryObject>();
            }
        }
    }
}
