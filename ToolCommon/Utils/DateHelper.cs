﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S.Danfoss.CommonTools.Utils
{
    public static class DateHelper
    {

        public static string NowDateTimeAsString { get; private set; }
        public static string NowDateTimeAsFileName { get; private set; }
        public static DateTime NowDateTime { get; private set; }
        public static void UpdateCurrentDateTime()
        {
            string msg = $"TIME UPDATED: {NowDateTimeAsString}";
            NowDateTime = DateTime.Now; //Current date and time
            NowDateTimeAsString = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
            NowDateTimeAsFileName = DateTime.Now.ToString("yy.MM.dd_HHmmss");
        }
    }
}
