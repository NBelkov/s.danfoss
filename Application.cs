﻿using Autodesk.Revit.UI;
using System;
using System.Reflection;
using System.Windows;

namespace S.Danfoss
{
    public class Application : IExternalApplication
    {
        private string TAB_NAME = "S.Plugin";

        public Result OnStartup(UIControlledApplication application)
        {
            try
            {
                #region - Perform
                //new LogConfigurator().CreateConfig();

                var v = Assembly.GetExecutingAssembly().GetName().Version;
                //TAB_NAME += $" v{v.Major}.{v.Minor}";
                
                //new ExpressToolsPlugManager(application, TAB_NAME);
                new OVPlugManager(application, TAB_NAME);

                //new DuplePlugManager(application, TAB_NAME);
                //new ParametersFromElementPlugManager(application, TAB_NAME);
                

                #endregion
                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                string msg =
                    $"{ex.Source}\n\n" +
                    $"{ex.Message}\n\n" +
                    $"{ex.InnerException}\n\n" +
                    $"{ex.TargetSite}\n\n" +
                    $"{ex.StackTrace}\n\n";
                MessageBox.Show(msg);
                return Result.Failed;
            }
        }
        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }
    }
}
