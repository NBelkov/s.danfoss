﻿using Autodesk.Revit.UI;
using S.Danfoss.CommonTools;
using System.Reflection;

namespace S.Danfoss
{
    public class OVPlugManager : UIRibbonItem
    {
        public readonly string PANEL_NAME = "ОВ";

        public OVPlugManager(UIControlledApplication application, string tabName) : base(application, tabName)
        {
            panelName = PANEL_NAME;

            pathCurrent = Assembly.GetExecutingAssembly().Location;
            location = pathCurrent.Substring(0, pathCurrent.LastIndexOf('\\') + 1);
            path = location + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".dll";

            CreateRibbonItems();
        }
        protected override void CreateRibbonItems()
        {
            Autodesk.Revit.UI.RibbonPanel ribbonPanel = CreatePanel(tabName, panelName);

            SplitButtonData groupData = new SplitButtonData("Импорт в приборы отопления", "Импорт данных в приборы отопления");
            SplitButton group = ribbonPanel.AddItem(groupData) as SplitButton;

            PushButtonData pbHeater = new PushButtonData(
                "HeaterTestCommand", "Импорт\nприборов", path,
                "S.Danfoss.HeaterParamImporter.HeaterTestCommand")
            {
                ToolTip = "Импорт в параметры приборов\n отопления данных из XLS или CSV,\n сформированных расчётной программой",
                LargeImage = GetImage(Icons.thumb_491868.GetHbitmap())
            };
            group.CurrentButton = group.AddPushButton(pbHeater);
        }
    }
}
